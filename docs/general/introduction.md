# Introduction

The [`README.md`](../../README.md) gives you adequate information on how to clone, install dependencies and launch the COMMOOP api.

Once you have done that, this document is intended to give you as taste of how everything works. It still assumes basic knowledge of [Flask](http://flask.pocoo.org/) and [SQLAlchemy](https://www.sqlalchemy.org/).

## Tech Stack

Here's a curated list of libraries that you should have knowledge of, when working with this project. However, the best way to have a complete list of requirements is to see [`requirements.txt`](../../requirements.txt).

### Core

- [ ] [Flask](http://flask.pocoo.org/)
- [ ] [Flask-SQLAlchemy](http://flask-sqlalchemy.pocoo.org)
- [ ] [SQLAlchemy](https://www.sqlalchemy.org/)

### Testing

- [ ] [Flask-Testing](http://flask.pocoo.org/docs/0.12/testing/)
- [ ] [Unittest](https://docs.python.org/3/library/unittest.html)

## Project Structure

### docs
As the name suggest, this folder contains the documentation of this project.

### commooop
Our API is based on the [REST pattern](https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9). All API endpoints can be found in `api/`. Because we want to restrict the access to those endpoints we have implemented a auth decorator, which can be found in `auth/`. To connect our API to a database we have defined various models in `models/` which are representations of the database tables.

### instance
Contains the configuration file for different environments. These are `testing`, `development` and `production`.

### tests
THis folder contains all tests which are build with `flask-testing`.