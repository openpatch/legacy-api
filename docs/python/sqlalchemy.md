# `sqlalchemy`

We use an ORM to manage our database. For more information about sqlalchemy see their [official documentation](http://docs.sqlalchemy.org/en/latest/).

## Migrate

When you have changed a model, you have to write a migration for this db change. We use Alembic to autodetect the changes and write a migration file for you, but not all changes will be regogniced see [Alembic docs](http://alembic.zzzcomputing.com/en/latest/autogenerate.html#what-does-autogenerate-detect-and-what-does-it-not-detect).
To create a new migration run this command:

```python manage.py db migrate```

Ensure that the generated migration is fitting your needs and provides all the necessary code to perform the migration. Therefore, open the newest file located in `/migrations/versions`. Afterwards run the following command to apply the migration.

```python manage.py db upgrade```