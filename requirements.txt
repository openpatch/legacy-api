Flask==0.12.2
Flask-Migrate==2.0.4
Flask-Script==2.0.5
Flask-SQLAlchemy==2.2
Flask-CORS==3.0.3
Flask-Testing==0.6.2
Flask-HTTPAuth==3.2.3
SQLAlchemy-Continuum==1.3.1
passlib==1.7.1
mysqlclient==1.3.8
blinker==1.4
contextlib2==0.5.5
raven==6.7.0
coverage==4.4.1
hashids==1.2.0
jsonschema==2.6.0
uWSGI==2.0.15
beautifulsoup4==4.6.3
Markdown==2.6.11
