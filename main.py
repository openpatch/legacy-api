import logging
import os

from commoop import create_app
from raven.contrib.flask import Sentry

config_name = os.getenv("COMMOOP_MODE")  # config_name = "development"
sentry_dns = os.getenv("SENTRY_DNS")
app = create_app(config_name)

# error loggin with sentry.io
if sentry_dns:
    sentry = Sentry(app, logging=True, level=logging.ERROR, dsn=sentry_dns)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
