-- MySQL dump 10.16  Distrib 10.2.11-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: commoop
-- ------------------------------------------------------
-- Server version	10.2.11-MariaDB-10.2.11+maria~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alembic_version`
--

DROP TABLE IF EXISTS `alembic_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL,
  PRIMARY KEY (`version_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alembic_version`
--

LOCK TABLES `alembic_version` WRITE;
/*!40000 ALTER TABLE `alembic_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `alembic_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `association_item_category`
--

DROP TABLE IF EXISTS `association_item_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `association_item_category` (
  `item_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  KEY `item_id` (`item_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `association_item_category_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
  CONSTRAINT `association_item_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `association_item_category`
--

LOCK TABLES `association_item_category` WRITE;
/*!40000 ALTER TABLE `association_item_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `association_item_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `association_item_favorized`
--

DROP TABLE IF EXISTS `association_item_favorized`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `association_item_favorized` (
  `item_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  KEY `item_id` (`item_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `association_item_favorized_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
  CONSTRAINT `association_item_favorized_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `association_item_favorized`
--

LOCK TABLES `association_item_favorized` WRITE;
/*!40000 ALTER TABLE `association_item_favorized` DISABLE KEYS */;
/*!40000 ALTER TABLE `association_item_favorized` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `association_item_group`
--

DROP TABLE IF EXISTS `association_item_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `association_item_group` (
  `item_id` int(11) DEFAULT NULL,
  `item_group_id` int(11) DEFAULT NULL,
  KEY `item_id` (`item_id`),
  KEY `item_group_id` (`item_group_id`),
  CONSTRAINT `association_item_group_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
  CONSTRAINT `association_item_group_ibfk_2` FOREIGN KEY (`item_group_id`) REFERENCES `item_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `association_item_group`
--

LOCK TABLES `association_item_group` WRITE;
/*!40000 ALTER TABLE `association_item_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `association_item_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `association_item_group_test`
--

DROP TABLE IF EXISTS `association_item_group_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `association_item_group_test` (
  `item_group_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  KEY `item_group_id` (`item_group_id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `association_item_group_test_ibfk_1` FOREIGN KEY (`item_group_id`) REFERENCES `item_group` (`id`),
  CONSTRAINT `association_item_group_test_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `association_item_group_test`
--

LOCK TABLES `association_item_group_test` WRITE;
/*!40000 ALTER TABLE `association_item_group_test` DISABLE KEYS */;
/*!40000 ALTER TABLE `association_item_group_test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `association_item_permitted`
--

DROP TABLE IF EXISTS `association_item_permitted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `association_item_permitted` (
  `item_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  KEY `item_id` (`item_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `association_item_permitted_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
  CONSTRAINT `association_item_permitted_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `association_item_permitted`
--

LOCK TABLES `association_item_permitted` WRITE;
/*!40000 ALTER TABLE `association_item_permitted` DISABLE KEYS */;
INSERT INTO `association_item_permitted` VALUES (4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1);
/*!40000 ALTER TABLE `association_item_permitted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `association_survey_favorized`
--

DROP TABLE IF EXISTS `association_survey_favorized`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `association_survey_favorized` (
  `survey_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  KEY `survey_id` (`survey_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `association_survey_favorized_ibfk_1` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`),
  CONSTRAINT `association_survey_favorized_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `association_survey_favorized`
--

LOCK TABLES `association_survey_favorized` WRITE;
/*!40000 ALTER TABLE `association_survey_favorized` DISABLE KEYS */;
/*!40000 ALTER TABLE `association_survey_favorized` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `association_survey_permitted`
--

DROP TABLE IF EXISTS `association_survey_permitted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `association_survey_permitted` (
  `survey_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  KEY `survey_id` (`survey_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `association_survey_permitted_ibfk_1` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`),
  CONSTRAINT `association_survey_permitted_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `association_survey_permitted`
--

LOCK TABLES `association_survey_permitted` WRITE;
/*!40000 ALTER TABLE `association_survey_permitted` DISABLE KEYS */;
/*!40000 ALTER TABLE `association_survey_permitted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `association_test_favorized`
--

DROP TABLE IF EXISTS `association_test_favorized`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `association_test_favorized` (
  `test_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  KEY `test_id` (`test_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `association_test_favorized_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`),
  CONSTRAINT `association_test_favorized_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `association_test_favorized`
--

LOCK TABLES `association_test_favorized` WRITE;
/*!40000 ALTER TABLE `association_test_favorized` DISABLE KEYS */;
/*!40000 ALTER TABLE `association_test_favorized` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `association_test_permitted`
--

DROP TABLE IF EXISTS `association_test_permitted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `association_test_permitted` (
  `test_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  KEY `test_id` (`test_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `association_test_permitted_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`),
  CONSTRAINT `association_test_permitted_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `association_test_permitted`
--

LOCK TABLES `association_test_permitted` WRITE;
/*!40000 ALTER TABLE `association_test_permitted` DISABLE KEYS */;
/*!40000 ALTER TABLE `association_test_permitted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `format`
--

DROP TABLE IF EXISTS `format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `format` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL,
  `content_schema` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `solution_schema` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `color_hex` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `format`
--

LOCK TABLES `format` WRITE;
/*!40000 ALTER TABLE `format` DISABLE KEYS */;
INSERT INTO `format` VALUES ('2018-01-22 08:33:30','2018-01-22 08:33:30',1,'trace','tra','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\"}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\"}','ff4500'),('2018-01-22 08:33:30','2018-01-22 08:33:30',2,'questionnaire','que','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\"}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\"}','ffa500'),('2018-01-22 08:33:30','2018-01-22 08:33:30',3,'fill-in-the-blank','fil','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\", \"properties\": {\"text\": {\"type\": \"string\"}}}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"inputs\": {\"type\": \"object\", \"patternProperties\": {\"^.*$\": {\"type\": \"string\"}}}}}','800000'),('2018-01-22 08:33:30','2018-01-22 08:33:30',4,'parsons-puzzle','pap','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\", \"properties\": {\"fragments\": {\"type\": \"array\", \"items\": {\"type\": \"object\", \"properties\": {\"text\": {\"type\": \"string\"}}}}}}, \"private\": {\"type\": \"object\", \"properties\": {}}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"fragments\": {\"type\": \"array\", \"items\": {\"type\": \"object\", \"properties\": {\"id\": {\"type\": \"number\"}, \"text\": {\"type\": \"string\"}}}}}}','6fd3ff'),('2018-01-22 08:33:30','2018-01-22 08:33:30',5,'instruction','ins','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\", \"properties\": {\"text\": {\"type\": \"string\"}}}, \"private\": {\"type\": \"object\", \"properties\": {}}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\"}','008000'),('2018-01-22 08:33:30','2018-01-22 08:33:30',6,'highlight','hig','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\", \"properties\": {\"text\": {\"type\": \"string\"}}}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"highlights\": {\"type\": \"string\"}}}','a349a4'),('2018-01-22 08:33:30','2018-01-22 08:33:30',7,'multiple-choice','mc','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\", \"properties\": {\"choices\": {\"type\": \"array\", \"items\": {\"type\": \"string\"}}}}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"choices\": {\"type\": \"array\", \"items\": {\"type\": \"object\", \"properties\": {\"id\": {\"type\": \"number\"}, \"choice\": {\"type\": \"string\"}, \"checked\": {\"type\": \"boolean\"}}}}}}','009688'),('2018-01-22 08:33:30','2018-01-22 08:33:30',8,'program','pro','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\"}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"sources\": {\"type\": \"array\", \"items\": {\"type\": \"object\"}}}}','191970');
/*!40000 ALTER TABLE `format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `format_version`
--

DROP TABLE IF EXISTS `format_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `format_version` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL,
  `type` varchar(64) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL,
  `content_schema` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `solution_schema` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `color_hex` varchar(6) DEFAULT NULL,
  `transaction_id` bigint(20) NOT NULL,
  `end_transaction_id` bigint(20) DEFAULT NULL,
  `operation_type` smallint(6) NOT NULL,
  PRIMARY KEY (`id`,`transaction_id`),
  KEY `ix_format_version_operation_type` (`operation_type`),
  KEY `ix_format_version_transaction_id` (`transaction_id`),
  KEY `ix_format_version_end_transaction_id` (`end_transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `format_version`
--

LOCK TABLES `format_version` WRITE;
/*!40000 ALTER TABLE `format_version` DISABLE KEYS */;
INSERT INTO `format_version` VALUES ('2018-01-22 08:33:30','2018-01-22 08:33:30',1,'trace','tra','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\"}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\"}','ff4500',1,NULL,0),('2018-01-22 08:33:30','2018-01-22 08:33:30',2,'questionnaire','que','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\"}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\"}','ffa500',1,NULL,0),('2018-01-22 08:33:30','2018-01-22 08:33:30',3,'fill-in-the-blank','fil','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\", \"properties\": {\"text\": {\"type\": \"string\"}}}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"inputs\": {\"type\": \"object\", \"patternProperties\": {\"^.*$\": {\"type\": \"string\"}}}}}','800000',1,NULL,0),('2018-01-22 08:33:30','2018-01-22 08:33:30',4,'parsons-puzzle','pap','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\", \"properties\": {\"fragments\": {\"type\": \"array\", \"items\": {\"type\": \"object\", \"properties\": {\"text\": {\"type\": \"string\"}}}}}}, \"private\": {\"type\": \"object\", \"properties\": {}}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"fragments\": {\"type\": \"array\", \"items\": {\"type\": \"object\", \"properties\": {\"id\": {\"type\": \"number\"}, \"text\": {\"type\": \"string\"}}}}}}','6fd3ff',1,NULL,0),('2018-01-22 08:33:30','2018-01-22 08:33:30',5,'instruction','ins','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\", \"properties\": {\"text\": {\"type\": \"string\"}}}, \"private\": {\"type\": \"object\", \"properties\": {}}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\"}','008000',1,NULL,0),('2018-01-22 08:33:30','2018-01-22 08:33:30',6,'highlight','hig','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\", \"properties\": {\"text\": {\"type\": \"string\"}}}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"highlights\": {\"type\": \"string\"}}}','a349a4',1,NULL,0),('2018-01-22 08:33:30','2018-01-22 08:33:30',7,'multiple-choice','mc','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\", \"properties\": {\"choices\": {\"type\": \"array\", \"items\": {\"type\": \"string\"}}}}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"choices\": {\"type\": \"array\", \"items\": {\"type\": \"object\", \"properties\": {\"id\": {\"type\": \"number\"}, \"choice\": {\"type\": \"string\"}, \"checked\": {\"type\": \"boolean\"}}}}}}','009688',1,NULL,0),('2018-01-22 08:33:30','2018-01-22 08:33:30',8,'program','pro','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"public\": {\"type\": \"object\"}, \"private\": {\"type\": \"object\"}}}','{\"$schema\": \"http://json-schema.org/draft-04/schema\", \"type\": \"object\", \"properties\": {\"sources\": {\"type\": \"array\", \"items\": {\"type\": \"object\"}}}}','191970',1,NULL,0);
/*!40000 ALTER TABLE `format_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `assignment` text DEFAULT NULL,
  `allow_note` tinyint(1) DEFAULT NULL,
  `format_id` int(11) DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `solution` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `hash` varchar(2048) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `format_id` (`format_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `item_ibfk_1` FOREIGN KEY (`format_id`) REFERENCES `format` (`id`),
  CONSTRAINT `item_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`),
  CONSTRAINT `CONSTRAINT_1` CHECK (`allow_note` in (0,1))
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES ('2018-01-22 14:43:40','2018-01-22 14:43:40',4,'Klassenbezeichner 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Klassenbezeichner.',NULL,6,'{\"public\": {\"text\": \"```\\nimport javax.swing.*;\\nimport java.awt.*;\\nimport java.util.*;\\n\\npublic class Canvas\\n{\\n    private static Canvas cs;\\n\\n    public static Canvas getCanvas()\\n    {\\n        if(cs == null) {\\n            cs = new Canvas(\\\"BlueJ Picture Demo\\\", 500, 300, \\n                                         Color.white);\\n        }\\n        cs.setVisible(true);\\n        return cs;\\n    }\\n\\n    private JFrame frame;\\n    private CanvasPane canvas;\\n    private Graphics2D graphic;\\n    private Color backgroundColor;\\n    private Image canvasImage;\\n    private List<Object> objects;\\n    private HashMap<Object, ShapeDescription> shapes;\\n    \\n    private Canvas(String title, int width, int height, Color bgColor)\\n    {\\n        frame = new JFrame();\\n        canvas = new CanvasPane();\\n```\"}}','{}','1636379301030479577',1),('2018-01-22 14:48:07','2018-01-22 14:48:07',5,'Klassenbezeichner 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Klassenbezeichner.',NULL,6,'{\"public\": {\"text\": \"```\\nimport java.awt.*;\\nimport java.awt.event.*;\\nimport javax.swing.*;\\nimport javax.swing.border.*;\\n\\npublic class Clock\\n{\\n    private JFrame frame;\\n    private JLabel label;\\n    private ClockDisplay clock;\\n    private boolean clockRunning = false;\\n    private TimerThread timerThread;\\n    \\n    public Clock()\\n    {\\n        makeFrame();\\n        clock = new ClockDisplay();\\n    }\\n    \\n    private void start()\\n    {\\n        clockRunning = true;\\n        timerThread = new TimerThread();\\n        timerThread.start();\\n    }\\n    \\n    private void stop()\\n    {\\n        clockRunning = false;\\n    }\\n```\"}}','{}','8780476362778718004',1),('2018-01-22 14:49:58','2018-01-22 14:49:58',6,'Attributbezeichner 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Attributbezeichner.',NULL,6,'{\"public\": {\"text\": \"```\\nimport java.awt.*;\\nimport java.awt.event.*;\\nimport javax.swing.*;\\nimport javax.swing.border.*;\\n\\npublic class Clock\\n{\\n    private JFrame frame;\\n    private JLabel label;\\n    private ClockDisplay clock;\\n    private boolean clockRunning = false;\\n    private TimerThread timerThread;\\n    \\n    public Clock()\\n    {\\n        makeFrame();\\n        clock = new ClockDisplay();\\n    }\\n    \\n    private void start()\\n    {\\n        clockRunning = true;\\n        timerThread = new TimerThread();\\n        timerThread.start();\\n    }\\n    \\n    private void stop()\\n    {\\n        clockRunning = false;\\n    }\\n```\"}}','{}','-2271126822005108172',1),('2018-01-22 14:50:48','2018-01-22 14:50:48',7,'Attributbezeichner 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Attributbezeichner.',NULL,6,'{\"public\": {\"text\": \"```\\nimport javax.swing.*;\\nimport java.awt.*;\\nimport java.util.*;\\n\\npublic class Canvas\\n{\\n    private static Canvas cs;\\n\\n    public static Canvas getCanvas()\\n    {\\n        if(cs == null) {\\n            cs = new Canvas(\\\"BlueJ Picture Demo\\\", 500, 300, \\n                                         Color.white);\\n        }\\n        cs.setVisible(true);\\n        return cs;\\n    }\\n\\n    private JFrame frame;\\n    private CanvasPane canvas;\\n    private Graphics2D graphic;\\n    private Color backgroundColor;\\n    private Image canvasImage;\\n    private List<Object> objects;\\n    private HashMap<Object, ShapeDescription> shapes;\\n    \\n    private Canvas(String title, int width, int height, Color bgColor)\\n    {\\n        frame = new JFrame();\\n        canvas = new CanvasPane();\\n```\"}}','{}','8529756731177359743',1),('2018-01-22 14:55:23','2018-01-22 14:55:23',8,'Objektbezeichner 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Objektbezeichner.',NULL,6,'{\"public\": {\"text\": \"    public MusicOrganizer()\\n    {\\n        tracks = new ArrayList<>();\\n        player = new MusicPlayer();\\n        reader = new TrackReader();\\n        readLibrary(\\\"../audio\\\");\\n        System.out.println(\\\"Music library loaded. \\\" + getNumberOfTracks() + \\n        \\\" tracks.\\\");\\n        System.out.println();\\n    }\\n    \\n    public void addFile(String filename)\\n    {\\n        tracks.add(new Track(filename));\\n    }\\n    \\n    public void addTrack(Track track)\\n    {\\n        tracks.add(track);\\n    }\\n    \\n    public void playTrack(int index)\\n    {\\n        if(indexValid(index)) {\\n            Track track = tracks.get(index);\\n            player.startPlaying(track.getFilename());\\n            System.out.println(\\\"Now playing: \\\" + track.getArtist() + \\\" - \\\" + \\n            track.getTitle());\\n        }\\n    }\\n\"}}','{}','-5957444758891824965',1),('2018-01-22 14:55:54','2018-01-22 14:55:54',9,'Objektbezeichner 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Objektbezeichner.',NULL,6,'{\"public\": {\"text\": \"```\\n    public AnimalMonitor()\\n    {\\n        this.sightings = new ArrayList<>();\\n    }\\n    \\n    public void addSightings(String filename)\\n    {\\n        SightingReader reader = new SightingReader();\\n        sightings.addAll(reader.getSightings(filename));\\n    }\\n    \\n    public void printList()\\n    {\\n        for(Sighting record : sightings) {\\n            System.out.println(record.getDetails());\\n        }\\n    }\\n    \\n    public void printEndangered(ArrayList<String> animalNames, \\n                                int dangerThreshold)\\n    {\\n        for(String animal : animalNames) {\\n            if(getCount(animal) <= dangerThreshold) {\\n                System.out.println(animal + \\\" is endangered.\\\");\\n            }\\n        }\\n    }\\n    \\n    public int getCount(String animal)\\n    {\\n```\"}}','{}','2695534147430277531',1),('2018-01-22 14:56:33','2018-01-22 14:56:33',10,'Methodensignaturen 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Methodensignaturen.',NULL,6,'{\"public\": {\"text\": \"```\\n    public void removeZeroCounts()\\n    {\\n        Iterator<Sighting> it = sightings.iterator();\\n        while(it.hasNext()) {\\n            Sighting record = it.next();\\n            if(record.getCount() == 0) {\\n                it.remove();\\n            }\\n        }\\n    }\\n    \\n    public ArrayList<Sighting> getSightingsInArea(String animal, int area)\\n    {\\n        ArrayList<Sighting> records = new ArrayList<>();\\n        for(Sighting record : sightings) {\\n            if(animal.equals(record.getAnimal())) {\\n                if(record.getArea() == area) {\\n                    records.add(record);\\n                }\\n            }\\n        }\\n        return records;\\n    }\\n    \\n    public ArrayList<Sighting> getSightingsOf(String animal)\\n    {\\n        ArrayList<Sighting> filtered = new ArrayList<>();\\n        for(Sighting record : sightings) {\\n            if(animal.equals(record.getAnimal())) {\\n                filtered.add(record);\\n```\"}}','{}','-8026176608982955883',1),('2018-01-22 14:57:23','2018-01-22 14:57:23',11,'Methodensignaturen 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Methodensignaturen.',NULL,6,'{\"public\": {\"text\": \"```\\n    public void changeSize(int newHeight, int newWidth)\\n    {\\n        erase();\\n        height = newHeight;\\n        width = newWidth;\\n        draw();\\n    }\\n\\n    public void changeColor(String newColor)\\n    {\\n        color = newColor;\\n        draw();\\n    }\\n\\n    private void draw()\\n    {\\n        if(isVisible) {\\n            Canvas canvas = Canvas.getCanvas();\\n            int[] xpoints = { xPosition, xPosition + (width/2), xPosition - \\n            (width/2) };\\n            int[] ypoints = { yPosition, yPosition + height, yPosition + height };\\n            canvas.draw(this, color, new Polygon(xpoints, ypoints, 3));\\n            canvas.wait(10);\\n        }\\n    }\\n\\n    private void erase()\\n    {\\n        if(isVisible) {\\n            Canvas canvas = Canvas.getCanvas();\\n```\"}}','{}','-760586423043698555',1),('2018-01-22 14:57:48','2018-01-22 14:57:48',12,'Methodenaufrufe 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Methodenaufrufe.',NULL,6,'{\"public\": {\"text\": \"```\\n    public StockDemo()\\n    {\\n        manager = new StockManager();\\n        manager.addProduct(new Product(132, \\\"Clock Radio\\\"));\\n        manager.addProduct(new Product(37,  \\\"Mobile Phone\\\"));\\n        manager.addProduct(new Product(23,  \\\"Microwave Oven\\\"));\\n    }\\n    \\n    public void demo()\\n    {\\n        manager.printProductDetails();\\n        manager.delivery(132, 5);\\n        manager.printProductDetails();\\n    }\\n    \\n    public void showDetails(int id)\\n    {\\n        Product product = getProduct(id);\\n        if(product != null) {\\n            System.out.println(product.toString());\\n        }\\n    }\\n    \\n    public void sellProduct(int id)\\n    {\\n        Product product = getProduct(id);\\n        \\n        if(product != null) {\\n            showDetails(id);\\n            product.sellOne();\\n```\"}}','{}','-6497759056556734115',1),('2018-01-22 14:58:39','2018-01-22 14:58:39',13,'Methodenaufrufe 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Methodenaufrufe.',NULL,6,'{\"public\": {\"text\": \"```\\n        backgroundColor = bgColor;\\n        frame.pack();\\n        setVisible(true);\\n    }\\n\\n    public void setVisible(boolean visible)\\n    {\\n        if(graphic == null) {\\n            Dimension size = canvas.getSize();\\n            canvasImage = canvas.createImage(size.width, size.height);\\n            graphic = (Graphics2D)canvasImage.getGraphics();\\n            graphic.setColor(backgroundColor);\\n            graphic.fillRect(0, 0, size.width, size.height);\\n            graphic.setColor(Color.black);\\n        }\\n        frame.setVisible(true);\\n    }\\n\\n    public boolean isVisible()\\n    {\\n        return frame.isVisible();\\n    }\\n\\n    public void draw(Shape shape)\\n    {\\n        graphic.draw(shape);\\n        canvas.repaint();\\n    }\\n \\n    public void fill(Shape shape)\\n```\"}}','{}','3625776579376874600',1),('2018-01-22 14:59:00','2018-01-22 14:59:00',14,'Parameter 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Parameter.',NULL,6,'{\"public\": {\"text\": \"```\\n    public void setForegroundColor(String colorString)\\n    {\\n        if(colorString.equals(\\\"red\\\")) {\\n            graphic.setColor(new Color(235, 25, 25));\\n        }\\n        else if(colorString.equals(\\\"black\\\")) {\\n            graphic.setColor(Color.black);\\n        }\\n        else if(colorString.equals(\\\"blue\\\")) {\\n            graphic.setColor(new Color(30, 75, 220));\\n        }\\n        else if(colorString.equals(\\\"yellow\\\")) {\\n            graphic.setColor(new Color(255, 230, 0));\\n        }\\n        else if(colorString.equals(\\\"green\\\")) {\\n            graphic.setColor(new Color(80, 160, 60));\\n        }\\n        else if(colorString.equals(\\\"magenta\\\")) {\\n            graphic.setColor(Color.magenta);\\n        }\\n        else if(colorString.equals(\\\"white\\\")) {\\n            graphic.setColor(Color.white);\\n        }\\n        else {\\n            graphic.setColor(Color.black);\\n        }\\n    }\\n\\n    public void wait(int milliseconds)\\n    {\\n```\"}}','{}','875521176065771461',1),('2018-01-22 14:59:29','2018-01-22 14:59:29',15,'Parameter 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Parameter.',NULL,6,'{\"public\": {\"text\": \"```\\n    public StockDemo()\\n    {\\n        manager = new StockManager();\\n        manager.addProduct(new Product(132, \\\"Clock Radio\\\"));\\n        manager.addProduct(new Product(37,  \\\"Mobile Phone\\\"));\\n        manager.addProduct(new Product(23,  \\\"Microwave Oven\\\"));\\n    }\\n    \\n    public void demo()\\n    {\\n        manager.printProductDetails();\\n        manager.delivery(132, 5);\\n        manager.printProductDetails();\\n    }\\n    \\n    public void showDetails(int id)\\n    {\\n        Product product = getProduct(id);\\n        if(product != null) {\\n            System.out.println(product.toString());\\n        }\\n    }\\n    \\n    public void sellProduct(int id)\\n    {\\n        Product product = getProduct(id);\\n        \\n        if(product != null) {\\n            showDetails(id);\\n            product.sellOne();\\n```\"}}','{}','-6497759056556734115',1),('2018-01-22 14:59:53','2018-01-22 14:59:53',16,'Objektinstanziierungen 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Objektinstanziierungen.',NULL,6,'{\"public\": {\"text\": \"```\\n    {\\n        Ellipse2D.Double circle = new Ellipse2D.Double(xPos, yPos,\\n        diameter, diameter);\\n        fill(circle);\\n    }\\n\\n    public void fillRectangle(int xPos, int yPos, int width, int height)\\n    {\\n        fill(new Rectangle(xPos, yPos, width, height));\\n    }\\n\\n    public void erase()\\n    {\\n        Color original = graphic.getColor();\\n        graphic.setColor(backgroundColor);\\n        Dimension size = canvas.getSize();\\n        graphic.fill(new Rectangle(0, 0, size.width, size.height));\\n        graphic.setColor(original);\\n        canvas.repaint();\\n    }\\n\\n    public void eraseCircle(int xPos, int yPos, int diameter)\\n    {\\n        Ellipse2D.Double circle = new Ellipse2D.Double(xPos, yPos, \\n        diameter, diameter);\\n    }\\n\\n    public void eraseRectangle(int xPos, int yPos, int width, int height)\\n    {\\n        erase(new Rectangle(xPos, yPos, width, height));\\n```\"}}','{}','-5208749045717979078',1),('2018-01-22 15:00:20','2018-01-22 15:00:20',17,'Objektinstanziierungen 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Objektinstanziierungen.',NULL,6,'{\"public\": {\"text\": \"```\\n    private void makeFrame()\\n    {\\n        frame = new JFrame(\\\"Clock\\\");\\n        JPanel contentPane = (JPanel)frame.getContentPane();\\n        contentPane.setBorder(new EmptyBorder(1, 60, 1, 60));\\n\\n        makeMenuBar(frame);\\n        \\n        contentPane.setLayout(new BorderLayout(12, 12));\\n        \\n        label = new JLabel(\\\"00:00\\\", SwingConstants.CENTER);\\n        Font displayFont = label.getFont().deriveFont(96.0f);\\n        label.setFont(displayFont);\\n        contentPane.add(label, BorderLayout.CENTER);\\n\\n        JPanel toolbar = new JPanel();\\n        toolbar.setLayout(new GridLayout(1, 0));\\n        \\n        JButton startButton = new JButton(\\\"Start\\\");\\n        startButton.addActionListener(e -> start());\\n        toolbar.add(startButton);\\n        \\n        JButton stopButton = new JButton(\\\"Stop\\\");\\n        stopButton.addActionListener(e -> stop());\\n        toolbar.add(stopButton);\\n\\n        JButton stepButton = new JButton(\\\"Step\\\");\\n        stepButton.addActionListener(e -> step());\\n        toolbar.add(stepButton);\\n\\n```\"}}','{}','759494469536448400',1),('2018-01-22 15:00:51','2018-01-22 15:00:51',18,'Polymorphe Methodenaufrufe 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden polymorphen Methodenaufrufe.',NULL,6,'{\"public\": {\"text\": \"```\\n    public Sighting(String animal, int spotter, int count, int area, int period)\\n    {\\n        this.animal = animal;\\n        this.spotter = spotter;\\n        this.count = count;\\n        this.area = area;\\n        this.period = period;\\n    }\\n\\n    public String getAnimal() \\n    {\\n        return animal;\\n    }\\n\\n    public static void main (String[] args){\\n        Cow c = new Cow();\\n        Dog d = new Dog();\\n        Cat ca = new Cat();\\n        Bird b = new Bird();\\n        ArrayList<Animal> zoo = new ArrayList<>();\\n        zoo.add(c);\\n        zoo.add(d);\\n        zoo.add(ca);\\n        zoo.add(b);\\n\\n        for(Animal i : zoo) {\\n            i.makeNoise();\\n        }\\n    }\\n}\\n```\"}}','{}','9188948080202917734',1),('2018-01-22 15:01:18','2018-01-22 15:01:23',19,'Polymorphe Methodenaufrufe 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden polymorphen Methodenaufrufe.',NULL,6,'{\"public\": {\"text\": \"```\\n            playerThread.start();\\n        }\\n        catch (Exception ex) {\\n            reportProblem(filename);\\n        }\\n    }\\n\\n    public static void main (String[] args){\\n        Pres p = new Pres();\\n        Music m = new Music();\\n        Video v = new Video();\\n        Gif g = new Gif();\\n        ArrayList<Media> library = new ArrayList<>();\\n        library.add(p);\\n        library.add(m);\\n        library.add(v);\\n        library.add(g);\\n\\n        for(Media i : library) {\\n            i.play();\\n        }\\n\\n        synchronized(this) {\\n            if(player != null) {\\n                player.stop();\\n                player = null;\\n            }\\n        }\\n    }\\n}\\n```\"}}','{}','-4454938611070428184',1),('2018-01-22 15:01:58','2018-01-22 15:01:58',20,'Typecasts 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Typecasts.',NULL,6,'{\"public\": {\"text\": \"```\\n        backgroundColor = bgColor;\\n        frame.pack();\\n        objects = new ArrayList<Object>();\\n        shapes = new HashMap<Object, ShapeDescription>();\\n    }\\n\\n    public void setVisible(boolean visible)\\n    {\\n        if(graphic == null) {\\n            Dimension size = canvas.getSize();\\n            canvasImage = canvas.createImage(size.width, size.height);\\n            graphic = (Graphics2D)canvasImage.getGraphics();\\n            graphic.setColor(backgroundColor);\\n            graphic.fillRect(0, 0, size.width, size.height);\\n            graphic.setColor(Color.black);\\n        }\\n        frame.setVisible(visible);\\n    }\\n\\n    public void draw(Object referenceObject, String color, Shape shape)\\n    {\\n        objects.remove(referenceObject);\\n        objects.add(referenceObject);\\n        shapes.put(referenceObject, new ShapeDescription(shape, color));\\n        this.bh = (int)(height * 0.7);\\n    }\\n \\n    public void erase(Object referenceObject)\\n    {\\n        objects.remove(referenceObject);\\n```\"}}','{}','5074261219220832296',1),('2018-01-22 15:02:18','2018-01-22 15:02:18',21,'Typecasts 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Typecasts.',NULL,6,'{\"public\": {\"text\": \"```\\n    public Font getFont()\\n    {\\n        return graphic.getFont();\\n    }\\n\\n    public void setSize(int width, int height)\\n    {\\n        canvas.setPreferredSize(new Dimension(width, height));\\n        Image oldImage = canvasImage;\\n        canvasImage = canvas.createImage(width, height);\\n        graphic = (Graphics2D)canvasImage.getGraphics();\\n        graphic.setColor(backgroundColor);\\n        graphic.fillRect(0, 0, width, height);\\n        graphic.drawImage(oldImage, 0, 0, null);\\n        this.bh = (int)(height * 0.7);\\n    }\\n\\n    public Dimension getSize()\\n    {\\n        return canvas.getSize();\\n    }\\n\\n    public void wait(int milliseconds)\\n    {\\n        try\\n        {\\n            Thread.sleep(milliseconds);\\n        } \\n        catch (InterruptedException e)\\n        {\\n```\"}}','{}','7103142981785631486',1),('2018-01-22 15:02:52','2018-01-22 15:02:52',22,'Dynamische Datenstrukturen 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden dynamischen Datenstrukturen.',NULL,6,'{\"public\": {\"text\": \"```\\nimport java.util.*;\\n\\npublic class LabClass\\n{\\n    private String instructor;\\n    private String room;\\n    private String timeAndDay;\\n    private ArrayList<Student> students;\\n    private int capacity;\\n    \\n    public LabClass(int maxNumberOfStudents)\\n    {\\n        instructor = \\\"unknown\\\";\\n        room = \\\"unknown\\\";\\n        timeAndDay = \\\"unknown\\\";\\n        students = new ArrayList<Student>();\\n        capacity = maxNumberOfStudents;\\n    }\\n\\n    public void enrollStudent(Student newStudent)\\n    {\\n        if(students.size() == capacity) {\\n            System.out.println(\\\"The class is full, you cannot enrol.\\\");\\n        }\\n        else {\\n            students.add(newStudent);\\n        }\\n    }\\n    \\n    public int numberOfStudents()\\n```\"}}','{}','5652733160175201932',1),('2018-01-22 15:03:21','2018-01-22 15:03:21',23,'Dynamische Datenstrukturen 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden dynamischen Datenstrukturen.',NULL,6,'{\"public\": {\"text\": \"```\\nimport java.util.ArrayList;\\nimport java.util.List;\\nimport java.util.Iterator;\\n\\npublic class MailServer\\n{\\n    private List<MailItem> items;\\n\\n    public MailServer()\\n    {\\n        items = new ArrayList<>();\\n    }\\n\\n    public int howManyMailItems(String who)\\n    {\\n        int count = 0;\\n        for(MailItem item : items) {\\n            if(item.getTo().equals(who)) {\\n                count++;\\n            }\\n        }\\n        return count;\\n    }\\n\\n    public MailItem getNextMailItem(String who)\\n    {\\n        Iterator<MailItem> it = items.iterator();\\n        while(it.hasNext()) {\\n            MailItem item = it.next();\\n            if(item.getTo().equals(who)) {\\n```\"}}','{}','5270012163699652732',1),('2018-01-22 15:03:52','2018-01-22 15:03:52',24,'Methoden Ã¼berladen 001','Bitte markieren Sie im folgenden Quelltextfragment alle Zeilen, in denen Methoden Ã¼berladen werden.',NULL,6,'{\"public\": {\"text\": \"```\\n    public void displayRandomComment(){\\n        int entry = 0;\\n        int size = this.getComments().size();\\n        if(size>0){\\n            entry=(int)(Math.random()*size);\\n            System.out.println(this.getComments().get(entry));\\n        }else{\\n            System.out.println(\\\"No comments to show :(\\\");\\n        }\\n    }\\n\\n    public void displayFirstComments(){\\n        int size = this.getComments().size();\\n        if(size<=10){\\n            displayFirstComments(size);\\n        }else{\\n            displayFirstComments(10);\\n        }\\n    }\\n\\n    public void displayFirstComments(int n){\\n        int size = this.getComments().size();\\n            if(size==0){\\n                System.out.println(\\\"No comments to show :(\\\");\\n            }else{\\n                for(int i = 0; i<n;i++){\\n                System.out.println(this.getComments().get(i));\\n            }\\n        }\\n    }\\n```\"}}','{}','2643765896329400888',1),('2018-01-22 15:04:14','2018-01-22 15:04:14',25,'Methoden Ã¼berladen 002','Bitte markieren Sie im folgenden Quelltextfragment alle Zeilen, in denen Methoden Ã¼berladen werden.',NULL,6,'{\"public\": {\"text\": \"```\\n        }else{\\n            System.out.println(\\\"-\\\");\\n        }\\n    }\\n\\n    public void dfc(){\\n        int s = this.gc().size();\\n        if(s<=10){\\n            dfc(s);\\n        }else{\\n            dfc(10);\\n        }\\n    }\\n    \\n    public void dfc(int n){\\n        int s = this.gc().size();\\n        if(s==0){\\n            System.out.println(\\\"-\\\");\\n        }else{\\n            for(int i = 0; i<n;i++){\\n                System.out.println(this.gc().get(i));\\n            }\\n        }\\n    }\\n\\n    public void dwc(){\\n        for (P p : c){\\n            p.d();\\n        }\\n    }\\n```\"}}','{}','1005309880138068763',1),('2018-01-22 15:05:02','2018-01-22 15:05:02',26,'Konstruktoren 001','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Konstruktoren, die eine Kopie eines Ã¼bergebenen Objektes erstellen.',NULL,6,'{\"public\": {\"text\": \"```\\nimport java.util.ArrayList;\\nimport java.awt.Color;\\nimport java.util.Random;\\n\\npublic class MP extends A{\\n    private String m;\\n    private P p;\\n    private ArrayList<P> c;\\n\\n    public MP(MP mp){\\n        this.m = mp.getM();\\n        this.p = mp.getP();\\n        this.c = mp.getC();\\n    }\\n\\n    public MP(String a, String t){\\n        super(a);\\n        this.m = t;\\n        p = new P(a);\\n        c = new ArrayList<>();\\n    }\\n\\n    private void sP(String f)\\n    {\\n        try {\\n            InSt is = getInSt (f);\\n            Pl p = new AdPl(is, crAuDe());\\n        }\\n        catch (IOException e) {\\n            rP(f);\\n```\"}}','{}','-6156454478449664726',1),('2018-01-22 15:05:24','2018-01-22 15:05:24',27,'Konstruktoren 002','Bitte unterstreichen Sie im folgenden Quelltextfragment alle auftretenden Konstruktoren, die eine Kopie eines Ã¼bergebenen Objektes erstellen.',NULL,6,'{\"public\": {\"text\": \"```\\nimport java.util.ArrayList;\\n\\npublic class MessagePost extends Post{\\n    private String message;\\n    private Post post;\\n    private ArrayList<Post> content;\\n\\n    public MessagePost(String author, String text){\\n        super(author);\\n        this.message = text;\\n        post = new Post(author);\\n        content = new ArrayList<>();\\n    }\\n\\n    public MessagePost(MessagePost aMessagePost){\\n        this.message = aMessagePost.getMessage();\\n        this.post = aMessagePost.getPost();\\n        this.content = aMessagePost.getContent();\\n    }\\n\\n    public void turnTo(int angle)\\n    {\\n        rotation = angle;\\n    }\\n\\n    public void setColor(Color newColor)\\n    {\\n        color = newColor;\\n    }\\n\\n```\"}}','{}','-6635041682504834103',1),('2018-01-22 15:18:04','2018-01-22 15:19:41',28,'Bennedsen 001','Welcher Inhalt wird nach AusfÃ¼hren der main-Methode der Klasse TestClass aus der Kommandozeile zu sehen sein?',NULL,1,'{\"public\": {\"blocks\": [{\"head\": \"```java\\npublic class A\\n```\", \"content\": \"```java\\nprivate int x;\\n```\", \"blocks\": [{\"head\": \"```java\\npublic A(int newX)\\n```\", \"content\": \"```java\\nx = newX;\\n```\", \"blocks\": []}, {\"head\": \"```java\\npublic void printA()\\n```\", \"content\": \"```java\\nSystem.out.println(x+2);\\n```\", \"blocks\": []}]}, {\"head\": \"```java\\npublic class TestClass\\n```\", \"content\": \"\", \"blocks\": [{\"head\": \"```java\\nstatic public void main()\\n```\", \"content\": \"```java\\nA a1 = new A(3);\\nA a2 = new A(7);\\na1.printA();\\na2.printA();\\n```\", \"blocks\": []}]}]}}','{\"answer\": \"5\\n9\"}','5875010789537673119',1),('2018-01-22 15:24:11','2018-01-22 15:47:15',29,'Striewe 002','FÃ¼lle die Leerstellen syntaktisch und semantisch korrekt aus!',NULL,3,'{\"public\": {\"text\": \"```\\npublic class <input /> {\\n\\tprivate String title;\\n\\tprivate <input /> author;\\n\\t\\n\\tpublic Book (<input /> title, String <input />) {\\n\\t\\tthis.title = title;\\n\\t\\tthis.author = author;\\n\\t}\\n\\t\\n\\tPublic <input /> getTitle() {\\n\\t\\treturn title;\\n\\t}\\n\\t\\n\\tpublic String getAuthor() {\\n\\t\\treturn author;\\n\\t}\\n}\\n\\n```\"}}','{}','6101400018992848603',1),('2018-01-22 15:46:04','2018-01-22 15:56:09',42,'Striewe 001','FÃ¼lle die Leerstellen syntaktisch und semantisch korrekt aus!',NULL,3,'{\"public\": {\"text\": \"```\\npublic class ArrayRechner {\\n  public int arraySum(int[][] sumAr) {\\n    <input /> (int i = 0; i < sumAr.length; i++) {\\n      int sum = 0;\\n      <input /> (int j = 0; j < sumAr[i]. <input />; j++) {\\n        sum = sum + sumAr[i][j];\\n      }\\n    }\\n    <input /> (sum > 0) {\\n      <input /> sum;\\n    } <input /> {\\n      <input /> 0;\\n    }\\n  }\\n\\n  public static <input /> main(String[] args) {\\n    int[][] test = {{1, 2, 3}, {4, 5, 6}};\\n    ArrayRechner t = <input /> ArrayRechner();\\n    <input />.out.println(t.arraySum(test));\\n  }\\n}\\n```\"}}','{}','5860238773871918627',1),('2018-01-22 15:47:02','2018-01-22 15:47:02',43,'Striewe 003','FÃ¼lle die Leerstellen syntaktisch und semantisch korrekt aus!',NULL,3,'{\"public\": {\"text\": \"```\\npublic <input /> Klasse {\\n\\tpublic <input /> methode(int a, int b, <input /> c, <input /> d) {\\n\\t\\t<input /> (;c!=d;a-=b) {\\n\\t\\t\\t<input />.out.println(d);\\n\\t\\t\\td = !d;\\n\\t\\t}\\n\\t}\\n\\t\\n\\tpublic static <input /> main(String[] args) {\\n\\t\\tKlasse <input /> = <input /> Klasse();\\n\\t\\tklasse. <input /> (1, 2, true, false);\\n\\t}\\n}\\n\\n```\"}}','{}','-971622638520730194',1),('2018-01-22 15:47:40','2018-01-22 15:47:40',44,'Striewe 004','FÃ¼lle die Leerstellen syntaktisch und semantisch korrekt aus!',NULL,3,'{\"public\": {\"text\": \"```\\npublic class Person {\\n\\tprivate <input /> name;\\n\\tprivate String <input />;\\n\\t\\n\\tpublic <input />(String name, String <input />) {\\n\\t\\tthis.name <input /> <input />;\\n\\t\\tthis.firstName = firstName;\\n\\t}\\n\\t\\n\\tpublic String getName() {\\n\\t\\treturn name;\\n\\t}\\n\\t\\n\\tpublic <input /> getFirstName() {\\n\\t\\treturn firstName;\\n\\t}\\n}\\n```\"}}','{}','1404709092688730620',1),('2018-01-22 15:48:03','2018-01-22 15:48:03',45,'Striewe 005','FÃ¼lle die Leerstellen syntaktisch und semantisch korrekt aus!',NULL,3,'{\"public\": {\"text\": \"```\\npublic class Quersumme {\\n\\t\\n\\tpublic <input /> int berechneQuersumme(int zahl)\\n\\t{\\n\\t\\tint <input /> =0;\\n\\t\\t<input /> (zahl>0){\\n\\t\\t\\tsum+=zahl%10;\\n\\t\\t\\tzahl=zahl/10;\\n\\t\\t}\\n\\t\\t<input /> sum;\\n\\t}\\n}\\n```\"}}','{}','-5731387295895228309',1),('2018-01-22 15:48:28','2018-01-22 15:48:28',46,'Striewe 006','FÃ¼lle die Leerstellen syntaktisch und semantisch korrekt aus!',NULL,3,'{\"public\": {\"text\": \"```\\npublic class Rechner {\\n    public <input /> berechnung(int <input />) {\\n    \\t<input /> ergebnis = true;\\n    \\t\\n    \\tint fak = 1;\\n    \\tfor (int i = 1; i<= zahl; i++) {\\n    \\t\\tfak = fak * i;\\n    \\t}\\n    \\t\\n    \\tfak = fak + 5;\\n    \\t\\n    \\tint test = 2;\\n    \\twhile (test * test <= fak && ergebnis) {\\n    \\t\\tif (fak % test == 0) {<input /> = false;}\\n    \\t\\ttest++;\\n    \\t}\\n\\n       return ergebnis;\\n    }\\n\\n    public static void main(String[] args) {\\n    \\tRechner t = new <input />();\\n        System.out.println(t.berechnung(5));\\n    }\\n}\\n```\"}}','{}','-3324144609404173999',1),('2018-01-22 15:49:03','2018-01-22 15:49:03',47,'Striewe 007','FÃ¼lle die Leerstellen syntaktisch und semantisch korrekt aus!',NULL,3,'{\"public\": {\"text\": \"```\\npublic class Schaltjahr {\\n\\n\\tpublic static <input /> schaltjahr(<input /> jahr)\\n\\t{\\n\\t\\t<input /> (jahr % 4 != 0) <input /> false;\\n\\t\\t<input /> (jahr % 400 == 0) <input /> true;\\n\\t\\t<input /> (jahr % 100 == 0) <input /> false;\\n\\t\\t<input /> true;\\n\\t}\\n}\\n```\"}}','{}','6770194880410983578',1),('2018-01-22 15:49:28','2018-01-22 15:49:28',48,'Striewe 008','FÃ¼lle die Leerstellen syntaktisch und semantisch korrekt aus!',NULL,3,'{\"public\": {\"text\": \"```\\npublic class Taschenrechner {\\n\\t\\n\\tpublic static int rechne(int a, int <input />, <input /> zeichen)\\n\\t{\\n\\t\\t<input /> (zeichen. <input /> (\\\"+\\\")) {\\n\\t\\t    return a+b;\\n\\t\\t} <input /> if (zeichen. <input /> (\\\"-\\\")) {\\n\\t\\t\\treturn a-b;\\n\\t\\t} else <input /> (zeichen.equals(\\\"*\\\")) {\\n\\t\\t\\treturn a*b;\\n\\t\\t} <input /> {\\n\\t\\t\\treturn a/b;\\n\\t\\t}\\n\\t}\\n}\\n```\"}}','{}','-7939190026606101647',1);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_group`
--

DROP TABLE IF EXISTS `item_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_group` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_randomized` tinyint(1) DEFAULT NULL,
  `is_jumpable` tinyint(1) DEFAULT NULL,
  `is_adaptive` tinyint(1) DEFAULT NULL,
  `needs_to_be_correct` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `CONSTRAINT_1` CHECK (`is_randomized` in (0,1)),
  CONSTRAINT `CONSTRAINT_2` CHECK (`is_jumpable` in (0,1)),
  CONSTRAINT `CONSTRAINT_3` CHECK (`is_adaptive` in (0,1)),
  CONSTRAINT `CONSTRAINT_4` CHECK (`needs_to_be_correct` in (0,1))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_group`
--

LOCK TABLES `item_group` WRITE;
/*!40000 ALTER TABLE `item_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_group_result`
--

DROP TABLE IF EXISTS `item_group_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_group_result` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_group_id` int(11) DEFAULT NULL,
  `test_result_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_group_id` (`item_group_id`),
  KEY `test_result_id` (`test_result_id`),
  CONSTRAINT `item_group_result_ibfk_1` FOREIGN KEY (`item_group_id`) REFERENCES `item_group` (`id`),
  CONSTRAINT `item_group_result_ibfk_2` FOREIGN KEY (`test_result_id`) REFERENCES `test_result` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_group_result`
--

LOCK TABLES `item_group_result` WRITE;
/*!40000 ALTER TABLE `item_group_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_group_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_result`
--

DROP TABLE IF EXISTS `item_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_result` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `solution` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `note` text DEFAULT NULL,
  `actions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `correct` tinyint(1) DEFAULT NULL,
  `item_group_result_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `member_id` (`member_id`),
  KEY `item_group_result_id` (`item_group_result_id`),
  CONSTRAINT `item_result_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
  CONSTRAINT `item_result_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`),
  CONSTRAINT `item_result_ibfk_3` FOREIGN KEY (`item_group_result_id`) REFERENCES `item_group_result` (`id`),
  CONSTRAINT `CONSTRAINT_1` CHECK (`correct` in (0,1))
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_result`
--

LOCK TABLES `item_result` WRITE;
/*!40000 ALTER TABLE `item_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_result_comment`
--

DROP TABLE IF EXISTS `item_result_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_result_comment` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `category` varchar(128) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `item_result_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `parent_id` (`parent_id`),
  KEY `item_result_id` (`item_result_id`),
  CONSTRAINT `item_result_comment_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`),
  CONSTRAINT `item_result_comment_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `item_result_comment` (`id`),
  CONSTRAINT `item_result_comment_ibfk_3` FOREIGN KEY (`item_result_id`) REFERENCES `item_result` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_result_comment`
--

LOCK TABLES `item_result_comment` WRITE;
/*!40000 ALTER TABLE `item_result_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_result_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) DEFAULT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password_hash` varchar(128) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `refresh_token` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `member_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES ('2018-01-22 08:33:30','2018-01-22 13:51:12',1,'admin',NULL,NULL,NULL,'$6$rounds=656000$6aqLOMNh5MNwzg43$uMH9iu6lV0OMkVuEm/b4a8ej83.ua/0BJaVRVkzOK2OEzgn5fi1u2ptmCSf9ANOD4enfYoWawfiqgBrn2e/d51',1,'eyJhbGciOiJIUzI1NiIsImlhdCI6MTUxNjYyOTA3MiwiZXhwIjoxNTE2NjMyNjcyfQ.eyJpZCI6MSwiZGF0ZSI6MTUxNjYyOTA3Mi4xMTUwMjd9._56eRkM9fD3p2yQLFrg_jJmhTUGxTkK7hKLividDJ6I');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES ('2018-01-22 08:33:30','2018-01-22 08:33:30',1,'admin'),('2018-01-22 08:33:30','2018-01-22 08:33:30',2,'author'),('2018-01-22 08:33:30','2018-01-22 08:33:30',3,'data_analyst'),('2018-01-22 08:33:30','2018-01-22 08:33:30',4,'user');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey`
--

DROP TABLE IF EXISTS `survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `term` varchar(128) DEFAULT NULL,
  `starts_on` date DEFAULT NULL,
  `ends_on` date DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `url_suffix` varchar(128) DEFAULT NULL,
  `password_hash` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `url_suffix` (`url_suffix`),
  KEY `member_id` (`member_id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `survey_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`),
  CONSTRAINT `survey_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey`
--

LOCK TABLES `survey` WRITE;
/*!40000 ALTER TABLE `survey` DISABLE KEYS */;
/*!40000 ALTER TABLE `survey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `item_amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `test_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_result`
--

DROP TABLE IF EXISTS `test_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_result` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  `session_hash` varchar(64) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_hash` (`session_hash`),
  KEY `test_id` (`test_id`),
  KEY `survey_id` (`survey_id`),
  CONSTRAINT `test_result_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`),
  CONSTRAINT `test_result_ibfk_2` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_result`
--

LOCK TABLES `test_result` WRITE;
/*!40000 ALTER TABLE `test_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `test_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `issued_at` datetime DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `remote_addr` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_transaction_user_id` (`user_id`),
  CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES ('2018-01-22 08:33:30',1,NULL,NULL);
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-22 16:01:30
