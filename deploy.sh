#/bin/bash
echo "building docker image"
docker build --pull -t git.uni-due.de:6666/commoop/commoop-api .

echo "pushing docker image"
docker push git.uni-due.de:6666/commoop/commoop-api

echo "deploying docker image"
ssh barkmin@app.cs.uni-due.de << EOF
 cd /data2/docker/commoop
 docker-compose pull
 docker-compose up -d --no-deps --build api
 sh dbdump.sh
 docker-compose up -d --no-deps --build api-migration
EOF

echo "deployed"
