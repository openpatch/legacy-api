import os


class Config(object):
    """Parent configuration class."""
    DEBUG = False
    CSRF_ENABLED = True
    SECRET_KEY = os.getenv('COMMOOP_SECRET')
    SQLALCHEMY_POOL_RECYCLE = 280
    SQLALCHEMY_POOL_TIMEOUT = 20
    SQLALCHEMY_DATABASE_URI = os.getenv('COMMOOP_DB')


class DevelopmentConfig(Config):
    """Configurations for Development."""
    DEBUG = True
    SENTRY_CONFIG = {
        'environment': 'development'
    }


class TestingConfig(Config):
    """Configurations for Testing, with a separate test database."""
    SQLALCHEMY_DATABASE_URI = os.getenv('COMMOOP_TEST_DB')
    SECRET_KEY = 'test'
    TESTING = True
    DEBUG = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False


class StagingConfig(Config):
    """Configurations for Staging."""
    DEBUG = True


class ProductionConfig(Config):
    """Configurations for Production."""
    DEBUG = False
    TESTING = False
    SENTRY_CONFIG = {
        'environment': 'production'
    }


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'production': ProductionConfig,
}
