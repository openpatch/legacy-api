from tests.api.test_api import BaseTestApi
from flask import json
import random

# The implemented tests deal with the functional and structural aspects
# of API calls. The response structure is only tested, if there is
# actually structured response.

base_url = '/api/v1/members'
urls = ['', '/me', '/login', '/logout',
        '/<member_id>', '/<member_id>/items',
        '/<member_id>/permissions', '/<member_id>/tests']

class TestMembers(BaseTestApi):

    # print the basic information to the console
    # check the response that k gets, when he or she tries to access a url 
    def print_information(self, url, k, response):
        print('Some ' + k + ' tried to access '+url+' and got a ' +
        str(response.status_code))

    # POST test
    def test_all_member_post_requests(self):
        for k, v in self.headers.items():
            print()
            print('***Incoming POST information on '+k.upper()+'***')
            self.post_member(k, v)

    # PATCH test
    def test_all_member_patch_requests(self):
        for k, v in self.headers.items():
            print()
            print('***Incoming PATCH information on '+k.upper()+'***')
            self.patch_member_permissions_by_id(k, v)

    # GET test
    def test_all_member_get_requests(self):
        member_urls = []
        for member in urls:
            member_urls.append(base_url+member)
        # all users shall try to access all urls
        for k, v in self.headers.items():
            print()
            print('***Incoming GET information on '+k.upper()+'***')
            self.get_all_members(member_urls[0], k, v)
            self.get_me(member_urls[1], k, v)
            self.login(member_urls[2], k, v)
            self.logout(member_urls[3], k, v)
            self.get_member_by_id(k, v)
            self.get_items_for_member(k, v)
            self.get_member_permissions_by_id(k, v)
            self.get_tests_for_member(k,v)

    def get_member_information(self, v):
        return self.client.get(base_url+'/me', headers=v).json.get('me',[])

# ---------GET all accessible members---------
    
    #functional test
    def get_all_members(self, url, k, v):
        # accessed resource
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid user
        if k != 'admin':
            self.assertEqual(response.status_code, 403)
        else:
            self.assertEqual(response.status_code, 200)
            content = response.json.get('members', [])
            self.get_all_members_struct(content, k, v)

    #structural test
    def get_all_members_struct(self, content, k, v):
        #admin can see all 12 members
        self.assertEqual(len(content), 12)
# ---------POST a member---------

    def post_member(self, k, v):
        # accessed resource
        data = {
            'username':'john_smith',
            'password':'smith_john',
            'first_name':'John',
            'last_name':'Smith',
            # 'email':'user@bogus.com',
            # 'role':'dataAnalyst'
        }
        response = self.client.post(base_url, data=json.dumps(data), headers=v)
        self.print_information(base_url, k, response)

        # response
        if k != 'admin':
            self.assertEqual(response.status_code, 403)
        else:
            self.assertEqual(response.status_code, 422)
            data['email'] = 'user@bogus.com'
            response = self.client.post(base_url, data=json.dumps(data), headers=v)
            self.print_information(base_url, k, response)
            self.assertEqual(response.status_code, 422)
            data['role'] = 'data_analyst'
            response = self.client.post(base_url, data=json.dumps(data), headers=v)
            self.print_information(base_url, k, response)
            self.assertEqual(response.status_code, 200)
            self.post_member_struct(response, k, v)
            response = self.client.post(base_url, data=json.dumps(data), headers=v)
            self.print_information(base_url, k, response)
            self.assertEqual(response.status_code, 200)
            self.post_member_struct(response, k, v)

    def post_member_struct(self, content, k, v):
        if content.json.get('status')=='created':
            self.assertEqual(int(content.json.get('member_id')), 13)
        elif content.json.get('status')=='existing':
            self.assertTrue(isinstance(content.json.get('detail', []), str))
        else:
            print('SOMETHING WNET HORRIBLY WRONG :(')

# ---------GET all accessible members---------

    def get_me(self, url, k, v):
        # accessed resource
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid member
        if k == 'unknown_user':
            self.assertEqual(response.status_code, 403)
        else:
            self.assertEqual(response.status_code, 200)
            content = self.get_member_information(v)
            self.get_me_struct(content, k, v)
    
    def get_me_struct(self, content, k, v):
        pass

# ---------GET login data---------

    def login(self, url, k, v):
        # accessed resource
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIn('access_token', data)
        self.assertIn('refresh_token', data)

# ---------GET new access token---------

    def get_token(self, url, k, v):
        # accessed resource
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIn('access_token', data)
        self.assertIn('refresh_token', data)

# ---------GET logout data---------

    def logout(self, url, k, v):
        # accessed resource
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertEqual(data.get('status'), 'ok', msg='status is not ok')

# ---------GET a member by its id---------

    def get_member_by_id(self, k, v):
        self.get_member_by_id_fun(k, v, random.randint(1, 12))
        self.get_member_by_id_fun(k, v, random.randint(13, 20))

    # functional test
    def get_member_by_id_fun(self, k, v, index):
        # accessed resource
        url = base_url+'/'+str(index)
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid member
        if k == 'unknown_user':
            self.assertEqual(response.status_code, 403)
        else:
            # valid member
            me = self.get_member_information(v)
            if k == 'admin' or me['id']==index:
                if 1<=index<=12:
                    self.assertEqual(response.status_code, 200)
                    content = response.json.get('member', [])
                    self.get_member_by_id_struct(content, k, v)
                else:
                    self.assertEqual(response.status_code, 404)
            else:
                self.assertEqual(response.status_code, 403)

    # structural test
    def get_member_by_id_struct(self, content, k, v):
        pass

# ---------GET all items of a member---------

    def get_items_for_member(self, k, v):
        self.get_items_for_member_fun(k, v, random.randint(1, 12))
        self.get_items_for_member_fun(k, v, random.randint(13, 20))

    def get_items_for_member_fun(self, k, v, index):
        all_ids = []
        i = 1
        while i <= 500:
            all_ids.append(i)
            i += 1

        # accessed resource
        url = base_url+'/'+str(index)+'/items'
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        #invalid member
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            if not 1 <= index <= 12:
                self.assertEqual(response.status_code, 404)
            else:
                self.assertEqual(response.status_code, 200)
                if k == 'admin':
                    # get the whole item list
                    content = response.json.get('items')
                    # check structure
                    self.get_items_for_member_struct(content, k, v)
                elif k == 'author' or k == 'data_analyst':
                    # author or data analyst can only see permitted items
                    items_permitted = self.client.get('/api/v1/items', headers=v).json.get('items', [])
    
    def get_items_for_member_struct(self, content, k, v):
        pass

# ---------GET all permissions of a member---------

    def get_member_permissions_by_id(self, k, v):
        self.get_member_permissions_by_id_fun(k, v, random.randint(1,12))
        self.get_member_permissions_by_id_fun(k, v, random.randint(13,20))
    
    def get_member_permissions_by_id_fun(self, k, v, index):
        # accessed resource
        url = base_url+'/'+str(index)+'/permissions'
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        #invalid member
        if k != 'admin':
            self.assertEqual(response.status_code, 403)
        else:
            if not 1 <= index <= 12:
                self.assertEqual(response.status_code, 404)
            else:
                self.assertEqual(response.status_code, 200)
                self.get_member_permissions_by_id_struct('content', k, v)

    def get_member_permissions_by_id_struct(self, content, k, v):
        pass

# ---------PATCH new permissions for a member---------
    def patch_member_permissions_by_id(self, k, v):
        self.patch_member_permissions_by_id_fun(k, v, random.randint(4, 9))
        self.patch_member_permissions_by_id_fun(k, v, random.randint(15, 20))

    def patch_member_permissions_by_id_fun(self, k, v, index):
        # accessed resource
        data = {
            'items_permitted': [1,4,5,7],
            'tests_permitted': [2,5,6,5],
            'surveys_permitted': [1,1,1,3],
            }
        response = self.client.patch(base_url+'/'+str(index)+'/permissions',
                    data=json.dumps(data), headers=v)
        self.print_information(base_url, k, response)
        # response
        if k != 'admin':
            self.assertEqual(response.status_code, 403)
        else:
            if not 1<=index<=12:
                self.assertEqual(response.status_code, 404)
            else:
                self.assertEqual(response.status_code, 200)
                self.patch_member_permissions_by_id_struct(response, k, v)

    def patch_member_permissions_by_id_struct(self, content, k, v):
        status = content.json.get('status')
        member_id = content.json.get('member_id')
        items_permitted = content.json.get('items_permitted')
        tests_permitted = content.json.get('tests_permitted')
        surveys_permitted = content.json.get('surveys_permitted')
        self.assertEqual(status, 'updated')
        self.assertGreaterEqual(member_id, 4)
        self.assertLessEqual(member_id, 9)
        self.assertEqual(len(items_permitted), 4)
        for item in items_permitted:
            self.assertTrue(item['id']==1 or item['id']==4 or item['id']==5 or item['id']==7)
        self.assertEqual(len(tests_permitted), 3)
        for test in tests_permitted:
            self.assertTrue(test['id']==2 or test['id']==5 or test['id']==6)
        self.assertEqual(len(surveys_permitted), 2)
        for survey in surveys_permitted:
            self.assertTrue(survey['id']==1 or survey['id']==3)

# ---------GET all tests of a member---------

    def get_tests_for_member(self, k,v):
        self.get_tests_for_member_fun(k, v, random.randint(1,12))
        self.get_tests_for_member_fun(k, v, random.randint(13,20))
    
    def get_tests_for_member_fun(self, k, v, index):
        # accessed resource
        url = base_url+'/'+str(index)+'/tests'
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        #invalid member
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            if not 1 <= index <= 12:
                self.assertEqual(response.status_code, 404)
            else:
                self.assertEqual(response.status_code, 200)
                if k == 'admin':
                    content = response.json.get('tests')
                    self.get_member_permissions_by_id_struct(content, k, v)

    def get_tests_for_member_struct(self, content, k, v):
        pass
