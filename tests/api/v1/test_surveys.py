from tests.api.test_api import BaseTestApi
from flask import json
import random

# The implemented tests deal with the functional and structural aspects
# of API calls. The response structure is only tested, if there is
# actually structured response.

base_url = '/api/v1/surveys'
urls = ['', '/<survey_id>']

class TestSurveys(BaseTestApi):

    # print the basic information to the console
    # check the response that k gets, when he or she tries to access a url 
    def print_information(self, url, k, response):
        print('Some ' + k + ' tried to access '+url+' and got a ' +
        str(response.status_code))

    # POST test
    def test_all_survey_post_requests(self):
        # all users shall try to access all urls
        for k, v in self.headers.items():
            print()
            print('***Incoming POST information on '+k.upper()+'***')
            self.post_survey(k, v)

    # GET test
    def test_all_survey_get_requests(self):
        survey_urls = []
        for survey in urls:
            survey_urls.append(base_url+survey)
        # all users shall try to access all urls
        for k, v in self.headers.items():
            print()
            print('***Incoming GET information on '+k.upper()+'***')
            self.get_all_surveys(k, v)      
            self.get_survey_by_id(k, v)

    def get_permitted_surveys_for_member(self, v):
        return self.client.get(base_url, headers=v).json.get('surveys',[])

# ---------GET all accessible surveys---------

    def get_all_surveys(self, k, v):
        self.get_all_surveys_fun(k, v)

    # functional test
    def get_all_surveys_fun(self, k, v):
        # accessed resource
        url = base_url
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid user
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            self.assertEqual(response.status_code, 200)
            displayed_surveys = response.json.get('surveys', [])
            self.get_all_surveys_struct(displayed_surveys, k, v)

    # structural test
    def get_all_surveys_struct(self, content, k, v):
        if k == 'admin':
            # admin has to see all 34 surveys
            self.assertEqual(len(content), 34)
        elif k == 'author' or 'data_analyst':
            # author or data analyst have to see none or some
            self.assertGreaterEqual(len(content), 0)
            for survey in content:
                # but they have to see their own surveys
                self.assertTrue(survey['member_id']==5)

# ---------POST a survey---------

    def post_survey(self, k, v):
        self.post_survey_fun(k, v)
    
    def post_survey_fun(self, k, v):
        # sent JSON data
        data = {
            #'city' = 'Oberhausen',
            #'term':'Summer2012',
            'test_id' : 17,
            'starts_on':'bullshit123',
            'ends_on':'yup'
            #'starts_on':'2017-08-22',
            #'ends_on':'2017-08-31'
        }
        # accessed resource
        response = self.client.post(base_url, data=json.dumps(data), headers=v)
        self.print_information(base_url, k, response)

        # response
        if k == 'admin' or k=='author':
            self.assertEqual(response.status_code, 400)
            
            data['starts_on'] = '2017-10-15'
            response = self.client.post(base_url, data=json.dumps(data), headers=v)
            self.print_information(base_url, k, response)
            self.assertEqual(response.status_code, 400)
            
            data['ends_on'] = '2016-09-23'
            response = self.client.post(base_url, data=json.dumps(data), headers=v)
            self.print_information(base_url, k, response)
            self.assertEqual(response.status_code, 422)
            
            data['starts_on'] = None
            response = self.client.post(base_url, data=json.dumps(data), headers=v)
            self.print_information(base_url, k, response)
            self.assertEqual(response.status_code, 422)
            
            data['ends_on'] = None
            response = self.client.post(base_url, data=json.dumps(data), headers=v)
            self.print_information(base_url, k, response)
            self.assertEqual(response.status_code, 422)
            
            data['city'] = 'Oberhausen'
            response = self.client.post(base_url, data=json.dumps(data), headers=v)
            self.print_information(base_url, k, response)
            self.assertEqual(response.status_code, 422)

            data['term'] = 'Summer2012'
            response = self.client.post(base_url, data=json.dumps(data), headers=v)
            self.print_information(base_url, k, response)
            self.assertEqual(response.status_code, 200)

            self.post_survey_struct(response, k, v)
        else:            
            self.assertEqual(response.status_code, 403)

    def post_survey_struct(self, content, k, v):
        status = content.json.get('status')
        self.assertEqual(status, 'created')
        survey_id = content.json.get('survey_id')
        survey_name = content.json.get('survey_name')
        if k=='admin':
            self.assertEqual(survey_id, 35)
            self.assertEqual(survey_name, 'Oberhausen_Summer2012_35')
        elif k=='author':
            self.assertEqual(survey_id, 36)
            self.assertEqual(survey_name, 'Oberhausen_Summer2012_36')
        else:
            print('SOMETHING WENT HORRIBLY WRONG')

# ---------GET a survey by its id---------

    def get_survey_by_id(self, k, v):
        self.get_survey_by_id_fun(k, v, random.randint(1,34))
        self.get_survey_by_id_fun(k, v, random.randint(35,50))

    def get_survey_by_id_fun(self, k, v, index):
        all_ids = []
        i = 1
        while i <= 34:
            all_ids.append(i)
            i += 1

        # accessed resource
        url = base_url+'/'+str(index)
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        #invalid member
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            if not 1 <= index <= 34:
                self.assertEqual(response.status_code, 404)
            else:
                if k == 'admin':
                    # admin has always access
                    self.assertEqual(response.status_code, 200)
                    content = response.json.get('survey')
                    # survey id must be the same as the queried index
                    self.assertEqual(content['id'], index)
                    # check structure
                    self.get_survey_by_id_struct(content, k, v)
                elif k == 'author' or k == 'data_analyst':
                    # author or data analyst can only see permitted surveys
                    surveys_permitted = self.get_permitted_surveys_for_member(v)
                    survey_ids = []
                    # collect all survey ids of permitted surveys
                    for survey in surveys_permitted:
                        survey_ids.append(survey['id'])
                    surveys_permitted = survey_ids
                    surveys_denied = all_ids
                    # with that knowledge, generate all ids of denied surveys
                    for survey in surveys_permitted:
                        if survey in surveys_denied:
                            surveys_denied.remove(survey)
                    # check that the sum of both lengths equals 34 (amount of all surveys)
                    self.assertEqual(len(surveys_denied) +
                                        len(surveys_permitted), 34)
                    # if author or data analyst are allowed to access the survey, response should be 200
                    if index in surveys_permitted:
                        self.assertEqual(response.status_code, 200)
                        content = response.json.get('survey')
                        self.assertEqual(content['id'], index)
                        self.get_survey_by_id_struct(content, k, v)
                    elif index in surveys_denied:
                        self.assertEqual(response.status_code, 403)
    
    def get_survey_by_id_struct(self, content, k, v):    
        # add general assertions
        pass