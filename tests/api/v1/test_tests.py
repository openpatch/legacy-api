from tests.api.test_api import BaseTestApi
from flask import json
import random

# The implemented tests deal with the functional and structural aspects
# of API calls. The response structure is only tested, if there is
# actually structured response.

base_url = '/api/v1/tests'
urls = ['', '/<test_id>', '/<test_id>/items', '/<test_id>/results']


class TestTests(BaseTestApi):

    # print the basic information to the console
    # check the response that k gets, when he or she tries to access a url
    def print_information(self, url, k, response):
        print('Some ' + k + ' tried to access ' + url + ' and got a ' +
              str(response.status_code))

    # POST test
    def test_all_test_post_requests(self):
        # all users shall try to access all urls
        for k, v in self.headers.items():
            print()
            print('***Incoming POST information on ' + k.upper() + '***')
            self.post_test(k, v)

    # GET test
    def test_all_test_get_requests(self):
        test_urls = []
        for test in urls:
            test_urls.append(base_url + test)
        # all users shall try to access all urls
        for k, v in self.headers.items():
            print()
            print('***Incoming GET information on ' + k.upper() + '***')
            self.get_all_tests(test_urls[0], k, v)
            self.get_test_by_id(k, v)
            self.get_items_for_test_id(k, v)
            self.get_results_for_test_id(k, v)

    def get_permitted_tests_for_member(self, v):
        return self.client.get(base_url, headers=v).json.get('tests', [])

# ---------GET all accessible tests---------

    def get_all_tests(self, url, k, v):
        self.get_all_tests_fun(url, k, v)

    # functional test
    def get_all_tests_fun(self, url, k, v):
        # accessed resource
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid user
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            self.assertEqual(response.status_code, 200)
            displayed_tests = response.json.get('tests', [])
            self.get_all_tests_struct(displayed_tests, k, v)

    # structural test
    def get_all_tests_struct(self, content, k, v):
        if k == 'admin':
            # admin has to see all 80 tests
            self.assertEqual(len(content), 80)
        elif k == 'author' or 'data_analyst':
            # author or data analyst have to see none or some
            self.assertGreaterEqual(len(content), 0)
            for test in content:
                # but they have to see their own tests
                self.assertTrue(test['member_id'] == 5)

# ---------POST a test---------

    def post_test(self, k, v):
        self.post_test_fun(k, v)

    def post_test_fun(self, k, v):
        # sent JSON data
        data = {
            #'name': 'Test_Name_1234',
            'item_groups': [
                {
                    'is_randomized': True,
                    'items': [1, 2, 3]
                },
                {
                    'is_randomized': False,
                    'items': [4, 5, 6]
                },
                {
                    'is_randomized': False,
                    'items': [7, 8, 9]
                },
                {
                    'is_randomized': True,
                    'items': [10, 11, 12]
                }
            ]
        }
        # accessed resource
        response = self.client.post(base_url, data=json.dumps(data), headers=v)
        self.print_information(base_url, k, response)

        # response
        if k == 'admin' or k == 'author':
            self.assertEqual(response.status_code, 422)
            data['name'] = 'Test_Name_1234'
            response = self.client.post(
                base_url, data=json.dumps(data), headers=v)
            self.print_information(base_url, k, response)
            self.assertEqual(response.status_code, 200)
            self.post_test_struct(response, k, v)
        else:
            self.assertEqual(response.status_code, 403)

    def post_test_struct(self, content, k, v):
        status = content.json.get('status')
        self.assertEqual(status, 'created')
        test_id = content.json.get('test_id')
        if k == 'admin':
            self.assertEqual(test_id, 81)
        elif k == 'author':
            self.assertEqual(test_id, 82)
        else:
            print('SOMETHING WENT HORRIBLY WRONG')

# ---------GET a test by its id---------

    def get_test_by_id(self, k, v):
        self.get_test_by_id_fun(k, v, random.randint(1, 80))
        self.get_test_by_id_fun(k, v, random.randint(81, 100))

    def get_test_by_id_fun(self, k, v, index):
        all_ids = []
        i = 1
        while i <= 80:
            all_ids.append(i)
            i += 1

        # accessed resource
        url = base_url + '/' + str(index)
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid member
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            if not 1 <= index <= 80:
                self.assertEqual(response.status_code, 404)
            else:
                if k == 'admin':
                    # admin has always access
                    self.assertEqual(response.status_code, 200)
                    content = response.json.get('test')
                    # test id must be the same as the queried index
                    self.assertEqual(content['id'], index)
                    # check structure
                    self.get_test_by_id_struct(content, k, v)
                elif k == 'author' or k == 'data_analyst':
                    # author or data analyst can only see permitted tests
                    tests_permitted = self.get_permitted_tests_for_member(v)
                    test_ids = []
                    # collect all test ids of permitted tests
                    for test in tests_permitted:
                        test_ids.append(test['id'])
                    tests_permitted = test_ids
                    tests_denied = all_ids
                    # with that knowledge, generate all ids of denied tests
                    for test in tests_permitted:
                        if test in tests_denied:
                            tests_denied.remove(test)
                    # check that the sum of both lengths equals 80 (amount of all tests)
                    self.assertEqual(len(tests_denied) +
                                     len(tests_permitted), 80)
                    # if author or data analyst are allowed to access the test, response should be 200
                    if index in tests_permitted:
                        self.assertEqual(response.status_code, 200)
                        content = response.json.get('test')
                        self.assertEqual(content['id'], index)
                        self.get_test_by_id_struct(content, k, v)
                    elif index in tests_denied:
                        self.assertEqual(response.status_code, 403)

    def get_test_by_id_struct(self, content, k, v):
        # add general assertions
        pass

# ---------GET all items for an test---------

    def get_items_for_test_id(self, k, v):
        self.get_items_for_test_fun(k, v, random.randint(1, 80))
        self.get_items_for_test_fun(k, v, random.randint(81, 100))

    def get_items_for_test_fun(self, k, v, index):
        all_ids = []
        i = 1
        while i < 80:
            all_ids.append(i)
            i += 1

        # accessed resource
        url = base_url + '/' + str(index) + '/items'
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid member
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            # invalid index
            if not 1 <= index <= 80:
                self.assertEqual(response.status_code, 404)
            # valid member and index
            else:
                # admin can see all items of a test
                if k == 'admin':
                    self.assertEqual(response.status_code, 200)
                    content = response.json.get('items')
                    self.get_items_for_test_struct(content, k, v)
                # authors and data analysts can only see those items,
                # that are used in permitted tests
                elif k == 'author' or k == 'data_analyst':
                    tests_permitted = self.get_permitted_tests_for_member(v)
                    test_ids = []
                    for test in tests_permitted:
                        test_ids.append(test['id'])
                    tests_permitted = test_ids
                    tests_denied = all_ids
                    for test in tests_permitted:
                        if test in tests_denied:
                            tests_denied.remove(test)
                    if index in tests_permitted:
                        self.assertEqual(response.status_code, 200)
                        content = response.json.get('items')
                        self.get_items_for_test_struct(content, k, v)
                    elif index in tests_denied:
                        self.assertEqual(response.status_code, 403)

    def get_items_for_test_struct(self, content, k, v):
        pass

# ---------GET all results for a test---------

    def get_results_for_test_id(self, k, v):
        self.get_results_for_test_fun(k, v, random.randint(1, 80))
        self.get_results_for_test_fun(k, v, random.randint(81, 100))

    def get_results_for_test_fun(self, k, v, index):
        all_ids = []
        i = 1
        while i < 80:
            all_ids.append(i)
            i += 1

        # accessed resource
        url = base_url + '/' + str(index) + '/results'
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid member
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            # invalid index
            if not 1 <= index <= 80:
                self.assertEqual(response.status_code, 404)
            # valid member and index
            else:
                # admin can see all 150 results
                if k == 'admin':
                    self.assertEqual(response.status_code, 200)
                    content = response.json.get('test_results')
                    self.get_results_for_test_struct(content, k, v)
                # authors and data analysts can only see those results,
                # where permitted tests are used
                elif k == 'author' or k == 'data_analyst':
                    tests_permitted = self.get_permitted_tests_for_member(v)
                    test_ids = []
                    for test in tests_permitted:
                        test_ids.append(test['id'])
                    tests_permitted = test_ids
                    tests_denied = all_ids
                    for test in tests_permitted:
                        if test in tests_denied:
                            tests_denied.remove(test)
                    if index in tests_permitted:
                        self.assertEqual(response.status_code, 200)
                        content = response.json.get('test_results')
                        self.get_results_for_test_struct(content, k, v)
                    elif index in tests_denied:
                        self.assertEqual(response.status_code, 403)

    def get_results_for_test_struct(self, content, k, v):
        pass
