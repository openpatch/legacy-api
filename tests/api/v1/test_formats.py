from tests.api.test_api import BaseTestApi
from flask import json
import random


class TestFormats(BaseTestApi):

    def test_get_formats(self):
        print()
        for k, v in self.headers.items():
            response = self.client.get('/api/v1/formats', headers=v)
            print('Some ' + k + ' tried to access /api/v1/formats and got a ' +
                  str(response.status_code))
            if k == 'admin' or k == 'author' or k == 'data_analyst':
                self.assertEqual(response.status_code, 200)
                data = response.json
                self.assertEqual(len(data.get('formats', [])), 9)
            else:
                self.assertEqual(response.status_code, 403)

    def test_get_format(self):
        #all available formats
        formats = ['trc', 'tracing-code', 'que', 'questionnaire',
        'c-t', 'c-test', 'pap', 'parsons-puzzle',
        'cot', 'copy-text', 'ins', 'instructions',
        'syn', 'syntax-semantics', 'hig', 'highlight',
        'mc', 'multiple-choice',
        '123qwe']
        print()
        for k, v in self.headers.items():
            print()
            print('***Incoming information on '+k.upper()+'***')
            for format in formats:
                response = self.client.get('/api/v1/formats/' + format, headers=v)
                print('Some ' + k + ' tried to access /api/v1/formats/' +
                    format + ' and got a ' + str(response.status_code))
                if k == 'admin' or k == 'author' or k == 'data_analyst':
                    if format and format != '123qwe':
                        self.assertEqual(response.status_code, 200)
                        data = response.json
                        self.assertTrue(data['format']['type']==format or data['format']['code']==format)
                    else:
                        self.assertEqual(response.status_code, 404)
                else:
                    self.assertEqual(response.status_code, 403)