from tests.api.test_api import BaseTestApi
from flask import json
from commoop.database import db
from commoop.models.item import Item
from commoop.models.item_group import ItemGroup
from commoop.models.item_result import ItemResult
from commoop.models.member import Member
from itertools import islice
import random

# The implemented tests deal with the functional and structural aspects
# of API calls. The response structure is only tested, if there is
# actually structured response.

base_url = '/api/v1/items'
urls = ['', '/results', '/<item_id>', '/<item_id>/tests', '/<item_id>/results']

# all available formats
formats = ['trc', 'tracing-code', 'que', 'questionnaire',
           'c-t', 'c-test', 'pap', 'parsons-puzzle',
           'cot', 'copy-text', 'ins', 'instructions',
           'syn', 'syntax-semantics', 'hig', 'highlight',
           '123qwe', None]


class TestItems(BaseTestApi):

    # print the basic information to the console
    # check the response that k gets, when he or she tries to access a url
    def print_information(self, url, k, response):
        print('Some ' + k + ' tried to access '+url+' and got a ' +
        str(response.status_code))
    
    # POST test
    def test_all_item_post_requests(self):
        for k, v in self.headers.items():
            print()
            print('***Incoming POST information on '+k.upper()+'***')
            self.post_item(k, v)

    # PUT test
    def test_item_put_requests(self):
        
        admin = Member.query.filter_by(username="admin").first()
        author = Member.query.filter_by(username="bob").first()
        data_analyst = Member.query.filter_by(username="david").first()

        admin_access = {
            404: range(501, 510),
            # because sqlalchemy can not filter relationships directly use inline if only take max the first 10
            405: [item.id for item in Item.query.all() if item.item_groups or item.results][-10:],
            403: [],
            200: [item.id for item in Item.query.filter(Item.member == admin or Item.member == author).all() if not item.item_groups and not item.results][-10:]
        }

        print()
        print('***Incoming PUT information on ADMIN***')
        self.put_item_by_id('admin', self.headers['admin'], admin_access)

        author_access = {
            404: range(501, 510),
            # because sqlalchemy can not filter relationships directly use inline if only take max the first 10
            405: [item.id for item in Item.query.filter_by(member=author).all() if item.item_groups or item.results][-10:],
            403: [item.id for item in Item.query.filter(Item.member != author).limit(5).all()],
            200: [item.id for item in Item.query.filter_by(member=author).all() if not item.item_groups and not item.results][-10:]
        }

        print()
        print('***Incoming PUT information on AUTHOR***')
        self.put_item_by_id('author', self.headers['author'], author_access)

        data_analyst_access = {
            404: range(501, 510),
            # because sqlalchemy can not filter relationships directly use inline if only take max the first 10
            405: [item.id for item in Item.query.filter_by(member=data_analyst).all() if item.item_groups or item.results][-10:],
            403: [item.id for item in Item.query.filter(Item.member != data_analyst).limit(5).all()],
            200: [item.id for item in Item.query.filter_by(member=data_analyst).all() if not item.item_groups and not item.results][-10:]
        }

        print()
        print('***Incoming PUT information on DATA_ANALYST***')
        self.put_item_by_id('data_analyst', self.headers['data_analyst'], data_analyst_access)

    # DELETE test
    def test_item_delete_requests(self):
        for k, v in self.headers.items():
            print()
            print('***Incoming DELETE information on '+k.upper()+'***')
            self.delete_item_by_id(k, v, {})

    # GET test
    def test_all_item_get_requests(self):
        item_urls = []
        for item in urls:
            item_urls.append(base_url + item)
        # all users shall try to access all urls
        for k, v in self.headers.items():
            print()
            print('***Incoming GET information on '+k.upper()+'***')
            self.get_all_items(item_urls[0], k, v)
            self.get_all_item_results(item_urls[1], k, v)
            self.get_item_by_id(item_urls[2], k, v)
            self.get_tests_for_item_id(item_urls[3], k, v)
            self.get_results_for_item_id(item_urls[4], k, v)

# ---------GET all items regarding a specific format---------

    def get_all_items(self, url, k, v):
        # all formats
        for format in formats:
            # a format different from 'None' has been chosen
            if format:
                self.get_all_items_w_format_fun(k, v, format)
            # format is None
            else:
                self.get_all_items_fun(url, k, v)

    def get_permitted_items_for_member(self, v):
        return self.client.get(base_url, headers=v).json.get('items', [])

    # functional test
    def get_all_items_w_format_fun(self, k, v, format):
        # accessed resource
        url = base_url + '?format=' + format
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid user
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            # invalid format
            if format == '123qwe':
                self.assertEqual(response.status_code, 404)
            # valid format and user
            else:
                displayed_items = response.json.get('items', [])
                if k == 'admin':
                    self.assertEqual(response.status_code, 200)
                elif k == 'author' or 'data_analyst':
                    self.assertEqual(response.status_code, 200)
                # check structure only if there is content
                self.get_all_items_w_format_struct(
                    displayed_items, k, v, format)

    # structural test
    def get_all_items_w_format_struct(self, content, k, v, format):
        if k == 'admin':
            # admin gets all items from all different authors regarding a certain format
            self.assertLess(len(content), 500)
        elif k == 'author' or 'data_analyst':
            items_permitted = self.get_permitted_items_for_member(v)
            # the displayed items are filtered for a certain format
            # and must be less or equal than the permitted items
            self.assertLessEqual(len(content), len(items_permitted))
        # collects all item formats and codes of displayed items in a list
        item_formats = []
        for item in content:
            if item['format']['type'] not in item_formats:
                item_formats.append(item['format']['type'])
            if item['format']['code'] not in item_formats:
                item_formats.append(item['format']['code'])
        # and this list has to be 0 (no items for a format exist)
        # or 2 (one type + one code)
        self.assertTrue(len(item_formats) == 0 or len(item_formats) == 2)
        # if it equals 2, it has to be the specified format
        if len(item_formats) > 0:
            self.assertIn(format, item_formats)

# ---------GET all accessible items---------

    # functional test
    def get_all_items_fun(self, url, k, v):
        # accessed resource
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid user
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            self.assertEqual(response.status_code, 200)
            displayed_items = response.json.get('items', [])
            self.get_all_items_struct(displayed_items, k, v)

    # structural test
    def get_all_items_struct(self, content, k, v):
        if k == 'admin':
            # admin has to see all 500 items
            self.assertEqual(len(content), 500)
        elif k == 'author' or 'data_analyst':
            # author or data analyst have to see none or some
            self.assertGreaterEqual(len(content), 0)
            for item in content:
                # but they have to see their own items
                self.assertTrue(item['member_id'] == 5 or item['member_id'] == 7)

# ---------POST an item---------

    def post_item(self, k, v):
        self.post_item_fun(k, v)
    
    def post_item_fun(self, k, v):
        # sent JSON data
        data = {
            'format' : 'qwertzuiop',
            'content' : {},
            'solution' : {}
        }
        # accessed resource
        response = self.newmethod937(data, v, k)

        # response
        if k == 'admin' or k=='author':
            self.assertEqual(response.status_code, 404)
            data['format'] = 'trc'
            response = self.newmethod937(data, v, k)
            self.assertEqual(response.status_code, 200)
            self.post_item_struct(response, k, v)
        else:            
            self.assertEqual(response.status_code, 403)

    def newmethod937(self, data, v, k):
        response = self.client.post(base_url, data=json.dumps(data), headers=v)
        self.print_information(base_url, k, response)
        return response

    def post_item_struct(self,content, k, v):
        status = content.json.get('status')
        self.assertTrue(isinstance(status,str))
        item_id = content.json.get('item_id')
        if status=='created':
            self.assertEqual(item_id, 501)
        elif status=='existing':
            self.assertLessEqual(item_id, 501)
        else:
            print('SOMETHING WENT HORRIBLY WRONG')

# ---------GET all possible item results---------

    def get_all_item_results(self, url, k, v):
        # all formats
        for format in formats:
            # a format different from 'None' has been chosen
            if format:
                self.get_all_item_results_w_format_fun(url, k, v, format)
            # format is None
            else:
                self.get_all_item_results_fun(url, k, v)

    def get_all_item_results_fun(self, url, k, v):
        # accessed resource
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid member
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            if k == 'admin' or k == 'author' or k == 'data_analyst':
                self.assertEqual(response.status_code, 200)
            # only check the content structure if there actually is structured content
            content = response.json.get('item_results', [])
            self.get_all_item_results_struct(content, k, v)

    def get_all_item_results_struct(self, content, k, v):
        if k == 'admin':
            # admin has to see all 150 item results
            self.assertEqual(len(content), 150)
        elif k == 'author' or 'data_analyst':
            # check out the permitted items
            items_permitted = self.get_permitted_items_for_member(v)
            item_ids = []
            # collect all ids that the author or data analyst is permitted to see
            for item in items_permitted:
                item_ids.append(item['id'])
            items_permitted = item_ids
            # the visible item results can be empty or something is visible
            self.assertGreaterEqual(len(content), 0)
            # it's technically possible that a member has no access to any item
            self.assertGreaterEqual(len(items_permitted), 0)
            # but if someone can see results, this must mean he or she has access to the item
            for item_result in content:
                self.assertIn(item_result['item_id'], items_permitted)

# ---------GET all item results regarding a specific format---------

    def get_all_item_results_w_format_fun(self, url, k, v, format):
        # accessed resource
        url = url + '?format=' + format
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid user
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            # invalid format
            if format == '123qwe':
                self.assertEqual(response.status_code, 404)
            # valid format and user
            else:
                displayed_items = response.json.get('item_results', [])
                if k == 'admin':
                    self.assertEqual(response.status_code, 200)
                elif k == 'author' or 'data_analyst':
                    self.assertEqual(response.status_code, 200)
                # check structure only if there is content
                self.get_all_item_results_w_format_struct(
                    displayed_items, k, v, format)

    def get_all_item_results_w_format_struct(self, content, k, v, format):
        if k == 'admin':
            # admin gets all item results from all different authors regarding a certain format
            self.assertLessEqual(len(content), 150)
        elif k == 'author' or 'data_analyst':
            # check out permitted items
            items_permitted = self.get_permitted_items_for_member(v)
        # collects all item formats and codes of displayed items in a list
        item_formats = []
        for item_result in content:
            if item_result['item']['format']['type'] not in item_formats:
                item_formats.append(item_result['item']['format']['type'])
            if item_result['item']['format']['code'] not in item_formats:
                item_formats.append(item_result['item']['format']['code'])
        # and this list has to be 0 (no items for a format exist)
        # or 2 (one type + one code)
        self.assertTrue(len(item_formats) == 0 or len(item_formats) == 2)
        # if it equals 2, it has to be the specified format
        if len(item_formats) > 0:
            self.assertIn(format, item_formats)

# ---------PUT an item by its id---------

    def put_item_by_id(self, k, v, access):
        for status, items in access.items():
            print(status)
            for item in items:
                data = {
                    'format' : 'multiple-choice',
                    'content' : {},
                    'solution' : {}
                }
                url = base_url + '/' + str(item)
                response = self.client.put(url, data=json.dumps(data), headers=v)
                self.print_information(url, k, response)
                self.assertEqual(response.status_code, status)


# ---------DELETE an item by its id---------

    def delete_item_by_id(self, k, v, access):
        pass

# ---------GET an item by its id---------

    def get_item_by_id(self, url, k, v):
        self.get_item_by_id_fun(url, k, v, random.randint(1, 500))
        self.get_item_by_id_fun(url, k, v, random.randint(501, 600))

    def get_item_by_id_fun(self, url, k, v, index):
        all_ids = []
        i = 1
        while i <= 500:
            all_ids.append(i)
            i += 1

        # accessed resource
        url = base_url + '/' + str(index)
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid user
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            if not 1 <= index <= 500:
                self.assertEqual(response.status_code, 404)
            else:
                if k == 'admin':
                    # admin can see every item
                    self.assertEqual(response.status_code, 200)
                    content = response.json.get('item')
                    # item id must be the same as the queried index
                    self.assertEqual(content['id'], index)
                    # check structure
                    self.get_item_by_id_struct(content, k, v)
                elif k == 'author' or k == 'data_analyst':
                    # author or data analyst can only see permitted items
                    items_permitted = self.get_permitted_items_for_member(v)
                    item_ids = []
                    # collect all item ids of permitted items
                    for item in items_permitted:
                        item_ids.append(item['id'])
                    items_permitted = item_ids
                    items_denied = all_ids
                    # with that knowledge, generate all ids of denied items
                    for item in items_permitted:
                        if item in items_denied:
                            items_denied.remove(item)
                    # check that the sum of both lengths equals 500 (amunt of all items)
                    self.assertEqual(len(items_denied) +
                                     len(items_permitted), 500)
                    # if author or data analyst are allowed to access the item, response should be 200
                    if index in items_permitted:
                        self.assertEqual(response.status_code, 200)
                        content = response.json.get('item')
                        self.assertEqual(content['id'], index)
                        self.get_item_by_id_struct(content, k, v)
                    elif index in items_denied:
                        self.assertEqual(response.status_code, 403)

    def get_item_by_id_struct(self, content, k, v):
        id = content['id']
        name = content['name']
        format_id = content['format_id']
        member_id = content['member_id']
        # add general assertions

# ---------GET all tests for an item---------

    def get_tests_for_item_id(self, url, k, v):
        self.get_tests_for_item_fun(url, k, v, random.randint(1, 500))
        self.get_tests_for_item_fun(url, k, v, random.randint(501, 600))

    def get_tests_for_item_fun(self, url, k, v, index):
        all_ids = []
        i = 1
        while i < 500:
            all_ids.append(i)
            i += 1

        # accessed resource
        url = base_url + '/' + str(index) + '/tests'
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid member
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            # invalid index
            if not 1 <= index <= 500:
                self.assertEqual(response.status_code, 404)
            # valid member and index
            else:
                # admin can see all 80 tests
                if k == 'admin':
                    self.assertEqual(response.status_code, 200)
                    content = response.json.get('tests')
                    self.get_tests_for_item_struct(content, k, v)
                # authors and data analysts can only see those tests,
                # where permitted items are used
                elif k == 'author' or k == 'data_analyst':
                    items_permitted = self.get_permitted_items_for_member(v)
                    item_ids = []
                    for item in items_permitted:
                        item_ids.append(item['id'])
                    items_permitted = item_ids
                    items_denied = all_ids
                    for item in items_permitted:
                        if item in items_denied:
                            items_denied.remove(item)
                    if index in items_permitted:
                        self.assertEqual(response.status_code, 200)
                        content = response.json.get('tests')
                        self.get_tests_for_item_struct(content, k, v)
                    elif index in items_denied:
                        self.assertEqual(response.status_code, 403)

    def get_tests_for_item_struct(self, content, k, v):
        pass

# ---------GET all results for an item---------

    def get_results_for_item_id(self, url, k, v):
        self.get_results_for_item_fun(url, k, v, random.randint(1, 500))
        self.get_results_for_item_fun(url, k, v, random.randint(501, 600))

    def get_results_for_item_fun(self, url, k, v, index):
        all_ids = []
        i = 1
        while i < 500:
            all_ids.append(i)
            i += 1

        # accessed resource
        url = base_url + '/' + str(index) + '/results'
        response = self.client.get(url, headers=v)
        self.print_information(url, k, response)

        # invalid member
        if k == 'unknown_user' or k == 'user':
            self.assertEqual(response.status_code, 403)
        else:
            # invalid index
            if not 1 <= index <= 500:
                self.assertEqual(response.status_code, 404)
            # valid member and index
            else:
                # admin can see all 150 results
                if k == 'admin':
                    self.assertEqual(response.status_code, 200)
                    content = response.json.get('results')
                    self.get_results_for_item_struct(content, k, v)
                # authors and data analysts can only see those results,
                # where permitted items are used
                elif k == 'author' or k == 'data_analyst':
                    items_permitted = self.get_permitted_items_for_member(v)
                    item_ids = []
                    for item in items_permitted:
                        item_ids.append(item['id'])
                    items_permitted = item_ids
                    items_denied = all_ids
                    for item in items_permitted:
                        if item in items_denied:
                            items_denied.remove(item)
                    if index in items_permitted:
                        self.assertEqual(response.status_code, 200)
                        content = response.json.get('results')
                        self.get_results_for_item_struct(content, k, v)
                    elif index in items_denied:
                        self.assertEqual(response.status_code, 403)

    def get_results_for_item_struct(self, url, k, v):
        pass
