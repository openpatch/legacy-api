from tests.test_case import BaseTestCase

from base64 import b64encode


class BaseTestApi(BaseTestCase):

    headers = {
        'admin': {
            'Authorization': 'Basic %s' % b64encode(b"admin:admin").decode("ascii"),
            'Content-Type' : 'application/json'
        },

        'author': {
            'Authorization': 'Basic %s' % b64encode(b"bob:b").decode("ascii"),
            'Content-Type' : 'application/json'
        },

        'data_analyst': {
            'Authorization': 'Basic %s' % b64encode(b"david:tobinski").decode("ascii"),
            'Content-Type' : 'application/json'
        },

        'user': {
            'Authorization': 'Basic %s' % b64encode(b"eve:e").decode("ascii"),
            'Content-Type' : 'application/json'
        },
        
        'unknown_user': {
            'Authorization': 'Basic %s' % b64encode(b"john:doe").decode("ascii"),
            'Content-Type' : 'application/json'
        }
    }
