from flask_testing import TestCase
from flask import Flask
from commoop import create_app, db
from commoop.utils.mock_db import setup_mock_db


class BaseTestCase(TestCase):
    def create_app(self):
        app = create_app('testing')
        return app

    def setUp(self):
        print('\n ***Database is getting created***')
        db.drop_all()
        db.create_all()
        setup_mock_db()
        db.session.commit()


    def tearDown(self):
        print('\n ***Database flushing***')
        db.session.remove()
        db.drop_all()
        print('***Database clear***')