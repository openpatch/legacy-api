FROM debian:stretch

# Install basic dependencies.
# The installation is intentionally split into two parts, so you can probably reuse the images created by building
# commoop-runner to save some disk space, since everything except docker is identical.
RUN apt-get update && \
	apt-get -y upgrade && \
	apt-get install -y \
	nginx build-essential python3-dev python3-pip python3-setuptools multitail vim

RUN apt-get install default-libmysqlclient-dev -y
# All of commoop-runner will be available under /var/www/.
COPY "." "/var/www/app"

WORKDIR /var/www/app

# Install python dependencies site-wide, since this is an isolated app in a container. We do not make use of a venv.
RUN pip3 install -r /var/www/app/requirements.txt

# Set up uwsgi and nginx. Remove nginx default configuration file:
RUN mkdir -p /var/log/uwsgi/
RUN ln -s /var/www/app/assets/configs/nginx.conf /etc/nginx/conf.d/
RUN rm /etc/nginx/sites-enabled/default
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Expose nginx default port 80
EXPOSE 80

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Give us r/w-access to the docker.sock, so we can deploy new runner-containers on the host-machine.
# Let him who hath understanding reckon the number of the beast. For it is a human number. -- Iron Maiden \m/
# Also start uwsgi and nginx - they will print the logs to stdout of the container. (Use 'docker logs' to read it)
# Beware: if you start the container without the detached-mode-flag (aka 'docker run -d'), the started process 
# will not allow the terminal emulator to exit by the popular key-combinations CTRL+D or CTRL+C. 
CMD uwsgi --ini /var/www/app/assets/configs/uwsgi.ini & \
	nginx