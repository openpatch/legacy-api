"""convert [] responses to {}

Revision ID: 3b344a180823
Revises: bc09cd919cb9
Create Date: 2018-09-02 11:58:37.236722

"""
import sqlalchemy as sa
from alembic import op
from commoop.models.item import Item
from sqlalchemy import orm

# revision identifiers, used by Alembic.
revision = '3b344a180823'
down_revision = 'bc09cd919cb9'
branch_labels = None
depends_on = None


def upgrade():
    bind = op.get_bind()
    session = orm.Session(bind=bind)
    items = session.query(Item)
    for item in items:
        if isinstance(item.responses, list):
            responses_dict = {}
            for i in range(0, len(item.responses)):
                responses_dict[str(i)] = item.responses[i]
            item.responses = responses_dict
    session.commit()


def downgrade():
    pass
