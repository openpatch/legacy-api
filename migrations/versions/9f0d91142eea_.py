"""cache item amount in test

Revision ID: 9f0d91142eea
Revises: 23006b7870ab
Create Date: 2018-09-13 14:08:24.251148

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '9f0d91142eea'
down_revision = '23006b7870ab'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        'test', sa.Column('item_groups_json', mysql.LONGTEXT(), nullable=True))
    op.add_column('test',
                  sa.Column('updated_item_on', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('test', 'updated_item_on')
    op.drop_column('test', 'item_groups_json')
    # ### end Alembic commands ###
