"""add survey_id and test_id to item_result

Revision ID: 0db5f0084cfc
Revises: 9f0d91142eea
Create Date: 2018-09-14 08:26:08.940974

"""
import sqlalchemy as sa
from alembic import op
from commoop.models.item_result import ItemResult
from sqlalchemy import orm
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '0db5f0084cfc'
down_revision = '9f0d91142eea'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('item_result',
                  sa.Column('survey_id', sa.Integer(), nullable=True))
    op.add_column('item_result',
                  sa.Column('test_id', sa.Integer(), nullable=True))
    op.add_column('item_result',
                  sa.Column('end_time', sa.DateTime(), nullable=True))
    op.add_column('item_result',
                  sa.Column('start_time', sa.DateTime(), nullable=True))
    op.create_foreign_key(None, 'item_result', 'survey', ['survey_id'], ['id'])
    op.create_foreign_key(None, 'item_result', 'test', ['test_id'], ['id'])

    bind = op.get_bind()
    session = orm.Session(bind=bind)
    item_results = session.query(ItemResult).all()
    for item_result in item_results:
        survey_id = item_result.item_group_result.test_result.survey_id
        test_id = item_result.item_group_result.test_result.test_id
        item_result.end_time = item_result.updated_on
        item_result.start_time = item_result.created_on
        item_result.survey_id = survey_id
        item_result.test_id = test_id
    session.commit()
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'item_result', type_='foreignkey')
    op.drop_constraint(None, 'item_result', type_='foreignkey')
    op.drop_column('item_result', 'test_id')
    op.drop_column('item_result', 'survey_id')
    op.drop_column('item_result', 'start_time')
    op.drop_column('item_result', 'end_time')
    # ### end Alembic commands ###
