"""insert common format

Revision ID: cbcf4a287e0f
Revises: 834b19dac3fb
Create Date: 2018-03-22 15:16:00.531479

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
from commoop.models.format import Format

# revision identifiers, used by Alembic.
revision = 'cbcf4a287e0f'
down_revision = '834b19dac3fb'
branch_labels = None
depends_on = None
Session = sessionmaker()


def upgrade():
    bind = op.get_bind()
    session = Session(bind=bind)

    com = Format(
        type='common',
        color_hex='FFCA28',
        code="com",
        solution_schema={
            '$schema': 'http://json-schema.org/draft-04/schema',
            'type': 'object'
        },
        content_schema={
            '$schema': 'http://json-schema.org/draft-04/schema',
            'type': 'object',
            'properties': {
                'public': {
                    'type': 'object',
                    'properties': {
                        'text': {
                            'type': 'string'
                        }
                    }
                },
                'private': {
                    'type': 'object',
                    'properties': {
                    }
                }
            }
        }
    )
    session.add(com)
    session.commit()


def downgrade():
    bind = op.get_bind()
    session = Session(bind=bind)
    com = Format.query.filter_by(type="common").first()
    session.delete(com)
    session.commit()
    pass
