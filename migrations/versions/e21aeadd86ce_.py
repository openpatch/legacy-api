"""merge common with instruction

Revision ID: e21aeadd86ce
Revises: 3b344a180823
Create Date: 2018-09-07 13:30:48.489100

"""
import sqlalchemy as sa
from alembic import op
from commoop.models.format import Format
from commoop.models.item import Item
from sqlalchemy import orm

# revision identifiers, used by Alembic.
revision = 'e21aeadd86ce'
down_revision = '3b344a180823'
branch_labels = None
depends_on = None


def upgrade():
    bind = op.get_bind()
    session = orm.Session(bind=bind)
    items = session.query(Item)
    format_instruction = session.query(Format).filter_by(
        type="instruction").first()
    format_common = session.query(Format).filter_by(type="common").first()
    for item in items:
        if item.format == format_instruction:
            item.format = format_common
    if format_instruction:
        session.delete(format_instruction)
    session.commit()


def downgrade():
    pass
