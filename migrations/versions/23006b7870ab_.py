"""add test_result correct

Revision ID: 23006b7870ab
Revises: b0740a8b36fb
Create Date: 2018-09-13 13:41:20.484945

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '23006b7870ab'
down_revision = 'b0740a8b36fb'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        'test_result',
        sa.Column('num_item_groups_correct', mysql.LONGTEXT(), nullable=True))
    op.add_column('test_result',
                  sa.Column('num_items_correct', sa.Integer(), nullable=True))
    op.add_column(
        'test_result',
        sa.Column('updated_correct_on', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('test_result', 'updated_correct_on')
    op.drop_column('test_result', 'num_items_correct')
    op.drop_column('test_result', 'num_item_groups_correct')
    # ### end Alembic commands ###
