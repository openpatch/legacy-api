import os
import unittest

from commoop import create_app
from commoop.database import db
from commoop.utils.member_command import MemberCommand
from commoop.utils.mock_db import setup_mock_db
from flask import url_for
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

app = create_app(config_name=os.getenv('COMMOOP_MODE', 'development'))
manager = Manager(app)
migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)
manager.add_command('member', MemberCommand)


@manager.command
def test():
    tests = unittest.TestLoader().discover('tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


@manager.command
def mock_db():
    db.drop_all()
    db.create_all()
    setup_mock_db()


@manager.command
def list_routes():
    import urllib
    output = []
    for rule in app.url_map.iter_rules():

        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        url = url_for(rule.endpoint, **options)
        line = urllib.parse.unquote("{:50s} {:20s} {}".format(
            rule.endpoint, methods, url))
        output.append(line)

    for line in sorted(output):
        print(line)


if __name__ == "__main__":
    manager.run()
