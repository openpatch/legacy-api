from flask import Flask
from commoop.database import db
# local import
from instance.config import app_config

# routes
from commoop.api.v1 import api as api_v1
from commoop.auth import auth


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)

    # register api endpoints
    app.register_blueprint(api_v1, url_prefix="/v1")

    return app
