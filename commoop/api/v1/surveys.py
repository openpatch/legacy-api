from datetime import date, datetime, timedelta

from commoop.api.v1 import api
from commoop.auth import auth
from commoop.database import db
from commoop.models.member import Member
from commoop.models.survey import Survey
from commoop.models.test import Test
from flask import abort, g, json, request

base_url = '/surveys'


@api.route(base_url, methods=['POST', 'GET'])
@auth.login_required
def surveys():
    if request.method == 'POST':
        return created_survey()
    elif request.method == 'GET':
        return get_all_surveys()
    abort(400)


def created_survey():
    """
    @api {post} /v1/surveys Adding a new survey.
    @apiVersion 1.0.0
    @apiName CreateNewSurvey
    @apiGroup Surveys

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiParam {String} city The name of the city where the survey will be administered.
    @apiParam {String} term The term when the survey will be administered.
    @apiParam {Number} test_id The id of the test the survey is based on.
    @apiParam {String} [starts_on] The day when the survey should be accessible.
    @apiParam {String} [ends_on] The day when the survey will shut down.


    @apiSuccess {Object} status An object containing status, survey name and survey id.
    @apiError 422:InsufficientDetails Someone tried to create a survey but didn't provide the necessary details.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role == 'admin' or role == 'author':
            city = request.json.get('city')
            term = request.json.get('term')
            member = current_member

            # set start value
            starts_on = request.json.get('starts_on')
            if starts_on == None:
                starts_on = date.today()
            else:
                try:
                    starts_on = datetime.strptime(starts_on, '%Y-%m-%d')
                    starts_on = starts_on.date()
                except ValueError:
                    abort(400)

            # set end value
            ends_on = request.json.get('ends_on')
            if ends_on == None:
                ends_on = starts_on + timedelta(days=30)
            else:
                try:
                    ends_on = datetime.strptime(ends_on, '%Y-%m-%d')
                    ends_on = ends_on.date()
                except ValueError:
                    abort(400)

            # abort if end date preceeds start date
            if ends_on < starts_on:
                abort(422)

            test = request.json.get('test_id')
            test = Test.query.filter_by(id=test).first()
            name = request.json.get('name')
            greeting = request.json.get('greeting')
            farewell = request.json.get('farewell')
            show_session_id = request.json.get('show_session_id')
            show_progress = request.json.get('show_progress')
            show_result = request.json.get('show_result')
            if member and test and name:
                survey = Survey(
                    city=city,
                    term=term,
                    greeting=greeting,
                    farewell=farewell,
                    show_session_id=show_session_id,
                    show_progress=show_progress,
                    show_result=show_result,
                    member=member,
                    members_permitted=[member],
                    test=test,
                    name=name,
                    starts_on=starts_on,
                    ends_on=ends_on)
                password = request.json.get('password')
                if password:
                    survey.hash_password(password)
                db.session.add(survey)
                db.session.commit()
                return json.dumps({
                    'status': 'created',
                    'survey_name': survey.name,
                    'survey_id': survey.id
                })
            else:
                abort(422)
    abort(403)


def get_all_surveys():
    """
    @api {get} /v1/surveys Get all surveys.
    @apiVersion 1.0.0
    @apiName GetAllSurveys
    @apiGroup Surveys
    @apiDescription Returns a list of all surveys that exist. Admins can see every survey,
    all other members can see the ones they created or need permissions to see more.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} surveys The surveys the current member is allowed to see.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role != 'user':
            surveys = []
            if role == 'admin':
                surveys = Survey.query.all()
            elif role == 'author' or role == 'data_analyst':
                surveys = current_member.surveys_permitted
            return json.dumps({
                'surveys': [survey.as_dict() for survey in surveys]
            })
    abort(403)


@api.route(base_url + '/<survey_id>', methods=['GET', 'PUT', 'DELETE'])
@auth.login_required
def survey_by_id(survey_id):
    if request.method == 'GET':
        return get_survey_by_id(survey_id)
    elif request.method == 'PUT':
        return put_survey_by_id(survey_id)
    elif request.method == 'DELETE':
        return delete_survey_by_id(survey_id)


def get_survey_by_id(survey_id):
    """
    @api {get} /v1/surveys/:id Get surveys by id.
    @apiVersion 1.0.0
    @apiName GetSurveyById
    @apiGroup Surveys
    @apiParam {Number} id The unique survey id.
    @apiDescription Returns information about the survey with the requested id.
    Admins can see every survey, authors can see their own surveys otherwise they need
    specific permissions, just like data analysts.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object} survey The information about a survey as survey object.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a survey without the necessary permissions.
    @apiError 404:NoSuchSurveyError Someone tried to access an invalid survey id.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role != 'user':
            survey = Survey.query.filter_by(id=survey_id).first()
            if survey:
                if role == 'admin':
                    pass
                elif role == 'author' or role == 'data_analyst':
                    if survey not in current_member.surveys_permitted:
                        abort(403)
                return json.dumps({'survey': survey.as_dict()})
            abort(404)
    abort(403)


def put_survey_by_id(survey_id):
    """
    @api {put} /v1/surveys/:id Overwrite a specific survey.
    @apiVersion 1.0.0
    @apiName PutSurveyById
    @apiGroup Surveys
    @apiParam {Number} id The unique survey id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiError 403:PermissionError The member tried to overwrite an survey without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to overwrite an invalid survey id.
    @apiError 405:OperationNotAllowedError The member tried to overwrite an survey which is currently used.
    """
    survey = Survey.query.get(survey_id)
    if not survey:
        abort(404)

    current_member = g.member
    if current_member == survey.member or current_member.role.type == 'admin':
        if survey.results:
            abort(405)
        survey.name = request.json.get('name')
        survey.city = request.json.get('city')
        survey.term = request.json.get('term')
        survey.greeting = request.json.get('greeting')
        survey.farewell = request.json.get('farewell')
        survey.show_session_id = request.json.get('show_session_id')
        survey.show_progress = request.json.get('show_progress')
        survey.show_result = request.json.get('show_result')

        # set start value
        starts_on = request.json.get('starts_on')
        if starts_on == None:
            starts_on = date.today()
        else:
            try:
                starts_on = datetime.strptime(starts_on, '%Y-%m-%d')
                starts_on = starts_on.date()
            except ValueError:
                abort(400)

        # set end value
        ends_on = request.json.get('ends_on')
        if ends_on == None:
            ends_on = starts_on + timedelta(days=30)
        else:
            try:
                ends_on = datetime.strptime(ends_on, '%Y-%m-%d')
                ends_on = ends_on.date()
            except ValueError:
                abort(400)

            # abort if end date preceeds start date
            if ends_on < starts_on:
                abort(422)

        survey.starts_on = starts_on
        survey.ends_on = ends_on
        new_password = request.json.get('password')
        password_protection = request.json.get('password_protection')
        if not password_protection:
            survey.password_hash = None
        elif new_password:
            survey.hash_password(new_password)
        new_test_id = request.json.get('test_id')
        new_test = Test.query.get(new_test_id)
        if new_test:
            survey.test = new_test
        db.session.commit()
        return json.dumps({'status': 'ok'})
    abort(403)


def delete_survey_by_id(survey_id):
    """
    @api {delete} /v1/surveys/:id Delete a specific item.
    @apiVersion 1.0.0
    @apiName DeleteSurveyById
    @apiGroup Surveys
    @apiParam {Number} id The unique survey id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiError 403:PermissionError The member tried to delete an survey without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to delete an invalid survey id.
    @apiError 405:OperationNotAllowedError The member tried to delete an survey which is currently used.
    """
    survey = Survey.query.get(survey_id)
    if not survey:
        abort(404)

    current_member = g.member
    if current_member == survey.member or current_member.role.type == 'admin':
        if survey.results:
            abort(405)
        db.session.delete(survey)
        db.session.commit()
        return json.dumps({'status': 'ok'})
    abort(403)


@api.route(base_url + '/<survey_id>/results', methods=['GET', 'DELETE'])
@auth.login_required
def results_for_survey(survey_id):
    if request.method == 'GET':
        return get_results_for_survey(survey_id)
    elif request.method == 'DELETE':
        return delete_results_for_survey(survey_id)


def delete_results_for_survey(survey_id):
    """
    @api {get} /v1/surveys/:id/results Delete results of a survey.
    @apiVersion 1.0.0
    @apiName DeleteResultsBySurveyId
    @apiGroup Surveys
    @apiParam {Number} id The unique survey id.
    @apiDescription Deletes all results of a survey.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} results The results in the survey with the provided id.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a survey without the necessary permissions.
    @apiError 404:NoSuchSurveyError Someone tried to access an invalid survey id.
    """
    current_member = g.member

    survey = Survey.query.get(survey_id)

    if not survey:
        abort(404)

    if not survey.is_permitted_for_write(current_member):
        abort(403)

    for test_result in survey.results:
        for item_group_result in test_result.item_group_results:
            for item_result in item_group_result.item_results:
                db.session.delete(item_result)
                db.session.commit()
            db.session.delete(item_group_result)
            db.session.commit()
        db.session.delete(test_result)
        db.session.commit()
    db.session.commit()

    return json.dumps({'status': 'ok'})


def get_results_for_survey(survey_id):
    """
    @api {get} /v1/surveys/:id/results Get results of a survey.
    @apiVersion 1.0.0
    @apiName GetResultsBySurveyId
    @apiGroup Surveys
    @apiParam {Number} id The unique survey id.
    @apiDescription Returns a list of results that were generated by a survey that used this survey.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} results The results in the survey with the provided id.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a survey without the necessary permissions.
    @apiError 404:NoSuchSurveyError Someone tried to access an invalid survey id.
    """
    current_member = g.member

    survey = Survey.query.get(survey_id)

    if not survey:
        abort(404)

    if not survey.is_permitted_for_read(current_member):
        abort(403)

    return json.dumps({
        'test_results':
        [test_result.as_dict() for test_result in survey.results]
    })


@api.route(base_url + '/<survey_id>/permissions', methods=['GET'])
@auth.login_required
def get_members_permitted_for_survey(survey_id):
    current_member = g.member

    survey = Survey.query.get(survey_id)
    if not survey:
        abort(404)

    return json.dumps({
        'members': [member.as_dict() for member in survey.members_permitted]
    })


@api.route(
    base_url + '/<survey_id>/permissions/<member>', methods=['PUT', 'DELETE'])
@auth.login_required
def member_permission_for_survey(survey_id, member):
    if request.method == 'PUT':
        return put_member_permission_for_survey(survey_id, member)
    elif request.method == 'DELETE':
        return delete_member_permission_for_survey(survey_id, member)
    abort(400)


def put_member_permission_for_survey(survey_id, member_identifier):
    """
    @api {put} /v1/surveys/:id/permissions/:member Add a member to the permitted ones for the survey
    @apiVersion 1.0.0
    @apiName PutMemberPermissionForSurvey
    @apiGroup Surveys
    @apiParam {Number} id The unique survey id.
    @apiParam {Number} member_identifier The unique member id or the unique username.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} status OK
    @apiError 403:PermissionError The member tried to access an survey without sufficient permissions.
    @apiError 404:NoSuchSurveyError The member tried to access an invalid survey id.
    """
    current_member = g.member
    survey = Survey.query.get(survey_id)

    if current_member.role.type != 'admin' and current_member != survey.member:
        abort(403)

    member = Member.query.get(member_identifier)

    if not member:
        member = Member.query.filter_by(username=member_identifier).first()

    if not survey or not member:
        abort(404)

    if member not in survey.members_permitted:
        survey.members_permitted.append(member)
        db.session.commit()

    return json.dumps({'member_id': member.id})


def delete_member_permission_for_survey(survey_id, member_identifier):
    """
    @api {delete} /v1/surveys/:id/permissions/:member Remove a member from the permitted ones for the survey
    @apiVersion 1.0.0
    @apiName DeleteMemberPermissionForSurvey
    @apiGroup Surveys
    @apiParam {Number} id The unique survey id.
    @apiParam {Number} member_identifier The unique member id or the unique username.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} status OK
    @apiError 403:PermissionError The member tried to access an survey without sufficient permissions.
    @apiError 404:NoSuchSurveyError The member tried to access an invalid survey id.
    """
    current_member = g.member
    survey = Survey.query.get(survey_id)

    if current_member.role.type != 'admin' and current_member != survey.member:
        abort(403)

    member = Member.query.get(member_identifier)

    if not member:
        member = Member.query.filter_by(username=member_identifier).first()

    if not survey or not member:
        abort(404)

    if member in survey.members_permitted:
        survey.members_permitted.remove(member)
        db.session.commit()

    return json.dumps({'member_id': member.id})


@api.route(base_url + '/<survey_id>/test', methods=['GET'])
@auth.login_required
def get_survey_test_by_id(survey_id):
    """
    @api {get} /v1/surveys/:id/test Get surveys by id.
    @apiVersion 1.0.0
    @apiName GetSurveyTestById
    @apiGroup Surveys
    @apiParam {Number} id The unique survey id.
    @apiDescription Returns information about the test of a survey with the requested id.
    Admins can see every survey, authors can see their own surveys otherwise they need
    specific permissions, just like data analysts.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object} survey The information about a survey as survey object.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a survey without the necessary permissions.
    @apiError 404:NoSuchSurveyError Someone tried to access an invalid survey id.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role != 'user':
            survey = Survey.query.filter_by(id=survey_id).first()
            if survey:
                if role == 'admin':
                    pass
                elif role == 'author' or role == 'data_analyst':
                    if survey not in current_member.surveys_permitted:
                        abort(403)
                return json.dumps({'test': survey.test.as_dict()})
            abort(404)
    abort(403)


@api.route(base_url + '/<survey_id>/items', methods=['GET'])
@auth.login_required
def get_items_of_survey(survey_id):
    """
    @api {get} /v1/tests/:id/items Get items of a test.
    @apiVersion 1.0.0
    @apiName GetItemsBySurveyId
    @apiGroup Surveys
    @apiParam {Number} id The unique survey id.
    @apiDescription Returns a list of items that are used in the test with the requested id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} items The items in the test with the provided id.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a test without the necessary permissions.
    @apiError 404:NoSuchTestError Someone tried to access an invalid test id.
    """
    current_member = g.member

    survey = Survey.query.get(survey_id)

    if not survey:
        abort(404)

    if not survey.is_permitted_for_read(current_member):
        abort(403)

    filters = {'survey_ids': [int(survey_id)]}

    return json.dumps({
        'items':
        [item.as_dict(filters=filters) for item in survey.test.get_items()]
    })
