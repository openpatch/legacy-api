from flask import Blueprint
from flask_cors import CORS

api = Blueprint(
    'api',
    __name__,
    template_folder='templates',
    static_folder='static'
)

CORS(api)

from . import formats
from . import items
from . import members
from . import surveys
from . import tests
from . import sessions