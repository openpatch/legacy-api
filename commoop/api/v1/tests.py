import ast
import csv
import io

from commoop.api.v1 import api
from commoop.auth import auth
from commoop.database import db
from commoop.models.item import Item
from commoop.models.item_group import ItemGroup
from commoop.models.member import Member
from commoop.models.survey import Survey
from commoop.models.test import Test
from commoop.models.test_result import TestResult
from flask import abort, g, json, make_response, request

base_url = '/tests'


@api.route(base_url, methods=['POST', 'GET'])
@auth.login_required
def tests():
    if request.method == 'POST':
        return created_test()
    elif request.method == 'GET':
        return get_all_tests()
    abort(400)


def created_test():
    """
    @api {post} /v1/tests Adding a new test.
    @apiVersion 1.0.0
    @apiName CreateNewTest
    @apiGroup Tests

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiParam {String} name The name of the test.
    @apiParam {Object[]} item_groups The item groups that are used in this test.

    @apiSuccess {Object} status An object containing status and either new test id or the faulty item id.
    @apiError 422:InsufficientDetails Someone tried to create a test but didn't provide the necessary details.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role == 'admin' or role == 'author':
            name = request.json.get('name')
            member = current_member
            item_groups = request.json.get('item_groups')
            if name and member:
                test = Test(
                    member=member, name=name, members_permitted=[member])
                for item_group in item_groups:
                    ig_items = item_group.get('items', None)
                    if not ig_items:
                        continue
                    ig = ItemGroup(
                        is_randomized=item_group.get('is_randomized', False),
                        is_adaptive=item_group.get('is_adaptive', False),
                        is_jumpable=item_group.get('is_jumpable', False),
                        needs_to_be_correct=item_group.get(
                            'needs_to_be_correct', False),
                        tests=[test])
                    for item in ig_items:
                        current_item = Item.query.filter_by(
                            id=int(item)).first()
                        if not current_item:
                            return json.dumps({
                                'status': 'failed',
                                'failed_item_id': item,
                            })
                        ig.items.append(current_item)
                    db.session.add(ig)
                db.session.add(test)
                db.session.commit()
                return json.dumps({'status': 'created', 'test_id': test.id})
            else:
                abort(422)
    abort(403)


def get_all_tests():
    """
    @api {get} /v1/tests Get all tests.
    @apiVersion 1.0.0
    @apiName GetAllTests
    @apiGroup Tests
    @apiDescription Returns a list of all tests.
    Admins can see all existing tests. Authors can by default see only the tests they created.
    Data analysts can see no test at all. Authors and data analysts can get access to tests,
    when some admin updates their test permissions.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} tests The tests a member has permission to see.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        tests = []
        if role != 'user':
            if role == 'admin':
                tests = Test.query.all()
            elif role == 'author' or role == 'data_analyst':
                tests = current_member.tests_permitted
            return json.dumps({'tests': [test.as_dict() for test in tests]})
    abort(403)


@api.route(base_url + '/<test_id>', methods=['GET', 'PUT', 'DELETE'])
@auth.login_required
def test_by_id(test_id):
    if request.method == 'GET':
        return get_test_by_id(test_id)
    elif request.method == 'PUT':
        return put_test_by_id(test_id)
    elif request.method == 'DELETE':
        return delete_test_by_id(test_id)
    abort(400)


def get_test_by_id(test_id):
    """
    @api {get} /v1/tests/:id Get test by id.
    @apiVersion 1.0.0
    @apiName GetTestById
    @apiGroup Tests
    @apiParam {Number} id The unique test id.
    @apiParam {Number} [survey_ids[]] survey id used for filtering
    @apiParam {Number} [result_ids[]] result id used for filtering
    @apiDescription Returns information about the test with the requested id.
    Admins can see every test, authors can see their own tests otherwise they need
    specific permissions, just like data analysts.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object} test The information about a test as test object.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a test without the necessary permissions.
    @apiError 404:NoSuchTestError Someone tried to access an invalid test id.
    """
    current_member = g.member

    survey_ids = request.args.getlist('survey_ids[]')
    result_ids = request.args.getlist('result_ids[]')
    filters = {}
    if survey_ids or result_ids:
        filters = {
            'survey_ids': [int(s) for s in survey_ids],
            'result_ids': [int(r) for r in result_ids],
        }

    test = Test.query.get(test_id)

    if not test:
        abort(404)

    if not test.is_permitted_for_read(current_member):
        abort(403)

    return json.dumps({'test': test.as_dict(filters=filters)})


@api.route(base_url + '/<test_id>/export', methods=['GET'])
@auth.login_required
def export_test_by_id(test_id):
    """
    @api {get} /v1/tests/:id/export Get test by id.
    @apiVersion 1.0.0
    @apiName ExportTestById
    @apiGroup Tests
    @apiParam {Number} id The unique test id.
    @apiDescription Returns information about the test with the requested id.
    Admins can see every test, authors can see their own tests otherwise they need
    specific permissions, just like data analysts.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object} test The information about a test as test object.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a test without the necessary permissions.
    @apiError 404:NoSuchTestError Someone tried to access an invalid test id.
    """

    current_member = g.member
    test = Test.query.filter_by(id=test_id).first()

    if not test:
        abort(404)

    if not test.is_permitted_for_read(current_member):
        abort(403)

    dict_test = test.as_dict()

    for ig in dict_test.get('item_groups', []):
        items = []
        for item in ig.get('items', []):
            item = Item.query.get(item)
            items.append(item.as_dict())
        ig['items'] = items

    return json.dumps({'test': dict_test})


def put_test_by_id(test_id):
    """
    @api {put} /v1/tests/:id Overwrite a specific test.
    @apiVersion 1.0.0
    @apiName PutTestById
    @apiGroup Tests
    @apiParam {Number} id The unique test id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiError 403:PermissionError The member tried to overwrite an test without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to overwrite an invalid test id.
    @apiError 405:OperationNotAllowedError The member tried to overwrite an test which is currently used.
    """
    test = Test.query.get(test_id)
    if not test:
        abort(404)

    current_member = g.member
    if current_member == test.member or current_member.role.type == 'admin':
        if test.results:
            abort(405)
        test.name = request.json.get('name')
        item_groups = request.json.get('item_groups')
        # clear old item groups
        for item_group in test.item_groups:
            db.session.delete(item_group)
        # insert new item groups

        item_amount = 0
        for item_group in item_groups:
            ig_items = item_group.get('items', None)
            if not ig_items:
                continue
            ig = ItemGroup(
                is_randomized=item_group.get('is_randomized', False),
                is_adaptive=item_group.get('is_adaptive', False),
                is_jumpable=item_group.get('is_jumpable', False),
                needs_to_be_correct=item_group.get('needs_to_be_correct',
                                                   False),
                tests=[test])
            for item in ig_items:
                current_item = Item.query.filter_by(id=int(item)).first()
                if not current_item:
                    return json.dumps({
                        'status': 'failed',
                        'failed_item_id': item,
                    })
                item_amount += 1
                ig.items.append(current_item)
            db.session.add(ig)
            test.item_groups.append(ig)
        test.item_amount = item_amount
        db.session.commit()
        return json.dumps({'status': 'ok'})
    abort(403)


def delete_test_by_id(test_id):
    """
    @api {delete} /v1/tests/:id Delete a specific item.
    @apiVersion 1.0.0
    @apiName DeleteTestById
    @apiGroup Tests
    @apiParam {Number} id The unique test id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiError 403:PermissionError The member tried to delete an test without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to delete an invalid test id.
    @apiError 405:OperationNotAllowedError The member tried to delete an test which is currently used.
    """
    test = Test.query.get(test_id)
    if not test:
        abort(404)

    current_member = g.member
    if current_member == test.member or current_member.role.type == 'admin':
        if test.surveys or test.results:
            abort(405)
        for item_group in test.item_groups:
            db.session.delete(item_group)
        db.session.delete(test)
        db.session.commit()
        return json.dumps({'status': 'ok'})
    abort(403)


@api.route(base_url + '/<test_id>/items', methods=['GET'])
@auth.login_required
def get_items_of_test(test_id):
    """
    @api {get} /v1/tests/:id/items Get items of a test.
    @apiVersion 1.0.0
    @apiName GetItemsByTestId
    @apiGroup Tests
    @apiParam {Number} id The unique test id.
    @apiDescription Returns a list of items that are used in the test with the requested id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} items The items in the test with the provided id.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a test without the necessary permissions.
    @apiError 404:NoSuchTestError Someone tried to access an invalid test id.
    """
    current_member = g.member

    test = Test.query.filter_by(id=test_id).first()

    if not test:
        abort(404)

    if not test.is_permitted_for_read(current_member):
        abort(403)

    filters = {
        'test_ids': [int(test_id)],
        'survey_ids': [int(s) for s in request.args.getlist('survey_ids[]')]
    }

    return json.dumps({
        'items': [item.as_dict(filters=filters) for item in test.get_items()]
    })


@api.route(base_url + '/<test_id>/surveys', methods=['GET'])
@auth.login_required
def get_surveys_of_test(test_id):
    """
    @api {get} /v1/tests/:id/surveys Get surveys for a specific test.
    @apiVersion 1.0.0
    @apiName GetSurveysByTestId
    @apiGroup Tests
    @apiParam {Number} id The unique test id.
    @apiDescription Returns a list of surveys that were administered based on the test with the requested id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} surveys The surveys regarding the test with the provided id.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a test without the necessary permissions.
    @apiError 404:NoSuchTestError Someone tried to access an invalid test id.
    """
    current_member = g.member

    test = Test.query.get(test_id)

    if not test:
        abort(404)

    if not test.is_permitted_for_read(current_member):
        abort(403)

    surveys = []
    for survey in test.surveys:
        if survey.is_permitted_for_read(current_member):
            surveys.append(survey)

    return json.dumps({'surveys': [survey.as_dict() for survey in surveys]})


@api.route(base_url + '/<test_id>/results', methods=['GET'])
@auth.login_required
def results_for_test(test_id):
    """
    @api {get} /v1/tests/:id/results Get results of a test.
    @apiVersion 1.0.0
    @apiName GetResultsByTestId
    @apiGroup Tests
    @apiParam {Number} id The unique test id.
    @apiDescription Returns a list of results that were generated by a survey that used this test.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} results The results in the test with the provided id.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a test without the necessary permissions.
    @apiError 404:NoSuchTestError Someone tried to access an invalid test id.
    """
    current_member = g.member

    test = Test.query.filter_by(id=test_id).first()

    if not test:
        abort(404)

    if not test.is_permitted_for_read(current_member):
        abort(403)

    test_results = []
    for survey in test.surveys:
        if survey.is_permitted_for_read(current_member):
            test_results += survey.results

    return json.dumps({
        'test_results':
        [test_result.as_dict() for test_result in test_results]
    })


@api.route(base_url + '/<test_id>/results/export', methods=['GET'])
@auth.login_required
def export_results_for_test(test_id):
    """
    @api {get} /v1/tests/:id/results/export Get test export by id.
    @apiVersion 1.0.0
    @apiName GetExportResultsForTest
    @apiGroup Tests
    @apiParam {Number} id The unique test id.
    @apiParam {String} format The format type: csv or json
    @apiParam {String} filter List of result id e.g.: [1,2,3,4]
    @apiDescription Returns information about the test results with the requested id.
    Admins can see every test, authors can see their own tests otherwise they need
    specific permissions, just like data analysts.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object} data The result information about a test.
    @apiSuccess {Object} header The header information about a test.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a test without the necessary permissions.
    @apiError 404:NoSuchTestError Someone tried to access an invalid test id.
    """
    current_member = g.member
    format = request.args.get('format', 'json')
    filter = request.args.get('filter', None)
    try:
        filter = ast.literal_eval(filter)
    except:
        filter = None

    test = Test.query.get(test_id)
    if not test:
        abort(404)

    if not test.is_permitted_for_read(current_member):
        abort(403)

    if format == 'json':
        test_results = []
        for test_result in test.results:
            if not filter or test_result.id in filter:
                test_result = test_result.as_dict(nested=True)
                test_results.append(test_result)
        return json.dumps({'test_results': test_results})
    elif format == 'csv':
        test_results = []
        headers = set([])
        for test_result in test.results:
            if not filter or test_result.id in filter:
                export_test_result = test_result.export()
                test_results.append(export_test_result['data'])
                headers = headers.union(export_test_result['headers'])
        headers = sorted(headers)
        si = io.StringIO()
        cw = csv.DictWriter(si, fieldnames=headers, quoting=csv.QUOTE_ALL)
        cw.writeheader()
        for test_result in test_results:
            cw.writerow(test_result)
        response = make_response(si.getvalue())
        response.headers["Access-Control-Allow-Origin"] = "*"
        response.headers["Content-Type"] = "text/csv"
        return response

    return abort(422)


@api.route(base_url + '/results', methods=['DELETE'])
@auth.login_required
def delete_results():
    current_member = g.member
    result_ids = request.json.get('ids', [])
    for result_id in result_ids:
        test_result = TestResult.query.get(result_id)
        if not test_result.is_permitted_for_write(current_member):
            db.session.rollback()
            abort(403)
        if not test_result:
            db.session.rollback()
            abort(404)
        db.session.delete(test_result)
    db.session.commit()
    return json.dumps({'status': 'ok'})


@api.route(base_url + '/results/<result_id>', methods=['GET'])
@auth.login_required
def test_result_by_id(result_id):
    """
    @api {get} /v1/tests/results/:id Get a result of a test.
    @apiVersion 1.0.0
    @apiName GetTestResultById
    @apiGroup Tests
    @apiParam {Number} id The unique result id.
    @apiDescription Returns a result that were generated by a survey that used this test.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} result The result in the test with the provided id.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a test without the necessary permissions.
    @apiError 404:NoSuchTestError Someone tried to access an invalid test id.
    """
    current_member = g.member
    test_result = TestResult.query.get(result_id)
    if not test_result:
        abort(404)

    if not test_result.is_permitted_for_read(current_member):
        abort(403)

    return json.dumps({'test_result': test_result.as_dict()})


@api.route(base_url + '/results/<result_id>/item_results', methods=['GET'])
@auth.login_required
def item_results_for_test_result(result_id):
    """
    @api {get} /v1/tests/results/:id/item_results Get the item results of a test result.
    @apiVersion 1.0.0
    @apiName GetItemResultsForTestResult
    @apiGroup Tests
    @apiParam {Number} id The unique result id.
    @apiDescription Returns the item results that were generated by a survey that used this test.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} result The item result in the test with the provided id.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about a test without the necessary permissions.
    @apiError 404:NoSuchTestError Someone tried to access an invalid test id.
    """
    current_member = g.member
    test_result = TestResult.query.get(result_id)
    if not test_result:
        abort(404)

    if not test_result.is_permitted_for_read(current_member):
        abort(403)

    item_results = test_result.get_item_results()

    return json.dumps({
        'results': [item_result.as_dict() for item_result in item_results]
    })


@api.route(base_url + '/<test_id>/permissions', methods=['GET'])
@auth.login_required
def get_members_permitted_for_test(test_id):
    current_member = g.member

    test = Test.query.get(test_id)

    if not test.is_permitted_for_read(current_member):
        abort(403)

    if not test:
        abort(404)

    return json.dumps({
        'members': [member.as_dict() for member in test.members_permitted]
    })


@api.route(
    base_url + '/<test_id>/permissions/<member>', methods=['PUT', 'DELETE'])
@auth.login_required
def member_permission_for_test(test_id, member):
    if request.method == 'PUT':
        return put_member_permission_for_test(test_id, member)
    elif request.method == 'DELETE':
        return delete_member_permission_for_test(test_id, member)
    abort(400)


def put_member_permission_for_test(test_id, member_identifier):
    """
    @api {put} /v1/tests/:id/permissions/:member Add a member to the permitted ones for the test
    @apiVersion 1.0.0
    @apiName PutMemberPermissionForTest
    @apiGroup Tests
    @apiParam {Number} id The unique test id.
    @apiParam {Number} member_identifier The unique member id or the unique username.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} status OK
    @apiError 403:PermissionError The member tried to access an test without sufficient permissions.
    @apiError 404:NoSuchTestError The member tried to access an invalid test id.
    """
    current_member = g.member
    test = Test.query.get(test_id)

    if current_member.role.type != 'admin' and current_member != test.member:
        abort(403)

    member = Member.query.get(member_identifier)

    if not member:
        member = Member.query.filter_by(username=member_identifier).first()

    if not test or not member:
        abort(404)

    if member not in test.members_permitted:
        test.members_permitted.append(member)
        db.session.commit()

    return json.dumps({'member_id': member.id})


def delete_member_permission_for_test(test_id, member_identifier):
    """
    @api {delete} /v1/tests/:id/permissions/:member Remove a member from the permitted ones for the test
    @apiVersion 1.0.0
    @apiName DeleteMemberPermissionForTest
    @apiGroup Tests
    @apiParam {Number} id The unique test id.
    @apiParam {Number} member_identifier The unique member id or the unique username.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} status OK
    @apiError 403:PermissionError The member tried to access an test without sufficient permissions.
    @apiError 404:NoSuchTestError The member tried to access an invalid test id.
    """
    current_member = g.member
    test = Test.query.get(test_id)

    if current_member.role.type != 'admin' and current_member != test.member:
        abort(403)

    member = Member.query.get(member_identifier)

    if not member:
        member = Member.query.filter_by(username=member_identifier).first()

    if not test or not member:
        abort(404)

    if member in test.members_permitted:
        test.members_permitted.remove(member)
        db.session.commit()

    return json.dumps({'member_id': member.id})
