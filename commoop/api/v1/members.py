import re
from uuid import uuid4

from commoop.api.v1 import api
from commoop.auth import auth
from commoop.database import db
from commoop.models.item import Item
from commoop.models.member import Member
from commoop.models.role import Role
from commoop.models.survey import Survey
from commoop.models.test import Test
from flask import abort, g, json, request
from sqlalchemy.exc import IntegrityError

base_url = '/members'

# following simplified RFC 2822 see https://stackoverflow.com/a/1373724/4205043
EMAIL_REGEX = re.compile(
    r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
)


@api.route(base_url, methods=['POST', 'GET'])
@auth.login_required
def members():
    if request.method == 'POST':
        return created_new_member()
    elif request.method == 'GET':
        return get_all_members()
    abort(400)


def created_new_member():
    """
    @api {post} /v1/members Adding a new member.
    @apiVersion 1.0.0
    @apiName CreateNewMember
    @apiGroup Members

    @apiUse Authorization
    @apiPermission admin
    @apiParam {String} username The username of the member.
    @apiParam {String} password The username of the member.
    @apiParam {String} [first_name] The optional first name of the member.
    @apiParam {String} [last_name] The optional last name of the member.
    @apiParam {String} email The username of the member.
    @apiParam {String} role The username of the member.

    @apiSuccess {Object} status An object containing status and either new member id or detailed error description.
    @apiError 422:InsufficientDetails Someone tried to create a member but didn't provide the necessary details.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role == 'admin':
            username = request.json.get('username')
            password = request.json.get('password')
            first_name = request.json.get('first_name')
            last_name = request.json.get('last_name')
            email = request.json.get('email')
            if not EMAIL_REGEX.match(email):
                abort(422)
            role = request.json.get('role')
            role = Role.query.filter_by(type=role).first()
            if not (role and username and password and email):
                abort(422)
            member = Member(
                username=username,
                first_name=first_name,
                last_name=last_name,
                email=email,
                role=role)
            member.hash_password(password)
            try:
                db.session.add(member)
                db.session.commit()
                return json.dumps({
                    'status': 'created',
                    'member_id': member.id
                })
            except IntegrityError as e:
                db.session.rollback()
                return json.dumps({
                    'status': 'existing',
                    'detail': str(e.orig)
                })
    abort(403)


def get_all_members():
    """
    @api {get} /v1/members List all members.
    @apiVersion 1.0.0
    @apiName GetAllMembers
    @apiGroup Members

    @apiUse Authorization
    @apiPermission admin

    @apiSuccess {Object[]} members The list of members.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role == 'admin':
            members = Member.query.all()

            return json.dumps({
                'members': [member.as_dict() for member in members]
            })
    abort(403)


@api.route(base_url + '/me', methods=['GET'])
@auth.login_required
def get_me():
    """
    @api {get} /v1/members/me Show current member.
    @apiVersion 1.0.0
    @apiName GetMe
    @apiGroup Members

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst
    @apiPermission user

    @apiSuccess {Object} me Information about the current user as member object.
    """
    me = g.member
    if me.role:
        return json.dumps({'me': me.as_dict()})
    abort(403)


@api.route(base_url + '/me/favorite-items', methods=['GET'])
@auth.login_required
def me_favorite_items():
    """
    @api {get} /v1/members/me/favorite-items My Favorite Items
    @apiVersion 1.0.0
    @apiName MemberMeFavoriteItems
    @apiGroup Members

    @apiSuccess {array} items
    """
    current_member = g.member

    items = []
    for item in current_member.items_favorized:
        if item.is_permitted_for_read(current_member):
            items.append(item.as_dict())
    return json.dumps({'items': items})


@api.route(base_url + '/me/favorite-tests', methods=['GET'])
@auth.login_required
def me_favorite_tests():
    """
    @api {get} /v1/members/me/favorite-tests My Favorite Tests
    @apiVersion 1.0.0
    @apiName MemberMeFavoriteTests
    @apiGroup Members

    @apiSuccess {array} tests
    """
    current_member = g.member

    tests = []
    for test in current_member.tests_favorized:
        if test.is_permitted_for_read(current_member):
            tests.append(test.as_dict())
    return json.dumps({'tests': tests})


@api.route(base_url + '/me/favorite-surveys', methods=['GET'])
@auth.login_required
def me_favorite_surveys():
    """
    @api {get} /v1/members/me/favorite-surveys My Favorite Surveys
    @apiVersion 1.0.0
    @apiName MemberMeFavoriteSurveys
    @apiGroup Members

    @apiSuccess {array} surveys
    """
    current_member = g.member

    surveys = []
    for survey in current_member.surveys_favorized:
        if survey.is_permitted_for_read(current_member):
            surveys.append(survey.as_dict())
    return json.dumps({'surveys': surveys})


@api.route(base_url + '/me/favorites', methods=['GET', 'PATCH'])
@auth.login_required
def me_favorites():
    if request.method == 'GET':
        return get_me_favorites()
    if request.method == 'PATCH':
        return patch_me_favorites()
    abort(400)


def get_me_favorites():
    """
    @api {get} /v1/members/me/favorites My Favorites
    @apiVersion 1.0.0
    @apiName MemberMeFavorites
    @apiGroup Members

    @apiSuccess {array} items_favorized
    @apiSuccess {array} tests_favorized
    @apiSuccess {array} surveys_favorized
    """
    me = g.member
    me_dict = me.as_dict()
    return json.dumps({
        'items_favorized': me_dict['items_favorized'],
        'tests_favorized': me_dict['tests_favorized'],
        'surveys_favorized': me_dict['surveys_favorized']
    })


def patch_me_favorites():
    """
    @api {patch} /v1/members/me/favorites My Favorites
    @apiVersion 1.0.0
    @apiName MemberMeFavorites
    @apiGroup Members

    @apiSuccess {String} success
    """
    me = g.member
    type = request.json.get('type')
    id = request.json.get('id')
    if type == 'items':
        item = Item.query.get(id)
        if item in me.items_favorized:
            me.items_favorized.remove(item)
        else:
            me.items_favorized.append(item)
    elif type == 'tests':
        test = Test.query.get(id)
        if test in me.tests_favorized:
            me.tests_favorized.remove(test)
        else:
            me.tests_favorized.append(test)
    elif type == 'surveys':
        survey = Survey.query.get(id)
        if survey in me.surveys_favorized:
            me.surveys_favorized.remove(survey)
        else:
            me.surveys_favorized.append(survey)
    db.session.commit()
    return json.dumps({'status': 'ok'})


@api.route(base_url + '/login', methods=['GET'])
@auth.login_required
def login():
    """
    @api {get} /v1/members/login Login
    @apiVersion 1.0.0
    @apiName MemberLogin
    @apiGroup Members

    @apiSuccess {Object} auth_token The auth token that is used to identify the current user.
    """
    access_token = g.member.generate_access_token(3600)
    refresh_token = g.member.generate_refresh_token()
    db.session.commit()

    return json.dumps({
        'access_token': access_token.decode('ascii'),
        'refresh_token': refresh_token.decode('ascii'),
        'duration': 3600,
        'me': g.member.as_dict()
    })


@api.route(base_url + '/token', methods=['POST'])
def get_token():
    """
    @api {get} /v1/members/token get a new access token
    @apiVersion 1.0.0
    @apiName MemberToken
    @apiGroup Members
    """

    refresh_token = request.json.get('refresh_token')

    if refresh_token is None:
        abort(403)

    member = Member.query.filter_by(refresh_token=refresh_token).first()

    if not member:
        abort(403)

    access_token = member.generate_access_token(3660)

    return json.dumps({'access_token': access_token.decode('ascii')})


@api.route(base_url + '/logout')
@auth.login_required
def logout():
    """
    @api {get} /v1/members/logout Logout
    @apiVersion 1.0.0
    @apiName MemberLogout
    @apiGroup Members

    @apiSuccess {Object} status Works every time like a charm :)
    """
    g.member.refresh_token = None
    db.session.commit()

    return json.dumps({'status': 'ok'})


@api.route(base_url + '/<member_id>', methods=['GET', 'PUT', 'DELETE'])
@auth.login_required
def member_by_id(member_id):
    if request.method == 'GET':
        return get_member_by_id(member_id)
    elif request.method == 'PUT':
        return put_member_by_id(member_id)
    elif request.method == 'DELETE':
        return delete_member_by_id(member_id)
    abort(400)


def get_member_by_id(member_id):
    """
    @api {get} /v1/members/:id Get member by id.
    @apiVersion 1.0.0
    @apiName GetMemberById
    @apiGroup Members
    @apiParam {Number} id The unique member id.
    @apiDescription Returns information about the user with the given id.
    Admins can see any user, everybody else can only see their own information.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst
    @apiPermission user

    @apiSuccess {Object} member The information about a user as member object.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about another member.
    @apiError 404:NoSuchMemberError The admin tried to access an invalid member id.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if (role == 'admin') or (int(member_id) == current_member.id):
            member = Member.query.filter_by(id=member_id).first()
            if member:
                return json.dumps({'member': member.as_dict()})
            abort(404)
    abort(403)


def put_member_by_id(member_id):
    """
    @api {put} /v1/members/:id Put member by id.
    @apiVersion 1.0.0
    @apiName PutMemberById
    @apiGroup Members
    @apiParam {Number} id The unique member id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst
    @apiPermission user

    @apiSuccess {Object} member The information about a user as member object.
    @apiError 403:PermissionError Someone who is not an admin tried to access information about another member.
    @apiError 404:NoSuchMemberError The admin tried to access an invalid member id.
    """
    current_member = g.member

    member = Member.query.get(member_id)

    if not member:
        abort(404)

    if not member.is_permitted_for_write(current_member):
        abort(403)

    first_name = request.json.get('first_name')
    if first_name:
        member.first_name = first_name
    last_name = request.json.get('last_name')
    if last_name:
        member.last_name = last_name
    role_type = request.json.get('role')
    if role_type and current_member.role.type == 'admin':
        role_admin = Role.query.filter_by(type='admin').first()
        if member.role == role_admin:
            num_admin_members = Member.query.filter_by(role=role_admin).count()
            if num_admin_members < 2:
                abort(403)
        role = Role.query.filter_by(type=role_type).first()
        member.role = role
    password = request.json.get('password')
    if password:
        member.hash_password(password)
    email = request.json.get('email')
    if email:
        member.email = email
    db.session.commit()
    return json.dumps({'member': member.as_dict()})


def delete_member_by_id(member_id):
    current_member = g.member

    member = Member.query.get(member_id)

    role_admin = Role.query.filter_by(type='admin').first()
    num_admin_members = Member.query.filter_by(role=role_admin).count()

    if not member:
        abort(404)

    if member.role == role_admin and num_admin_members < 2:
        abort(403)

    if not current_member.role.type == 'admin':
        abort(403)

    if not (len(member.tests) == 0 and len(member.surveys) == 0
            and len(member.items) == 0):
        abort(403)
    db.session.delete(member)
    db.session.commit()
    return json.dumps({'status': 'ok'})


@api.route(base_url + '/<member_id>/items', methods=['GET'])
@auth.login_required
def items_for_member(member_id):
    """
    @api {get} /v1/members/:id/items Get all items of a member.
    @apiVersion 1.0.0
    @apiName ItemsForMembers
    @apiGroup Members
    @apiParam {Number} id The unique member id.
    @apiParam {String} [format=None] Optional format parameter.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} items The list of items this user has permissions for.
    @apiError 403:PermissionError Someone who is not an admin tried to access the items of another member.
    @apiError 404:NoSuchMemberError The member tried to access an invalid item id.
    """
    current_member = g.member
    if current_member.role:
        # valid member
        role = current_member.role.type
        if role != 'user':
            format = request.args.get('format', None)
            member_id = int(member_id)
            requested_member = Member.query.filter_by(id=member_id).first()
            if requested_member:
                items = []
                if role == 'admin':
                    items = requested_member.items
                elif role == 'author' or role == 'data_analyst':
                    items_permitted = current_member.items_permitted
                    for item in items_permitted:
                        if item.member_id == member_id:
                            items.append(item)
                if format:
                    filtered_items = []
                    for item in items:
                        if item.format.type == format:
                            filtered_items.append(item)
                    items = filtered_items

                return json.dumps({
                    'items': [item.as_dict() for item in items]
                })
            abort(404)
    abort(403)


@api.route(base_url + '/<member_id>/permissions', methods=['GET', 'PATCH'])
@auth.login_required
def member_permissions_by_id(member_id):
    if request.method == 'GET':
        return get_member_permissions_by_id(member_id)
    elif request.method == 'PATCH':
        return patch_member_permissions_by_id(member_id)
    abort(400)


def get_member_permissions_by_id(member_id):
    """
    @api {get} /v1/members/:id/permissions Get permissions of a member.
    @apiVersion 1.0.0
    @apiName PermissionsForMembers
    @apiGroup Members
    @apiParam {Number} id The unique member id.

    @apiUse Authorization
    @apiPermission admin

    @apiSuccess {Object} dict A dictionary containing the permissions for items, tests and surveys of this member.
    @apiError 404:NoSuchMemberError The admin tried to access an invalid item id.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role == 'admin':
            requested_member = Member.query.filter_by(id=member_id).first()
            if requested_member:
                return json.dumps({
                    'items_permitted':
                    [item.id for item in requested_member.items_permitted],
                    'tests_permitted':
                    [test.id for test in requested_member.tests_permitted],
                    'surveys_permitted': [
                        survey.id
                        for survey in requested_member.surveys_permitted
                    ],
                })
            abort(404)
    abort(403)


def patch_member_permissions_by_id(member_id):
    """
    @api {patch} /v1/members/:id/permissions Update member permissions.
    @apiVersion 1.0.0
    @apiName PatchMemberPermissions
    @apiGroup Members
    @apiParam {Number} id The unique member id.
    @apiParam {Number[]} items Permissions as list of item ids.
    @apiParam {Number[]} tests Permissions as list of test ids.
    @apiParam {Number[]} surveys Permissions as list of survey ids.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} items The list of items this user has permissions for.
    @apiError 403:PermissionError Someone who is not an admin tried to access the items of another member.
    @apiError 404:NoSuchMemberError The member tried to access an invalid item id.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role == 'admin':
            requested_member = Member.query.filter_by(id=member_id).first()
            if requested_member:
                items_permitted = request.json.get('items_permitted')
                tests_permitted = request.json.get('tests_permitted')
                surveys_permitted = request.json.get('surveys_permitted')
                requested_member.items_permitted = []
                for item_id in items_permitted:
                    item = Item.query.filter_by(id=int(item_id)).first()
                    if item and item not in requested_member.items_permitted:
                        requested_member.items_permitted.append(item)
                requested_member.tests_permitted = []
                for test_id in tests_permitted:
                    test = Test.query.filter_by(id=int(test_id)).first()
                    if test and test not in requested_member.tests_permitted:
                        requested_member.tests_permitted.append(test)
                requested_member.surveys_permitted = []
                for survey_id in surveys_permitted:
                    survey = Survey.query.filter_by(id=int(survey_id)).first()
                    if survey and survey not in requested_member.surveys_permitted:
                        requested_member.surveys_permitted.append(survey)
                return json.dumps({
                    'status':
                    'updated',
                    'member_id':
                    requested_member.id,
                    'items_permitted': [
                        item.as_dict()
                        for item in requested_member.items_permitted
                    ],
                    'tests_permitted': [
                        test.as_dict()
                        for test in requested_member.tests_permitted
                    ],
                    'surveys_permitted': [
                        survey.as_dict()
                        for survey in requested_member.surveys_permitted
                    ],
                })
            abort(404)
    abort(403)


@api.route(base_url + '/<member_id>/tests', methods=['GET'])
@auth.login_required
def tests_for_member(member_id):
    """
    @api {get} /v1/members/:id/tests Get tests for a member.
    @apiVersion 1.0.0
    @apiName GetTestsForMember
    @apiGroup Members
    @apiParam {Number} id The unique member id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} tests The list of tests this user has permissions for.
    @apiError 403:PermissionError Someone who is not an admin tried to access the tests of another member.
    @apiError 404:NoSuchMemberError The member tried to access an invalid item id.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role != 'user':
            include = request.args.get('include', False)
            member_id = int(member_id)
            if role:
                requested_member = Member.query.filter_by(id=member_id).first()
                if requested_member:
                    tests = []
                    if role == 'admin':
                        tests = requested_member.tests
                    elif role == 'author' or role == 'data_analyst':
                        tests_permitted = current_member.tests_permitted
                        for test in tests_permitted:
                            if test.member_id == member_id:
                                tests.append(test)
                    return json.dumps({
                        'tests': [test.as_dict() for test in tests]
                    })
                abort(404)
    abort(403)
