from flask import abort, g, json

from commoop.auth import auth
from commoop.api.v1 import api
from commoop.models.format import Format


@api.route('/formats', methods=['GET'])
@auth.login_required
def get_formats():
    """
    @api {get} /v1/formats Request information about all formats
    @apiVersion 1.0.0
    @apiName GetFormats
    @apiGroup Formats

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission data_analyst

    @apiSuccess {Object[]} formats Array of all formats.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role == 'admin'or role == 'author' or role == 'data_analyst':
            formats = Format.query.all()
            return json.dumps(
                {
                    'formats': [format.as_dict() for format in formats]
                }
            )
    abort(403)


@api.route('/formats/<type_or_code>', methods=['GET'])
@auth.login_required
def get_format(type_or_code):
    """
    @api {get} /v1/formats/:type_or_code Request specific format information
    @apiParam {String} type_or_code The specific type or code of a format.
    @apiVersion 1.0.0
    @apiName GetFormat
    @apiGroup Formats
    @apiParam {String} type_or_code The specific type or code of a format.
    @apiParamExample {json} Request-Example:
    {
        "type_or_code": "trc"
    }

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object} format The specific format.
    @apiError 404:FormatNotFound The requested format was invalid.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role == 'admin' or role == 'author' or role == 'data_analyst':
            format = Format.query.filter_by(type=type_or_code).first()
            if not format:
                format = Format.query.filter_by(code=type_or_code).first()

            if not format:
                return abort(404)
            return json.dumps(
                {
                    'format': format.as_dict()
                }
            )
    abort(403)
