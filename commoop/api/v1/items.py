from commoop.api.v1 import api
from commoop.auth import auth
from commoop.database import db
from commoop.models.format import Format
from commoop.models.item import Item
from commoop.models.item_result import ItemResult
from commoop.models.item_result_comment import ItemResultComment
from commoop.models.member import Member
from commoop.models.survey import Survey
from commoop.models.test import Test
from commoop.utils.hash import make_hash
from flask import abort, g, json, jsonify, request

base_url = '/items'


@api.route(base_url, methods=['POST', 'GET'])
@auth.login_required
def items():
    if request.method == 'POST':
        return created_item_object()
    elif request.method == 'GET':
        return get_all_items()
    abort(400)


def created_item_object():
    """
    @api {post} /v1/items Create a new item.
    @apiVersion 1.0.0
    @apiName CreateNewItem
    @apiGroup Items
    @apiParam {String} code The specific code of a format for the item.
    @apiParam {Object} content The content of the item.
    @apiParam {Object} solution The specific solution of an item.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author

    @apiSuccess {Object} status_and_item_id The status of and either a new item id or an existing one.
    @apiError 404:FormatNotFound The requested format was invalid.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role == 'admin' or role == 'author':
            type_or_code = request.json.get('format')
            if type_or_code:
                format = Format.query.filter_by(type=type_or_code).first()
                if not format:
                    format = Format.query.filter_by(code=type_or_code).first()
                if not format:
                    abort(404)
            else:
                abort(404)
            # categories = request.json.get('categories')
            content = request.json.get('content', {
                'private': {},
                'public': {}
            })
            solution = request.json.get('solution', {})
            name = request.json.get('name')
            assignment = request.json.get('assignment')
            allow_note = request.json.get('allow_note')
            responses = request.json.get('responses', {})

            to_hash = {'content': content, 'solution': solution}

            if not isinstance(responses, dict):
                abort(422)

            hash = make_hash(to_hash)

            try:
                item = Item(
                    name=name,
                    format=format,
                    allow_note=allow_note,
                    # categories = categories,
                    content=content,
                    responses=responses,
                    solution=solution,
                    assignment=assignment,
                    hash=hash,
                    member=current_member,
                    members_permitted=[current_member])
                db.session.add(item)
                db.session.commit()
                if not name:
                    item.name = current_member.username + '_' + \
                        format.code + '_' + str(item.id)
                db.session.commit()
                return json.dumps({'status': 'created', 'item_id': item.id})
            except:
                abort(406)
    abort(403)


def get_all_items():
    """
    @api {get} /v1/items Get all items.
    @apiVersion 1.0.0
    @apiName GetAllItems
    @apiGroup Items
    @apiParam {String} [format=None] Optional format paramter.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} items All items that the member is permitted to see.
    @apiError 404:FormatNotFound The requested format was invalid.
    """
    current_member = g.member

    type_or_code = request.args.get('format', None)

    items = []
    if type_or_code:
        format = Format.query.filter_by(type=type_or_code).first()
        if not format:
            format = Format.query.filter_by(code=type_or_code).first()
        if not format:
            abort(404)
        items = Item.query.filter_by(format=format)
    else:
        items = Item.query.all()

    permitted_items = []
    for item in items:
        if item.is_permitted_for_read(current_member):
            permitted_items.append(item)

    return json.dumps({'items': [item.as_dict() for item in permitted_items]})


@api.route(base_url + '/results', methods=['GET'])
@auth.login_required
def get_all_item_results():
    """
    @api {get} /v1/items/results All results of selected items.
    @apiVersion 1.0.0
    @apiName GetItemResults
    @apiGroup Items
    @apiParam {String} [format=None] Optional format paramter.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} item_results A list of all item results, depending on the selected format and permitted items.
    @apiError 404:FormatNotFound The requested format was invalid.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role != 'user':
            type_or_code = request.args.get('format', None)
            format = None
            if type_or_code:
                format = Format.query.filter_by(type=type_or_code).first()
                if not format:
                    format = Format.query.filter_by(code=type_or_code).first()
                if not format:
                    return abort(404)
            item_results = []
            if role == 'admin':
                item_results = ItemResult.query.all()
            elif role == 'author' or role == 'data_analyst':
                for item in current_member.items_permitted:
                    item_results.extend(item.results)
            if format:
                filtered_item_results = []
                for item_result in item_results:
                    if item_result.item.format.type == format:
                        filtered_item_results.append(item_result)
                item_results = filtered_item_results

            return json.dumps({
                'item_results':
                [item_result.as_dict() for item_result in item_results]
            })
    abort(403)


@api.route(base_url + '/<item_id>', methods=['GET', 'PUT', 'DELETE'])
@auth.login_required
def item_by_id(item_id):
    if request.method == 'GET':
        return get_item_by_id(item_id)
    elif request.method == 'PUT':
        return put_item_by_id(item_id)
    elif request.method == 'DELETE':
        return delete_item_by_id(item_id)
    abort(400)


def get_item_by_id(item_id):
    """
    @api {get} /v1/items/:id Detailed information about a specific item.
    @apiVersion 1.0.0
    @apiName GetItemById
    @apiGroup Items
    @apiParam {Number} id The unique item id.
    @apiParam {Number} [survey_ids[]] survey id used for filtering
    @apiParam {Number} [result_ids[]] result id used for filtering
    @apiParam {Number} [test_ids[]] test id used for filtering

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object} item The information about an item.
    @apiError 403:PermissionError The member tried to access an item without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item id.
    """
    current_member = g.member

    item = Item.query.filter_by(id=item_id).first()

    if not item:
        abort(404)

    if not item.is_permitted_for_read(current_member):
        abort(403)

    filters = {
        'survey_ids': [int(s) for s in request.args.getlist('survey_ids[]')],
        'test_ids': [int(t) for t in request.args.getlist('test_ids[]')],
        'result_ids': [int(r) for r in request.args.getlist('result_ids[]')],
    }

    return json.dumps({'item': item.as_dict(filters=filters)})


def put_item_by_id(item_id):
    """
    @api {put} /v1/items/:id Overwrite a specific item.
    @apiVersion 1.0.0
    @apiName PutItemById
    @apiGroup Items
    @apiParam {Number} id The unique item id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiError 403:PermissionError The member tried to overwrite an item without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to overwrite an invalid item id.
    @apiError 405:OperationNotAllowedError The member tried to overwrite an item which is currently used.
    """
    item = Item.query.get(item_id)
    if not item:
        abort(404)

    current_member = g.member
    if current_member == item.member or current_member.role.type == 'admin':
        if item.results:
            abort(405)
        item.content = request.json.get('content', {
            'private': {},
            'public': {}
        })
        item.solution = request.json.get('solution', {})
        item.name = request.json.get('name')
        item.assignment = request.json.get('assignment')
        item.allow_note = request.json.get('allow_note')
        item.responses = request.json.get('responses', {})
        try:
            db.session.commit()
        except:
            abort(406)
        return json.dumps({'status': 'ok'})
    abort(403)


def delete_item_by_id(item_id):
    """
    @api {delete} /v1/items/:id Delete a specific item.
    @apiVersion 1.0.0
    @apiName DeleteItemById
    @apiGroup Items
    @apiParam {Number} id The unique item id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiError 403:PermissionError The member tried to delete an item without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to delete an invalid item id.
    @apiError 405:OperationNotAllowedError The member tried to delete an item which is currently used.
    """
    item = Item.query.get(item_id)
    if not item:
        abort(404)

    current_member = g.member
    if current_member == item.member or current_member.role.type == 'admin':
        if item.item_groups or item.results:
            abort(405)
        db.session.delete(item)
        db.session.commit()
        return json.dumps({'status': 'ok'})
    abort(403)


@api.route(base_url + '/<item_id>/statistic', methods=['GET'])
@auth.login_required
def get_item_stats_by_id(item_id):
    """
    @api {get} /v1/items/:id/statistic Statistic information about a specific item.
    @apiVersion 1.0.0
    @apiName GetItemStatsById
    @apiGroup Items
    @apiParam {Number} id The unique item id.
    @apiParam {Number} [survey_ids[]] survey id used for filtering
    @apiParam {Number} [result_ids[]] result id used for filtering
    @apiParam {Number} [test_ids[]] test id used for filtering

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object} item_stats The statistics about an item.
    @apiError 403:PermissionError The member tried to access an item without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item id.
    """
    current_member = g.member

    filters = {
        'survey_ids': [int(s) for s in request.args.getlist('survey_ids[]')],
        'test_ids': [int(t) for t in request.args.getlist('test_ids[]')],
        'result_ids': [int(r) for r in request.args.getlist('result_ids[]')],
    }

    item = Item.query.filter_by(id=item_id).first()

    if not item:
        abort(404)

    # TODO restrict access

    return json.dumps({
        'item_statistic':
        item.get_detailed_statistic(filters=filters)
    })


@api.route(base_url + '/<item_id>/permissions', methods=['GET'])
@auth.login_required
def get_members_permitted_for_item(item_id):
    """
    @api {get} /v1/items/:id/permissions Get members permitted for a specific item.
    @apiVersion 1.0.0
    @apiName GetMembersPermittedForItem
    @apiGroup Items
    @apiParam {Number} id The unique item id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object} members The members permitted for the item.
    @apiError 403:PermissionError The member tried to access an item without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item id.
    """
    current_member = g.member

    item = Item.query.get(item_id)
    if not item:
        abort(404)

    if not item.is_permitted_for_read(current_member):
        abort(403)

    return json.dumps({
        'members': [member.as_dict() for member in item.members_permitted]
    })


@api.route(
    base_url + '/<item_id>/permissions/<member>', methods=['PUT', 'DELETE'])
@auth.login_required
def member_permission_for_item(item_id, member):
    if request.method == 'PUT':
        return put_member_permission_for_item(item_id, member)
    elif request.method == 'DELETE':
        return delete_member_permission_for_item(item_id, member)
    abort(400)


def put_member_permission_for_item(item_id, member_identifier):
    """
    @api {put} /v1/items/:id/permissions/:member Add a member to the permitted ones for the item
    @apiVersion 1.0.0
    @apiName PutMemberPermissionForItem
    @apiGroup Items
    @apiParam {Number} id The unique item id.
    @apiParam {Number} member_identifier The unique member id or the unique username.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} status OK
    @apiError 403:PermissionError The member tried to access an item without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item id.
    """
    current_member = g.member
    item = Item.query.get(item_id)

    if current_member.role.type != 'admin' and current_member != item.member:
        abort(403)

    member = Member.query.get(member_identifier)

    if not member:
        member = Member.query.filter_by(username=member_identifier).first()

    if not item or not member:
        abort(404)

    if member not in item.members_permitted:
        item.members_permitted.append(member)
        db.session.commit()

    return json.dumps({'member_id': member.id})


def delete_member_permission_for_item(item_id, member_identifier):
    """
    @api {delete} /v1/items/:id/permissions/:member Remove a member from the permitted ones for the item
    @apiVersion 1.0.0
    @apiName DeleteMemberPermissionForItem
    @apiGroup Items
    @apiParam {Number} id The unique item id.
    @apiParam {Number} member_identifier The unique member id or the unique username.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} status OK
    @apiError 403:PermissionError The member tried to access an item without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item id.
    """
    current_member = g.member
    item = Item.query.get(item_id)

    if current_member.role.type != 'admin' and current_member != item.member:
        abort(403)

    member = Member.query.get(member_identifier)

    if not member:
        member = Member.query.filter_by(username=member_identifier).first()

    if not item or not member:
        abort(404)

    if member in item.members_permitted:
        item.members_permitted.remove(member)
        db.session.commit()

    return json.dumps({'member_id': member.id})


@api.route(base_url + '/<item_id>/tests', methods=['GET'])
@auth.login_required
def tests_for_item(item_id):
    """
    @api {get} /v1/items/:id/tests Information about the tests that contain the specific item.
    @apiVersion 1.0.0
    @apiName GetTestsForItemId
    @apiGroup Items
    @apiParam {Number} id The unique item id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} item The list of tests that contain this item.
    @apiError 403:PermissionError The member tried to access an item without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item id.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role != 'user':
            tests = []
            item = Item.query.filter_by(id=item_id).first()
            if item:
                if role == 'admin':
                    tests = item.get_tests()
                elif role == 'author' or role == 'data_analyst':
                    if item in current_member.items_permitted:
                        for test in current_member.tests_permitted:
                            if item in test.get_items():
                                tests.append(test)
                    else:
                        abort(403)

                return json.dumps({
                    'tests': [test.as_dict() for test in tests]
                })
            abort(404)
    abort(403)


@api.route(base_url + '/<item_id>/results', methods=['GET'])
@auth.login_required
def results_for_item(item_id):
    """
    @api {get} /v1/items/:id/results Information about all the results that were produced for that specific item.
    @apiVersion 1.0.0
    @apiName GetResultsForItemId
    @apiGroup Items
    @apiParam {Number} id The unique item id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} item The list of results that were produced for this item.
    @apiError 403:PermissionError The member tried to access an item without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item id.
    """
    current_member = g.member
    if current_member.role:
        role = current_member.role.type
        if role != 'user':
            item = Item.query.filter_by(id=item_id).first()
            if item:
                results = []
                if role == 'admin':
                    results = item.results
                elif role == 'author' or role == 'data_analyst':
                    if item in current_member.items_permitted:
                        results = item.results
                    else:
                        abort(403)

                return json.dumps({
                    'results': [result.as_dict() for result in results]
                })
            abort(404)
    abort(403)


@api.route(base_url + '/results/<result_id>', methods=['GET'])
@auth.login_required
def result_for_item(result_id):
    """
    @api {get} /v1/items/results/:id Information about one specific result that was produced for that specific item.
    @apiVersion 1.0.0
    @apiName GetResultForItem
    @apiGroup Items
    @apiParam {Number} id The unique result id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} item The list of results that were produced for this item.
    @apiError 403:PermissionError The member tried to access an item without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item id.
    """
    current_member = g.member

    item_result = ItemResult.query.get(result_id)

    if not item_result:
        abort(404)

    if not item_result.is_permitted_for_read(current_member):
        abort(403)

    return json.dumps({'result': item_result.as_dict()})


@api.route(base_url + '/results/<result_id>/comments', methods=['GET', 'PUT'])
@auth.login_required
def comments_for_result_id(result_id):
    if request.method == 'PUT':
        return put_comment_for_result_id(result_id)
    elif request.method == 'GET':
        return get_comments_for_result_id(result_id)


def put_comment_for_result_id(result_id):
    """
    @api {put} /v1/items/results/:id/comments Add a comment ot the result of an item.
    @apiVersion 1.0.0
    @apiName PutCommentForResultId
    @apiGroup Items
    @apiParam {Number} id The unique result id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} status ok when everything was good
    @apiError 403:PermissionError The member tried to access an item result without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item result id.
    """
    current_member = g.member

    item_result = ItemResult.query.get(result_id)

    if not item_result:
        abort(404)

    if not item_result.is_permitted_for_read(current_member):
        abort(403)

    text = request.json.get('text')
    category = request.json.get('category')
    start_time = request.json.get('start_time')
    end_time = request.json.get('end_time')
    parent_id = request.json.get('parent_id')

    if parent_id and (category or start_time or end_time):
        abort(422)
    elif not parent_id and not (isinstance(category, str) and start_time != ""
                                and end_time != ""):
        abort(422)

    if start_time and end_time and int(end_time) < int(start_time):
        abort(422)

    item_result_comment = ItemResultComment(
        member=current_member,
        text=text,
        category=category,
        start_time=start_time,
        end_time=end_time,
        parent_id=parent_id,
        item_result=item_result)

    db.session.add(item_result_comment)
    db.session.commit()

    return json.dumps({'item_result_comment': item_result_comment.as_dict()})


def get_comments_for_result_id(result_id):
    """
    @api {get} /v1/items/results/:id/comments Comments for one specific item result.
    @apiVersion 1.0.0
    @apiName GetCommentsForResultId
    @apiGroup Items
    @apiParam {Number} id The unique result id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} item The list of comments that were made for this item result.
    @apiError 403:PermissionError The member tried to access an item result without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item result id.
    """
    current_member = g.member

    item_result = ItemResult.query.get(result_id)

    if not item_result:
        abort(404)

    if not item_result.is_permitted_for_read(current_member):
        abort(403)

    item_result_comments = []

    for item_result_comment in item_result.item_result_comments:
        if not item_result_comment.end_time:
            continue
        item_result_comments.append(item_result_comment.as_dict())

    return json.dumps({'item_result_comments': item_result_comments})


@api.route(
    base_url + '/results/<result_id>/comments/<comment_id>',
    methods=['POST', 'DELETE'])
@auth.login_required
def comment_for_result_id(result_id, comment_id):
    if request.method == 'POST':
        return post_comment_for_result_id(result_id, comment_id)
    elif request.method == 'DELETE':
        return delete_comment_for_result_id(result_id, comment_id)


def post_comment_for_result_id(result_id, comment_id):
    """
    @api {post} /v1/items/results/:id/comments/:comment_id Overwrite a comment for the result of an item.
    @apiVersion 1.0.0
    @apiName PostCommentForResultId
    @apiGroup Items
    @apiParam {Number} id The unique result id.
    @apiParam {Number} comment_id The unique comment id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} status ok when everything was good
    @apiError 403:PermissionError The member tried to access an item result or comment without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item result id or comment id.
    """
    current_member = g.member

    item_result = ItemResult.query.get(result_id)

    if not item_result:
        abort(404)

    if not item_result.is_permitted_for_write(current_member):
        abort(403)

    item_result_comment = ItemResultComment.query.get(comment_id)

    if not item_result_comment:
        abort(404)

    if not item_result_comment.is_permitted_for_write(current_member):
        abort(403)

    text = request.json.get('text')
    category = request.json.get('category')
    start_time = request.json.get('start_time')
    end_time = request.json.get('end_time')

    item_result_comment.text = text
    item_result_comment.category = category
    item_result_comment.start_time = start_time
    item_result_comment.end_time = end_time

    db.session.commit()

    return json.dumps({'status': 'ok'})


def delete_comment_for_result_id(result_id, comment_id):
    """
    @api {delete} /v1/items/results/:id/comments/:comment_id Delete a comment for the result of an item.
    @apiVersion 1.0.0
    @apiName DeleteCommentForResultId
    @apiGroup Items
    @apiParam {Number} id The unique result id.
    @apiParam {Number} comment_id The unique comment id.

    @apiUse Authorization
    @apiPermission admin
    @apiPermission author
    @apiPermission dataAnalyst

    @apiSuccess {Object[]} status ok when everything was good
    @apiError 403:PermissionError The member tried to access an item result or comment without sufficient permissions.
    @apiError 404:NoSuchItemError The member tried to access an invalid item result id or comment id.
    """
    current_member = g.member

    item_result = ItemResult.query.get(result_id)

    if not item_result:
        abort(404)

    item_result_comment = ItemResultComment.query.get(comment_id)
    if not item_result_comment:
        abort(404)

    if not item_result_comment.is_permitted_for_write(current_member):
        abort(403)

    for reply in item_result_comment.replies:
        db.session.delete(reply)

    db.session.delete(item_result_comment)
    db.session.commit()

    return json.dumps({'status': 'ok'})


@api.route(base_url + '/evaluate', methods=['POST'])
@auth.login_required
def evaluate_item():
    """
    @api {post} /v1/items/evaluate Evaluate a received item data.
    @apiVersion 1.0.0
    @apiName GetItemStatsById
    @apiGroup Items
    @apiParam {Number} id The unique item id.

    @apiSuccess {Object} evaluation The result of the evaluation.
    @apiError 422:NotEnoughInformation Not enough information was received to evalute.
    """

    solution = request.json.get('solution')
    content = request.json.get('content')
    responses = request.json.get('responses')
    format_type = request.json.get('format')
    submitted_solution = request.json.get('submittedSolution', {})

    evaluated_item = Item._evaluate(submitted_solution, solution, responses,
                                    content, format_type)

    return json.dumps({'evaluation': evaluated_item})
