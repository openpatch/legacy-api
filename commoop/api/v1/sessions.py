import random
from datetime import datetime

from commoop.api.v1 import api
from commoop.database import db
from commoop.models.item_group_result import ItemGroupResult
from commoop.models.item_result import ItemResult
from commoop.models.survey import Survey
from commoop.models.test_result import TestResult
from flask import abort, json, request

base_url = '/sessions'


@api.route(base_url + '/prepare', methods=['POST'])
def prepare_session():
    survey_identifier = request.json.get('survey_identifier')
    password = request.json.get('password')
    if not survey_identifier:
        abort(422)

    survey = Survey.query.filter_by(name=survey_identifier).first()
    if not survey:
        survey = Survey.query.filter_by(url_suffix=survey_identifier).first()
        if not survey:
            abort(404)

    if survey.starts_on and survey.starts_on > datetime.now().date():
        return json.dumps({'status': 'inactive'})

    if survey.ends_on and survey.ends_on < datetime.now().date():
        return json.dumps({'status': 'expired'})

    if (survey.password_hash
            and not password) or (survey.password_hash
                                  and not survey.verify_password(password)):
        return json.dumps({'status': 'forbidden'})

    test_result = TestResult(
        survey=survey,
        test=survey.test,
        abort=False,
    )

    db.session.add(test_result)
    db.session.commit()
    session_hash = test_result.session_hash

    return json.dumps({
        'session_hash': session_hash,
        'survey': {
            'greeting': survey.greeting,
            'farewell': survey.farewell,
            'show_session_id': survey.show_session_id,
            'show_progress': survey.show_progress
        }
    })


@api.route(base_url + '/start', methods=['GET'])
def start_session():
    session_hash = request.args.get('session_hash')

    if not session_hash:
        abort(404)

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()

    if not test_result:
        abort(404)

    test_result.start = db.func.now()
    db.session.commit()
    session_hash = test_result.session_hash

    return json.dumps({
        'session_hash': session_hash,
    })


@api.route(base_url + '/abort', methods=['POST'])
def end_session():
    session_hash = request.args.get('session_hash')
    reason = request.json.get('reason')

    if not session_hash:
        abort(404)

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()
    if test_result is None:
        abort(404)

    test_result.end = db.func.now()
    test_result.abort = True
    test_result.abort_reason = reason
    db.session.commit()

    return json.dumps({'status': 'ok'})


@api.route(base_url, methods=['POST', 'GET'])
def sessions():
    if request.method == 'POST':
        return submit_solutions()
    elif request.method == 'GET':
        return get_items()
    abort(400)


def submit_solutions():
    """
    @api {post} /v1/sessions Submitting solutions for current items.
    @apiVersion 1.0.0
    @apiName SubmitSolutions
    @apiGroup Sessions

    @apiPermission admin
    @apiPermission author
    @apiPermission data_analyst
    @apiPermission user
    @apiParam {String} session_hash The current hash of the session.
    @apiParam {Array} item_solutions The proposed solutions to the items in form of { id, solution }.

    @apiSuccess {String} session_hash the one that was provided.
    @apiSuccess {Array} checks an array of objects containing information about success and errors.
    @apiSuccess {Boolean} overall true, when all solutions are correct or pass
    @apiError 422:InsufficientDetails Someone tried to create a session but didn't provide any session hash.
    @apiError 404:NoSuchPage There is no session with the provided session hash.
    """
    session_hash = request.json.get('session_hash')
    user_item_solutions = request.json.get('item_solutions', [])
    user_item_notes = request.json.get('item_notes', [])
    user_item_actions = request.json.get('item_actions', [])
    skip = request.json.get('skip', False)

    if session_hash is None:
        abort(422)

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()
    if test_result is None:
        abort(404)

    last_item_group_result = test_result.item_group_results[-1]

    if last_item_group_result is None:
        abort(404)

    checks = []
    passed = True

    # only two possibilites. Either only one solutions is submitted or all
    # solutions are submitted, because the item group is jumpable.
    if len(user_item_solutions) == 1:
        # only items which were send to the user have an item result, therefore
        # take the last one.
        try:
            user_item_solution = user_item_solutions[0]
        except IndexError:
            user_item_solution = {}
        try:
            user_item_action = user_item_actions[0]
        except IndexError:
            user_item_action = {}
        try:
            user_item_note = user_item_notes[0]
        except IndexError:
            user_item_note = ''
        item_result = last_item_group_result.item_results[-1]
        item = item_result.item

        evaluation = item.evaluate(user_item_solution)
        check = evaluation['correct']

        item_result.solution = user_item_solution
        item_result.correct = check
        item_result.actions = user_item_action
        item_result.note = user_item_note
        item_result.end_time = datetime.now()

        checks.append(check)
        passed = passed and check
    elif len(user_item_solutions) == len(
            last_item_group_result.item_group.items):
        for i in range(len(user_item_solutions)):
            try:
                user_item_solution = user_item_solutions[i]
            except IndexError:
                user_item_solution = {}
            try:
                user_item_action = user_item_actions[i]
            except IndexError:
                user_item_action = {}
            try:
                user_item_note = user_item_notes[i]
            except IndexError:
                user_item_note = ''
            item_result = last_item_group_result.item_results[i]
            item_solution = item_result.item.solution
            item = item_result.item

            evaluation = item.evaluate(user_item_solution)
            check = evaluation['correct']

            item_result.solution = user_item_solution
            item_result.correct = check
            item_result.actions = user_item_action
            item_result.note = user_item_note
            item_result.end_time = datetime.now()

            checks.append(check)
            passed = passed and check

    if not last_item_group_result.item_group.needs_to_be_correct and not skip:
        passed = True
    if passed:
        db.session.commit()
    else:
        db.session.rollback()

    return json.dumps({
        'passed': passed,
        'checks': checks,
        'session_hash': session_hash,
    })


def get_items():
    """
    @api {get} /v1/sessions Get items for a session.
    @apiVersion 1.0.0
    @apiName GetItemsSession
    @apiGroup Sessions

    @apiPermission admin
    @apiPermission author
    @apiPermission data_analyst
    @apiPermission user
    @apiParam {String} session_hash The current hash of the session.

    @apiSuccess {String} session_hash the one that was provided.
    @apiSuccess {Array} items The next items in the queue.
    @apiError 422:InsufficientDetails Someone tried to create a session but didn't provide any session hash.
    @apiError 404:NoSuchPage There is no session with the provided session hash.
    """
    session_hash = request.args.get('session_hash')

    test_result = TestResult.query.filter_by(session_hash=session_hash).first()
    if test_result is None:
        abort(404)

    items = next_items(test_result)

    if len(items) == 0:
        test_result.end = db.func.now()

    current_progress = len(test_result.get_item_results()) - len(items)
    max_progress = len(test_result.survey.test.get_items())
    db.session.commit()

    return json.dumps({
        'session_hash':
        session_hash,
        'current_progress':
        current_progress,
        'max_progress':
        max_progress,
        'items': [{
            'assignment': item.assignment,
            'allow_note': item.allow_note,
            'responses': item.responses,
            'content': {
                'public': item.content.get('public', {})
            },
            'id': item.id,
            'format': {
                'type': item.format.type
            }
        } for item in items]
    })


def next_items(test_result):
    item_group_results = test_result.item_group_results
    items = []
    if not item_group_results:
        first_item_group = test_result.test.item_groups[0]
        items = new_item_group_result(first_item_group, test_result)
    else:
        last_item_group_result = item_group_results[-1]
        if is_item_group_result_complete(last_item_group_result):
            next_index = len(test_result.item_group_results)
            if next_index >= len(test_result.test.item_groups):
                return []
            next_item_group = test_result.test.item_groups[next_index]
            items = new_item_group_result(next_item_group, test_result)
        else:
            items = next_item_group_items(last_item_group_result)

    return items


def is_item_group_result_complete(item_group_result):
    items = item_group_result.item_group.items
    item_results = item_group_result.item_results
    return len(items) == len(item_results)


def next_item_group_items(item_group_result):
    items = []
    item_group = item_group_result.item_group
    if item_group.is_jumpable and item_group.is_randomized:
        items = random.shuffle(item_group.items)
    elif item_group.is_jumpable:
        items = item_group.items
    else:
        completed_items = []
        for item_result in item_group_result.item_results:
            completed_items.append(item_result.item)
        remaining_items = [
            item for item in item_group.items if item not in completed_items
        ]
        if item_group.is_randomized:
            items = random.sample(remaining_items, 1)
        else:
            items = [remaining_items[0]]
    for item in items:
        item_result = ItemResult(
            item=item,
            correct=False,
            survey_id=item_group_result.survey_id,
            start_time=datetime.now,
            test_id=item_group_result.test_id)
        item_group_result.item_results.append(item_result)
    return items


def new_item_group_result(item_group, test_result):
    item_group_result = ItemGroupResult(
        item_group=item_group, test_result=test_result)
    return next_item_group_items(item_group_result)
