from flask import g
from flask_httpauth import HTTPBasicAuth

from commoop.models.member import Member

auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(username_or_token, password):
    """
    @apiDefine Authorization
    @apiHeader {String} Authorization Members auth token or username and password
    @apiHeaderExample {json} Admin-Example:
        {
            "Authorization": "Basic YWRtaW46YWRtaW4="
        }
    @apiHeaderExample {json} Author-Example:
        {
            "Authorization": "Basic Ym9iOmI="
        }
    @apiHeaderExample {json} Data Analyst-Example:
        {
            "Authorization": "Basic ZGF2aWQ6dG9iaW5za2k="
        }
    @apiError 403:InvalidUser A user tried to access a resource.
    """
    member = Member.verify_access_token(username_or_token)
    if not member:
        member = Member.query.filter_by(username=username_or_token).first()
        if not member or not member.verify_password(password):
            return False
    g.member = member
    return True
