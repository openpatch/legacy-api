from commoop.statistics import descriptive, interrater

from . import Format


class Highlight(Format):
    @classmethod
    def init_frequency_matrix(cls, text):
        lines = text.splitlines(True)
        matrix = []
        for line in lines:
            matrix_row = []
            for char in line:
                matrix_row.append({'char': char, 'value': 0})
            matrix.append(matrix_row)
        return matrix

    @classmethod
    def fill_frequency_matrix(cls, coder_data, matrix):
        char_index = 0
        for line in matrix:
            for char in line:
                if coder_data[char_index]:
                    char['value'] += 1
                char_index += 1
        return matrix

    @classmethod
    def get_char_highlights(cls, highlights):
        if not highlights:
            return []
        raw_highlights = highlights.split('|')
        del raw_highlights[0]
        char_highlights = []
        for raw_highlight in raw_highlights:
            raw_highlight = raw_highlight.split('$')
            char_highlights += range(
                int(raw_highlight[0]), int(raw_highlight[1]))
        return char_highlights

    @classmethod
    def make_coder_data_task_from_chars(cls, chars, last_char):
        data = []
        for char in range(last_char):
            data.append(char in chars)
        return data

    @classmethod
    def get_statistic(cls, results, content, solution):
        statistic = {}
        text = content.get('public', {}).get('text',
                                             '').replace('```\n', '').replace(
                                                 '\n```', '')
        num_chars = len(text)
        char_highlights_solution = cls.get_char_highlights(
            solution.get('highlights'))
        solution_coder_data = cls.make_coder_data_task_from_chars(
            char_highlights_solution, num_chars)
        alphas = []
        rhos = []
        kappas = []
        matrix = cls.init_frequency_matrix(text)
        for result in results:
            highlights = ''
            if result.solution:
                highlights = result.solution.get('highlights', '')
            char_highlights_submitted_solution = cls.get_char_highlights(
                highlights)
            submitted_coder_data = cls.make_coder_data_task_from_chars(
                char_highlights_submitted_solution, num_chars)
            contingency_table = interrater.get_contingency_table(
                solution_coder_data, submitted_coder_data)
            kappa = interrater.get_kappa(contingency_table)
            rho = interrater.get_rho(contingency_table)
            alpha = interrater.get_alpha(contingency_table, num_chars)
            if not alpha == 'NA':
                alphas.append(alpha)
            if not kappa == 'NA':
                kappas.append(kappa)
            if not rho == 'NA':
                rhos.append(rho)
            matrix = cls.fill_frequency_matrix(submitted_coder_data, matrix)

        statistic['frequency_matrix'] = matrix

        statistic['alpha'] = descriptive.summary(alphas)
        statistic['rho'] = descriptive.summary(rhos)
        statistic['kappa'] = descriptive.summary(kappas)

        return statistic
