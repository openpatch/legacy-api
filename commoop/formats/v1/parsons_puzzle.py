import copy
import json

from markdown import markdown

from . import Format


class ParsonsPuzzle(Format):
    @classmethod
    def get_html_from_fragments(cls, fragments):
        text = ''
        for fragment in fragments:
            fragment = json.loads(fragment)
            text += fragment['source']
            text += '\n'
        html = markdown(text, extensions=['markdown.extensions.fenced_code'])
        return html

    @classmethod
    def build_graph(cls, results, org_user_fragments, org_source_fragments):
        nodes = {}
        edges = []

        for result in results:
            actions = result.actions
            correct = result.correct

            if not actions:
                continue

            user_fragments = copy.deepcopy(org_user_fragments)
            source_fragments = copy.deepcopy(org_source_fragments)

            user_fragments = [json.dumps(uf) for uf in user_fragments]
            source_fragments = [json.dumps(sf) for sf in source_fragments]

            start_state = user_fragments
            start_state_html = cls.get_html_from_fragments(start_state)
            hashed_start_state = str(hash(start_state_html))

            if hashed_start_state not in nodes:
                nodes[hashed_start_state] = {
                    'html': start_state_html,
                    'type': 'start',
                    'payload': 0
                }
            nodes[hashed_start_state]['payload'] += 1

            current_state = start_state
            hashed_previous_state = hashed_start_state

            for action in actions:
                action = action.get('action', {})
                action_type = action.get('type')
                if not action_type:
                    continue
                source_id = action.get('sourceId', -1)
                target_id = action.get('targetId', -1)

                if not source_id > -1 or not target_id > -1:
                    continue
                try:
                    if 'MOVE_FROM_SOURCE_TO_USER' in action_type:
                        source_fragment = source_fragments[source_id]
                        user_fragments.insert(target_id, source_fragment)
                        source_fragments.remove(source_fragment)
                    elif 'MOVE_FROM_USER_TO_SOURCE' in action_type:
                        source_fragment = source_fragments[target_id]
                        user_fragment = user_fragments[source_id]
                        source_fragments.insert(target_id, user_fragment)
                        user_fragments.remove(user_fragment)
                    elif 'MOVE_WITHIN_USER' in action_type:
                        user_fragment = user_fragments[source_id]
                        user_fragments.remove(user_fragment)
                        user_fragments.insert(target_id, user_fragment)
                except IndexError:
                    # TODO why does an index out of bounds happen?
                    continue
                current_state = user_fragments
                current_state_html = cls.get_html_from_fragments(current_state)
                hashed_current_state = str(hash(current_state_html))
                if hashed_current_state not in nodes:
                    nodes[hashed_current_state] = {
                        'html': current_state_html,
                        'payload': 0
                    }
                nodes[hashed_current_state]['payload'] += 1
                edge_exists = False
                for edge in edges:
                    source = edge.get('source')
                    target = edge.get('target')
                    if source == hashed_previous_state and target == hashed_current_state:
                        edge_exists = True
                        edge['payload'] += 1
                        break
                if not edge_exists:
                    edges.append({
                        'source': hashed_previous_state,
                        'target': hashed_current_state,
                        'action': action,
                        'payload': 1
                    })
                hashed_previous_state = hashed_current_state

            if correct:
                nodes[hashed_current_state]['type'] = 'accept'
            else:
                nodes[hashed_current_state]['type'] = 'end'

        return {'nodes': nodes, 'edges': edges}

    @classmethod
    def get_fragments(cls, content):
        user_fragments = []
        source_fragments = []

        if content and content.get('public', {}).get('fragments'):
            fragments = content.get('public').get('fragments')
            for fragment in fragments:
                if fragment.get('type') == 'fixed':
                    user_fragments.append(fragment)
                else:
                    source_fragments.append(fragment)

        return {
            'user_fragments': user_fragments,
            'source_fragments': source_fragments
        }

    @classmethod
    def get_statistic(cls, results, content, solution):
        statistic = {}
        fragments = cls.get_fragments(content)
        statistic['graph'] = cls.build_graph(results,
                                             fragments['user_fragments'],
                                             fragments['source_fragments'])
        return statistic
