from . import Format


class FillInTheBlank(Format):
    @classmethod
    def get_statistic(cls, results, content, solution):
        statistic = {}
        frequency = {}
        for result in results:
            solution = result.solution
            if solution and 'inputs' in solution:
                inputs = solution['inputs']
                for input_key in inputs:
                    input_value = inputs[input_key]
                    if not input_key in frequency:
                        frequency[input_key] = {}
                    if not input_value in frequency[input_key]:
                        frequency[input_key][input_value] = 0
                    frequency[input_key][input_value] += 1
        statistic['frequencies'] = frequency
        return statistic
