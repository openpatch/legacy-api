class Format():
    @classmethod
    def get_statistic(cls, results, content, solution):
        return {}

    @classmethod
    def evaluate(cls, submittedSolution, solution, content):
        return submittedSolution == solution
