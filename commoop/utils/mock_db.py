import random

from commoop import db
from commoop.models.member import Member
from commoop.models.format import Format
from commoop.models.item import Item
from commoop.models.role import Role
from commoop.models.item_result import ItemResult
from commoop.models.test import Test
from commoop.models.item_group import ItemGroup
from commoop.models.survey import Survey
from datetime import date, timedelta


def setup_mock_db():
    admin = Role(type='admin')

    author = Role(type='author')

    data_analyst = Role(type='data_analyst')

    user = Role(type='user')

    # admin-user
    username = 'admin'
    password = 'admin'
    admin1 = Member(username=username, role=admin)
    admin1.hash_password(password)
    db.session.add(admin1)

    username = 'nimda'
    password = '123pass'
    admin2 = Member(username=username, role=admin)
    admin2.hash_password(password)
    db.session.add(admin2)

    username = 'root'
    password = '******'
    admin3 = Member(username=username, role=admin)
    admin3.hash_password(password)
    db.session.add(admin3)

    username = 'alice'
    password = 'a'
    au1 = Member(username=username, role=author)
    au1.hash_password(password)
    db.session.add(au1)

    username = 'bob'
    password = 'b'
    au2 = Member(username=username, role=author)
    au2.hash_password(password)
    db.session.add(au2)

    username = 'carl'
    password = 'c'
    au3 = Member(username=username, role=author)
    au3.hash_password(password)
    db.session.add(au3)

    username = 'david'
    password = 'tobinski'
    da1 = Member(username=username, role=data_analyst)
    da1.hash_password(password)
    db.session.add(da1)

    username = 'johannes'
    password = 'hartig'
    da2 = Member(username=username, role=data_analyst)
    da2.hash_password(password)
    db.session.add(da2)

    username = 'stephen'
    password = 'wolfram'
    da3 = Member(username=username, role=data_analyst)
    da3.hash_password(password)
    db.session.add(da3)

    username = 'dave'
    password = 'd'
    us1 = Member(username=username, role=user)
    us1.hash_password(password)
    db.session.add(us1)

    username = 'eve'
    password = 'e'
    us2 = Member(username=username, role=user)
    us2.hash_password(password)
    db.session.add(us2)

    username = 'john'
    password = 'doe'
    us3 = Member(username=username)
    us3.hash_password(password)
    db.session.add(us3)

    trc = Format(
        type='tracing-code',
        color_hex='ff4500',
        code="trc",
        content_schema={},
        solution_schema={}
    )
    db.session.add(trc)

    que = Format(
        type='questionnaire',
        color_hex='ffa500',
        code="que",
        content_schema={},
        solution_schema={}
    )
    db.session.add(que)

    c_t = Format(
        type='c-test',
        color_hex='800000',
        code="c-t",
        content_schema={},
        solution_schema={}
    )
    db.session.add(c_t)

    pap = Format(
        type='parsons-puzzle',
        color_hex='6fd3ff',
        code="pap",
        content_schema={},
        solution_schema={}
    )
    db.session.add(pap)

    cot = Format(
        type='copy-text',
        color_hex='3f48cc',
        code="cot",
        content_schema={},
        solution_schema={}
    )
    db.session.add(cot)

    ins = Format(
        type='instructions',
        color_hex='008000',
        code="ins",
        content_schema={},
        solution_schema={}
    )
    db.session.add(ins)

    syn = Format(
        type='syntax-semantics',
        color_hex='b5e61d',
        code="syn",
        content_schema={},
        solution_schema={}
    )
    db.session.add(syn)

    hig = Format(
        type='highlight-text',
        color_hex='a349a4',
        code="hig",
        content_schema={},
        solution_schema={}
    )
    db.session.add(hig)

    mc = Format(
        type='multiple-choice',
        color_hex='009688',
        code="mc",
        content_schema={},
        solution_schema={}
    )
    db.session.add(mc)

    formats = [trc, pap, mc, hig]

    members = [admin1, admin2, admin3, au1, au2, au3]

    users = [us1, us2]

    cities = ['AC', 'BO', 'E', 'DU', 'PB', 'W']

    terms = ['SS17', 'WS17', 'SS18', 'WS18', 'SS19', 'WS19', 'SS20', 'WS20']

    items = fill_with_random_item(formats, members, 500)

    item_groups = generate_random_item_groups(items, 100)

    item_results = generate_random_results(items, users, 150)

    tests = generate_random_tests(item_groups, members, 80)

    surveys = generate_random_surveys(tests, cities, terms, members, 34)

    print('\n ***Database getting filled with***')
    print('12 members')
    print(str(len(formats)) + ' formats')
    print(str(len(items)) + ' items')
    print(str(len(item_groups)) + ' item groups')
    print(str(len(item_results)) + ' item results')
    print(str(len(tests)) + ' tests')
    print('and ' + str(len(surveys)) + ' surveys')

    db.session.commit()


def fill_with_random_item(formats, members, count):
    items = []
    while count > 0:
        rand_member_index = random.randint(0, len(members) - 1)
        rand_format_index = random.randint(0, len(formats) - 1)
        member = members[rand_member_index]
        format = formats[rand_format_index]
        check = random.randint(1, 10) == 1

        item = Item(
            name=member.username + '_' + format.type + '_' + str(count),
            # check=check,
            solution={},
            content={},
            format=format,
            member=member
        )
        member.items_permitted.append(item)
        items.append(item)
        db.session.add(item)
        count -= 1
    return items


def generate_random_item_groups(items, count):
    item_groups = []
    while count > 0:
        ig_items = random.sample(items, random.randint(5, 10))
        is_randomized = random.randint(1, 10) == 1
        is_jumpable = random.randint(1, 10) == 1
        item_group = ItemGroup(
            is_randomized=is_randomized,
            is_jumpable=False,
            is_adaptive=False,
            needs_to_be_correct=False,
            items=ig_items
        )
        item_groups.append(item_group)
        db.session.add(item_group)
        count -= 1
    return item_groups


def generate_random_results(items, users, count):
    item_results = []
    while count > 0:
        rand_item_index = random.randint(0, len(items) - 1)
        rand_user_index = random.randint(0, len(users) - 1)
        item_result = ItemResult(
            member=users[rand_user_index],
            item=items[rand_item_index],
            correct=random.randint(0, 1) == 1
        )
        item_results.append(item_result)
        db.session.add(item_result)
        count -= 1
    return item_results

# Generate a number of surveys during specific terms in specific cities
# out of a number of tests, written by members


def generate_random_surveys(tests, cities, terms, members, count):
    surveys = []
    while count > 0:
        rand_member_index = random.randint(0, len(members) - 1)
        rand_city_index = random.randint(0, len(cities) - 1)
        rand_term_index = random.randint(0, len(terms) - 1)
        rand_test_index = random.randint(0, len(tests) - 1)
        rand_start = random.randint(0, 20)
        rand_end = random.randint(25, 60)
        starts_on = None
        ends_on = None
        if rand_start > 7:
            starts_on = date.today() + timedelta(days=rand_start)
        if rand_end > 30:
            ends_on = date.today() + timedelta(days=rand_end)
        survey = Survey(
            member=members[rand_member_index],
            city=cities[rand_city_index],
            term=terms[rand_term_index],
            name=cities[rand_city_index] + '_' +
            terms[rand_term_index] + '_' + str(count),
            test=tests[rand_test_index],
            starts_on=starts_on,
            ends_on=ends_on
        )
        surveys.append(survey)
        db.session.add(survey)
        count -= 1
    return surveys

# Generate count tests out of a larger number of items, written by members


def generate_random_tests(item_groups, members, count):
    tests = []
    while count > 0:
        rand_member_index = random.randint(0, len(members) - 1)
        test_igs = random.sample(item_groups, random.randint(1, 4))
        test = Test(
            member=members[rand_member_index],
            name='Test_' + str(count),
            item_groups=test_igs
        )
        # add something between 5 and 20 items to each test
        tests.append(test)
        db.session.add(test)
        count -= 1
    return tests
