import json

def make_hash(o):
    dump=json.dumps(o, sort_keys=True)
    dump=dump.lower().replace(' ', '')
    return hash(dump)