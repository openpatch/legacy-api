import random
import string

from commoop import db
from commoop.models.member import Member
from commoop.models.role import Role

from flask_script import Command, Manager, Option


class MemberCommand(Command):
    option_list = (
        Option('--mode', '-m', dest='mode', default='add'),
        Option('--username', '-u', dest='username'),
        Option('--firstname', '-f', dest='first_name'),
        Option('--lastname', '-l', dest='last_name'),
        Option('--email', '-e', dest='email'),
        Option('--role', '-r', dest='role', default='user'),
        Option('--password', '-p', dest='password')
    )

    def run(self, mode, username, email, first_name, last_name, role, password):
        if mode == 'add':
            member = Member(
                username=username,
                first_name=first_name,
                last_name=last_name,
                email=email,
                role=self.get_role(role)
            )

            if password:
                member.hash_password(password)
                db.session.add(member)
                db.session.commit()
            else:
                password = ''.join(random.SystemRandom().choice(
                    string.ascii_uppercase + string.digits) for _ in range(20))
                member.hash_password(password)
                db.session.add(member)
                db.session.commit()

                print('Password: ' + password)

        elif mode == 'modify':
            member = Member.query.filter_by(username=username).first()
            if not member:
                print ('no member ' + username + ' found')
                return
            if password:
                member.hash_password(password)
            if role:
                member.role = self.get_role(role)
            db.session.commit()

        print('success')

    def get_role(self, role):
        return Role.query.filter_by(type=role).first()
