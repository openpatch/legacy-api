from . import Response


class Text(Response):
    @classmethod
    def get_statistic(cls, results, response, responseKey, solution):
        statistic = {}
        frequency = {'NA': 0}
        num_words = 0
        num_responses = 0
        for result in results:
            response = cls.get_response_from_result(result, responseKey)
            if not response:
                frequency['NA'] += 1
                continue
            num_responses += 1
            if not response in frequency:
                if response == 'NA':
                    response = '$NA'
                frequency[str(response)] = 0
            frequency[str(response)] += 1
            num_words += 1
        statistic['frequency'] = frequency
        statistic['num_words'] = num_words
        if num_responses > 0:
            statistic['avg_words'] = num_words / num_responses
        else:
            statistic['avg_words'] = 0
        return statistic
