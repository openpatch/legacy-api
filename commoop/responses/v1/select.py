from . import Response


class Select(Response):
    @classmethod
    def get_statistic(cls, results, response, responseKey, solution):
        statistic = {}
        frequency = {
            'NA': 0
        }
        for result in results:
            response = cls.get_response_from_result(result, responseKey)
            if not response:
                frequency['NA'] += 1
                continue
            if not response in frequency:
                if response == 'NA':
                    response = '$NA'
                frequency[str(response)] = 0
            frequency[str(response)] += 1
        statistic['frequency'] = frequency
        return statistic
