from . import Response


class Choice(Response):
    @classmethod
    def get_statistic(cls, results, response, responseKey, solution):
        statistic = {
        }
        frequency = {
            'NA': 0
        }
        for result in results:
            resultResponse = cls.get_response_from_result(result, responseKey)
            if not resultResponse:
                frequency['NA'] += 1
                continue
            for choiceKey in resultResponse:
                if resultResponse[choiceKey]:
                    choice = response['choices'][choiceKey]
                    if choice == 'NA':
                        choice = '$NA'
                    if not choice in frequency:
                        frequency[str(choice)] = 0
                    frequency[str(choice)] += 1
        statistic['frequency'] = frequency
        return statistic
