import re


class Response():
    @classmethod
    def evaluate_regex(cls, submittedSolution, solution):
        pattern = re.compile(str(solution))
        pattern.match(str(submittedSolution))
        return pattern is not None

    @classmethod
    def evaluate_checked(cls, submittedSolution, solution):
        return submittedSolution == solution

    @classmethod
    def evaluate(cls, submittedSolution, solution):
        return submittedSolution == solution

    @classmethod
    def get_statistic(cls, results, response, responseKey, solution):
        num_correct = 0
        count = 0
        for result in results:
            if result.correct:
                num_correct += 1
            count += 1
        return {
            'count': count,
            'num_correct': num_correct
        }

    @classmethod
    def get_response_from_result(cls, result, responseKey):
        solution = result.solution
        response = None
        if solution and 'responses' in solution and responseKey in solution['responses']:
            response = solution['responses'][responseKey]
        return response
