from . import Response


class Matrix(Response):
    @classmethod
    def get_statistic(cls, results, response, responseKey, solution):
        statistic = {}
        frequencies = {}

        allow_additional = False
        if 'allowAdditional' in response:
            allow_additional = response['allowAdditional']

        columns = []
        if 'columns' in response:
            columns = response['columns']

        rows = []
        if 'rows' in response:
            rows = response['rows']

        input = 'single-choice'
        if 'input' in response:
            input = response['input']['type']

        def update_frequency(row, rowIndex, resultResponse):
            if not row in frequencies:
                frequencies[row] = {'frequency': { 'NA': 0 }, 'type': input}
            frequency = frequencies[row]['frequency']
            values = 'values'
            if allow_additional:
                values = 'additionalValues'
            if resultResponse and values in resultResponse:
                responseValues = resultResponse[values]
                if str(rowIndex) in responseValues:
                    rowResponseValues = responseValues[str(rowIndex)]
                    for columnIndex in rowResponseValues:
                        column = columns[int(columnIndex)]
                        if input != 'text' and rowResponseValues[columnIndex]:
                            if not column in frequency:
                                frequency[column] = 0
                            frequency[column] += 1
                        else:
                            rowResponseValue = rowResponseValues[columnIndex]
                            if not rowResponseValue in frequency:
                                frequency[rowResponseValue] = {}
                            if not column in frequency[rowResponseValue]:
                                frequency[rowResponseValue][column] = 0
                            frequency[rowResponseValue][column] += 1

                else:
                    frequency['NA'] += 1
            else:
                frequency['NA'] += 1
            rowIndex += 1
            return frequency

        for result in results:
            resultResponse = cls.get_response_from_result(result, responseKey)
            rowIndex = 0
            for row in rows:
                update_frequency(row, rowIndex, resultResponse)
                rowIndex += 1

            rowIndex = 0
            if resultResponse and 'additionalRows' in resultResponse:
                for row in resultResponse['additionalRows']:
                    update_frequency(row, rowIndex, resultResponse)
                    rowIndex += 1

        statistic['frequencies'] = frequencies
        return statistic
