from commoop import db
from commoop.models.base import Base


class Format(Base):
    __tablename__ = 'format'

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(64), unique=True)
    code = db.Column(db.String(3), unique=True)
    items = db.relationship('Item')
    content_schema = db.Column(db.JSON)
    solution_schema = db.Column(db.JSON)
    color_hex = db.Column(db.String(6))

    def __repr__(self):
        return '<Format %r>' % self.type
