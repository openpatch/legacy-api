from commoop import db
from commoop.models.item_abstract_statistic import ItemAbstractStatistic
from commoop.models.item_result import ItemResult


class ItemStatistic(ItemAbstractStatistic):
    __tablename__ = 'item_statistic'

    item_id = db.Column(db.Integer, db.ForeignKey('item.id'))
    item = db.relationship('Item', back_populates='statistic')

    def as_dict(self, filters={}):
        updated_on = self.updated_on
        results = ItemResult.query.filter_by(item=self.item).order_by(
            ItemResult.updated_on.desc()).all()

        last_result = None
        try:
            last_result = results[0]
        except IndexError:
            last_result = None

        if filters:
            dict_obj = self.generate(filters=filters, results=results)
        elif last_result and (not updated_on
                              or updated_on < last_result.updated_on):
            dict_obj = self.update(results=results)
        else:
            dict_obj = super(ItemStatistic, self).as_dict()
        return dict_obj
