from datetime import datetime

from commoop import db
from commoop.formats.v1.fill_in_the_blank import FillInTheBlank
from commoop.formats.v1.highlight import Highlight
from commoop.formats.v1.parsons_puzzle import ParsonsPuzzle
from commoop.formats.v1.trace import Trace
from commoop.models.association_item_category import association_item_category
from commoop.models.association_item_favorized import \
    association_item_favorized
from commoop.models.association_item_group import association_item_group
from commoop.models.association_item_permitted import \
    association_item_permitted
from commoop.models.base import Base
from commoop.models.item_statistic import ItemStatistic
from commoop.models.item_survey_statistic import ItemSurveyStatistic
from commoop.models.item_test_statistic import ItemTestStatistic
from commoop.responses.v1.choice import Choice
from commoop.responses.v1.matrix import Matrix
from commoop.responses.v1.select import Select
from commoop.responses.v1.text import Text
from commoop.statistics import descriptive
from sqlalchemy import UniqueConstraint


class Item(Base):
    __tablename__ = 'item'
    __table_args__ = (UniqueConstraint(
        'name', 'format_id', 'member_id',
        name='_name_format_id_member_id_uc'), )

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    assignment = db.Column(db.Text)
    allow_note = db.Column(db.Boolean)
    child_id = db.Column(db.Integer, db.ForeignKey('item.id'))
    child = db.relationship('Item', foreign_keys=[child_id])
    format_id = db.Column(db.Integer, db.ForeignKey('format.id'))
    format = db.relationship('Format', back_populates='items')
    categories = db.relationship(
        'Category',
        secondary=association_item_category,
        back_populates='items')
    responses = db.Column(db.JSON)
    content = db.Column(db.JSON)
    solution = db.Column(db.JSON)
    hash = db.Column(db.String(2048))
    member_id = db.Column(db.Integer, db.ForeignKey('member.id'))
    member = db.relationship('Member', back_populates='items')
    results = db.relationship('ItemResult', back_populates='item')
    statistic = db.relationship(
        "ItemStatistic", uselist=False, back_populates="item")
    item_groups = db.relationship(
        'ItemGroup', secondary=association_item_group, back_populates='items')
    members_permitted = db.relationship(
        'Member',
        secondary=association_item_permitted,
        back_populates='items_permitted')
    members_favorized = db.relationship(
        'Member',
        secondary=association_item_favorized,
        back_populates='items_favorized')

    def as_dict(self, filters={}):
        dict_obj = super(Item, self).as_dict()
        dict_obj['member'] = self.member.as_dict()
        dict_obj['format'] = self.format.as_dict()
        dict_obj['members_permitted'] = [
            member.id for member in self.members_permitted
        ]
        stats = self.get_statistic(filters=filters)
        dict_obj['stats'] = stats
        dict_obj['is_deletable'] = stats.get('num_results') == 0 and stats.get(
            'num_tests') == 0
        dict_obj['is_editable'] = stats.get('num_results') == 0
        return dict_obj

    def __repr__(self):
        return '<Item %r>' % self.id

    def get_statistic(self, filters={}):
        survey_ids = filters.get('survey_ids', [])
        test_ids = filters.get('test_ids', [])

        item_statistic = None
        if len(survey_ids) == 1 and len(test_ids) <= 1:
            # try to find cached statistic
            item_statistic = ItemSurveyStatistic.query.filter_by(
                survey_id=survey_ids[0], item_id=self.id).first()
            if not item_statistic:
                # generate survey tatistic
                item_statistic = ItemSurveyStatistic()
                item_statistic.item_id = self.id
                item_statistic.item = self
                item_statistic.survey_id = survey_ids[0]
                db.session.add(item_statistic)
            # we already used the cache no need to filter
            filters = {}
        elif len(survey_ids) == 0 and len(test_ids) == 1:
            # try to find cached statistic
            item_statistic = ItemTestStatistic.query.filter_by(
                test_id=test_ids[0], item_id=self.id).first()
            if not item_statistic:
                # generate survey tatistic
                item_statistic = ItemTestStatistic()
                item_statistic.item_id = self.id
                item_statistic.item = self
                item_statistic.test_id = test_ids[0]
                db.session.add(item_statistic)
            # we already used the cache no need to filter
            filters = {}
        else:
            # try to find cached statistic
            if not self.statistic:
                # generate item statistic
                self.statistic = ItemStatistic()
            item_statistic = self.statistic
        return item_statistic.as_dict(filters=filters)

    def get_descendants_ids(self):
        if self.child:
            return [self.child.id] + self.child.get_descendants()
        return []

    def get_tests(self):
        tests = []
        for ig in self.item_groups:
            tests += ig.tests
        return tests

    def get_surveys(self):
        surveys = []
        for ig in self.item_groups:
            for test in ig.tests:
                surveys += test.surveys
        return list(set(surveys))

    def get_detailed_statistic(self, filters={}):
        statistic = {'responses': {}, 'format': {}}

        # generate responses statistic
        results = self.results

        filter_surveys = filters.get('survey_ids', None)
        filter_tests = filters.get('test_ids', None)
        filter_results = filters.get('result_ids', None)

        if filter_surveys:
            results = [
                r for r in results if r.get_survey_id() in filter_surveys
            ]
        if filter_tests:
            results = [r for r in results if r.test_id in filter_tests]
        if filter_results:
            results = [r for r in results if r.id in filter_results]

        responses = self.responses
        solution = self.solution
        solutionResponses = solution.get('responses', {})

        times = []
        for result in results:
            time = (result.end_time - result.start_time).total_seconds()
            times.append(time)

        statistic['times'] = descriptive.summary(times)

        for responseKey in responses:
            response = responses[responseKey]
            responseType = response['type']
            solutionResponse = {}
            if responseKey in solutionResponses:
                solutionResponse = solutionResponses[responseKey]
            responseStatistic = {}
            if responseType == 'multiple-choice' or responseType == 'single-choice':
                responseStatistic = Choice.get_statistic(
                    results, response, responseKey, solutionResponse)
            elif responseType == 'matrix':
                responseStatistic = Matrix.get_statistic(
                    results, response, responseKey, solutionResponse)
            elif responseType == 'select':
                responseStatistic = Select.get_statistic(
                    results, response, responseKey, solutionResponse)
            elif responseType == 'text':
                responseStatistic = Text.get_statistic(
                    results, response, responseKey, solutionResponse)
            responseStatistic['type'] = responseType
            responseStatistic['label'] = response.get('label', '')
            statistic['responses'][responseKey] = responseStatistic

        if 'responses' in solution:
            del solution['responses']
        formatType = self.format.type
        if formatType == 'highlight':
            statistic['format'] = Highlight.get_statistic(
                results, self.content, solution)
        elif formatType == 'parsons-puzzle':
            statistic['format'] = ParsonsPuzzle.get_statistic(
                results, self.content, solution)
        elif formatType == 'trace':
            statistic['format'] = Trace.get_statistic(results, self.content,
                                                      solution)
        elif formatType == 'fill-in-the-blank':
            statistic['format'] = FillInTheBlank.get_statistic(
                results, self.content, solution)
        statistic['format']['type'] = formatType

        return statistic

    @classmethod
    def _evaluate(cls, submittedSolution, solution, responses, content,
                  formatType):
        if not solution:
            return {'correct': True}

        correct = True

        # evaluate responses
        evaluatedResponses = {}
        if 'responses' in solution:
            solutionResponses = solution.get('responses', {})
            submittedSolutionResponses = submittedSolution.get('responses', {})
            if solutionResponses:
                for responseIndex in responses:
                    response = responses[responseIndex]
                    responseType = response['type']
                    if not str(responseIndex) in solutionResponses:
                        continue
                    if not str(responseIndex) in submittedSolutionResponses:
                        evaluatedResponses[str(responseIndex)] = False
                        correct = correct and False
                        continue
                    solutionReponse = solutionResponses[str(responseIndex)]
                    submittedSolutionResponse = submittedSolutionResponses[str(
                        responseIndex)]
                    evaluatedResponse = True
                    if responseType == 'multiple-choice' or responseType == 'single-choice':
                        evaluatedResponse = Choice.evaluate(
                            submittedSolutionResponse, solutionReponse)
                    elif responseType == 'matrix':
                        evaluatedResponse = Matrix.evaluate(
                            submittedSolutionResponse, solutionReponse)
                    elif responseType == 'select':
                        evaluatedResponse = Select.evaluate(
                            submittedSolutionResponse, solutionReponse)
                    elif responseType == 'text':
                        evaluatedResponse = Text.evaluate(
                            submittedSolutionResponse, solutionReponse)
                    evaluatedResponses[str(responseIndex)] = evaluatedResponse
                    correct = correct and evaluatedResponse
            if 'responses' in submittedSolution:
                del submittedSolution['responses']
            if 'responses' in solution:
                del solution['responses']

        # evaluate content
        formatType = formatType
        evaluatedContent = True
        if formatType == 'highlight':
            evaluatedContent = Highlight.evaluate(submittedSolution, solution,
                                                  content)
        elif formatType == 'parsons-puzzle':
            evaluatedContent = ParsonsPuzzle.evaluate(submittedSolution,
                                                      solution, content)
        elif formatType == 'trace':
            evaluatedContent = Trace.evaluate(submittedSolution, solution,
                                              content)
        elif formatType == 'fill-in-the-blank':
            evaluatedContent = FillInTheBlank.evaluate(submittedSolution,
                                                       solution, content)
        correct = correct and evaluatedContent

        return {
            'correct': correct,
            'responses': evaluatedResponses,
            'content': evaluatedContent
        }

    def evaluate(self, submittedSolution):
        return Item._evaluate(submittedSolution.copy(), self.solution.copy(),
                              self.responses, self.content, self.format.type)

    def is_permitted_for_read(self, user):
        return user.role.type == 'admin' \
            or user == self.member \
            or user in self.members_permitted

    def is_permitted_for_write(self, user):
        return user.role.type == 'admin' \
            or user == self.member
