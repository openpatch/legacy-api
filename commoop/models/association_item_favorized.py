from commoop import db


association_item_favorized = db.Table('association_item_favorized', db.metadata,
                                     db.Column('item_id', db.Integer,
                                               db.ForeignKey('item.id')),
                                     db.Column('member_id', db.Integer,
                                               db.ForeignKey('member.id'))
                                     )
