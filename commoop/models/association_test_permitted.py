from commoop import db


association_test_permitted = db.Table('association_test_permitted', db.metadata,
                                     db.Column('test_id', db.Integer,
                                               db.ForeignKey('test.id')),
                                     db.Column('member_id', db.Integer,
                                               db.ForeignKey('member.id'))
                                     )
