from commoop import db
from commoop.models.association_survey_favorized import \
    association_survey_favorized
from commoop.models.association_survey_permitted import \
    association_survey_permitted
from commoop.models.base import Base
from commoop.models.survey_statistic import SurveyStatistic
from hashids import Hashids
from passlib.apps import custom_app_context as pwd_context
from sqlalchemy import func


class Survey(Base):
    __tablename__ = 'survey'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), unique=True)
    city = db.Column(db.String(128))
    term = db.Column(db.String(128))
    statistic = db.relationship(
        "SurveyStatistic", uselist=False, back_populates="survey")
    starts_on = db.Column(db.Date)
    ends_on = db.Column(db.Date)
    greeting = db.Column(db.Text)
    farewell = db.Column(db.Text)
    show_session_id = db.Column(db.Boolean, default=False)
    show_progress = db.Column(db.Boolean, default=False)
    show_result = db.Column(db.Boolean, default=False)
    results = db.relationship('TestResult', back_populates='survey')
    member_id = db.Column(db.Integer, db.ForeignKey('member.id'))
    member = db.relationship('Member', back_populates='surveys')
    test_id = db.Column(db.Integer, db.ForeignKey('test.id'))
    test = db.relationship('Test', back_populates='surveys')
    url_suffix = db.Column(db.String(128), unique=True)
    password_hash = db.Column(db.String(128))
    members_permitted = db.relationship(
        'Member',
        secondary=association_survey_permitted,
        back_populates='surveys_permitted')
    members_favorized = db.relationship(
        'Member',
        secondary=association_survey_favorized,
        back_populates='surveys_favorized')

    def hash_password(self, password):
        self.password_hash = pwd_context.hash(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def __init__(self, *args, **kwargs):
        super(Survey, self).__init__(*args, **kwargs)
        count = db.session.query(func.max(Survey.id)).scalar()
        self.url_suffix = Hashids(min_length=10).encode(count)

    def as_dict(self):
        dict_obj = super(Survey, self).as_dict()
        del dict_obj['password_hash']
        dict_obj['member'] = self.member.as_dict(public=True)
        dict_obj['num_results'] = len(self.results)
        dict_obj['is_deletable'] = len(self.results) == 0
        dict_obj['is_editable'] = len(self.results) == 0
        dict_obj['members_permitted'] = [
            member.id for member in self.members_permitted
        ]
        dict_obj['password_protection'] = self.password_hash is not None
        dict_obj['stats'] = self.get_statistic()
        return dict_obj

    def __repr__(self):
        return '<Survey %r>' % self.id

    def get_statistic(self):
        if not self.statistic:
            survey_statistic = SurveyStatistic()
            survey_statistic.survey_id = self.id
            survey_statistic.test_id = self.test_id
            db.session.add(survey_statistic)
            self.statistic = survey_statistic
        return self.statistic.as_dict()

    def is_permitted_for_read(self, user):
        return user.role.type == 'admin' \
            or user == self.member \
            or user in self.members_permitted

    def is_permitted_for_write(self, user):
        return user.role.type == 'admin' \
            or user == self.member
