from commoop.models.base import Base
from commoop import db
from commoop.models.association_item_category import association_item_category
from commoop.models.association_member_item_category import association_member_item_category


class Category(Base):
    __tablename__ = 'category'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    items = db.relationship(
        'Item', secondary=association_item_category, back_populates='categories')
    members = db.relationship(
        'Member', secondary=association_member_item_category, back_populates='item_categories')
    public = db.Column(db.Boolean, default=False)

    def __init__(self, type):
        self.type = type

    def is_permitted_for_read(self, user):
        return user.role.type == 'admin' \
            or user in self.members \
            or self.public

    def is_permitted_for_write(self, user):
        return user.role.type == 'admin' \
            or (user in self.members and not self.public)

    def __repr__(self):
        return '<Category %r>' % self.name
