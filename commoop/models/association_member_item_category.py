from commoop import db


association_member_item_category = db.Table('association_member_item_category', db.metadata,
                                            db.Column('member_id', db.Integer,
                                                      db.ForeignKey('member.id')),
                                            db.Column('category_id', db.Integer,
                                                      db.ForeignKey('category.id'))
                                            )
