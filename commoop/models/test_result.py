from datetime import datetime

from commoop import db
from commoop.models.base import Base
from hashids import Hashids
from sqlalchemy import func


class TestResult(Base):
    __tablename__ = "test_result"

    id = db.Column(db.Integer, primary_key=True)
    test_id = db.Column(db.Integer, db.ForeignKey("test.id"))
    test = db.relationship("Test", back_populates="results")
    survey_id = db.Column(db.Integer, db.ForeignKey("survey.id"))
    survey = db.relationship("Survey", back_populates="results")
    session_hash = db.Column(db.String(64), unique=True)
    abort = db.Column(db.Boolean, default=False)
    abort_reason = db.Column(db.Text)
    item_group_results = db.relationship(
        "ItemGroupResult", back_populates="test_result"
    )
    num_item_groups_correct = db.Column(db.JSON)
    num_items_correct = db.Column(db.Integer)
    updated_correct_on = db.Column(db.DateTime)
    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)

    def __init__(self, *args, **kwargs):
        super(TestResult, self).__init__(*args, **kwargs)
        count = db.session.query(func.max(TestResult.id)).scalar()
        self.session_hash = Hashids(min_length=6).encode(count)

    def as_dict(self, nested=False):
        dict_obj = super(TestResult, self).as_dict()

        correct = self.get_correct()
        dict_obj["num_items_correct"] = correct.get("num_items_correct")
        dict_obj["num_item_groups_correct"] = correct.get("num_item_groups_correct")

        if nested:
            dict_obj["item_group_results"] = [
                item_group_result.as_dict(nested=nested)
                for item_group_result in self.item_group_results
            ]

        return dict_obj

    def get_correct(self):
        correct = {
            "num_items_correct": self.num_items_correct,
            "num_item_groups_correct": self.num_item_groups_correct,
        }
        if not self.updated_correct_on:
            correct = self.update_correct()
        return correct

    def generate_correct(self):
        num_items_correct = 0
        num_item_groups_correct = {}

        for item_group_result in self.item_group_results:
            num_item_group_correct = item_group_result.get_num_correct()
            num_item_groups_correct[item_group_result.id] = num_item_group_correct
            num_items_correct += num_item_group_correct

        return {
            "num_items_correct": num_items_correct,
            "num_item_groups_correct": num_item_groups_correct,
        }

    def update_correct(self):
        correct = self.generate_correct()

        self.num_item_groups_correct = correct["num_item_groups_correct"]
        self.num_items_correct = correct["num_items_correct"]
        self.updated_correct_on = datetime.now()
        db.session.commit()

        return correct

    def get_item_results(self):
        item_results = []
        for item_group_result in self.item_group_results:
            item_results += item_group_result.item_results
        return item_results

    def export(self):
        item_results = self.get_item_results()
        export = {
            "000_id": self.id,
            "000_session": self.session_hash,
            "000_survey_id": self.survey_id,
            "000_test_id": self.test_id,
            "000_abort": self.abort,
            "000_abort_reason": self.abort_reason,
            "000_start": self.start,
            "000_end": self.end,
            "000_time": (self.end - self.start).total_seconds()
            if self.end and self.start
            else None,
        }
        headers = set(export.keys())
        i = 1
        for item_result in item_results:
            export_item_result = item_result.export(i)
            export = {**export, **export_item_result}
            headers = headers.union(export_item_result.keys())
            i += 1
        return {"data": export, "headers": headers}

    def is_permitted_for_read(self, user):
        return (
            user.role.type == "admin"
            or user == self.test.member
            or user == self.survey.member
            or user in self.survey.members_permitted
            or user in self.test.members_permitted
        )

    def is_permitted_for_write(self, user):
        return user.role.type == "admin"

    def __repr__(self):
        return "<TestResult %r>" % self.id
