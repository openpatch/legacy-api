import ast
import json

from commoop import db
from commoop.models.base import Base


class ItemResult(Base):
    __tablename__ = 'item_result'

    id = db.Column(db.Integer, primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'))
    item = db.relationship('Item', back_populates='results')
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))
    test_id = db.Column(db.Integer, db.ForeignKey('test.id'))
    member_id = db.Column(db.Integer, db.ForeignKey('member.id'))
    member = db.relationship('Member', back_populates='item_results')
    solution = db.Column(db.JSON)
    note = db.Column(db.Text)
    actions = db.Column(db.JSON)
    correct = db.Column(db.Boolean)
    item_group_result_id = db.Column(db.Integer,
                                     db.ForeignKey('item_group_result.id'))
    item_group_result = db.relationship(
        'ItemGroupResult', back_populates='item_results')
    item_result_comments = db.relationship(
        'ItemResultComment', back_populates='item_result')
    start_time = db.Column(db.DateTime)
    end_time = db.Column(db.DateTime)

    def __repr__(self):
        return '<ItemResult %r>' % self.id

    def as_dict(self, filter_keys=[], nested=False):
        dict_obj = super(ItemResult, self).as_dict()
        dict_obj['item_result_comments'] = []
        for item_result_comment in self.item_result_comments:
            if not item_result_comment.parent_id:
                dict_obj['item_result_comments'].append(
                    item_result_comment.as_dict())
        if self.item_group_result and self.item_group_result.test_result:
            dict_obj[
                'session_hash'] = self.item_group_result.test_result.session_hash
            dict_obj[
                'survey_id'] = self.item_group_result.test_result.survey.id
        return dict_obj

    def get_survey_id(self):
        return self.survey_id

    def get_test_id(self):
        return self.test_id

    def export(self, id):
        export = {}
        format = self.item.format.type
        export['{0:03d}_item_result_id'.format(id)] = self.id
        export['{0:03d}_item_id'.format(id)] = self.item.id
        export['{0:03d}_item_group_id'.format(
            id)] = self.item_group_result.item_group_id
        export['{0:03d}_correct'.format(id)] = self.correct
        export['{0:03d}_format'.format(id)] = format
        export['{0:03d}_start'.format(id)] = self.created_on
        export['{0:03d}_end'.format(id)] = self.updated_on
        export['{0:03d}_time'.format(id)] = (
            self.updated_on - self.created_on).total_seconds()

        solution = self.solution

        if not isinstance(solution, dict):
            return export

        raw_responses = solution.get('responses', {})
        item_responses = self.item.responses

        item_response_id = 0

        # helper function because some responses are dict and some are list
        def generate_response_export(item_response_id, raw_item_response):
            response = raw_responses.get(str(item_response_id))
            item_response = ast.literal_eval(str(raw_item_response))

            if not isinstance(item_response, dict):
                return

            export['{0:03d}_{1:03d}_response_type'.format(
                id, int(item_response_id))] = item_response['type']

            if item_response['type'] == 'single-choice' or item_response['type'] == 'multiple-choice':
                if isinstance(response, dict):
                    response = response.keys()
                    export['{0:03d}_{1:03d}'.format(
                        id, int(item_response_id))] = ','.join(response)
            elif item_response['type'] == 'text':
                export['{0:03d}_{1:03d}'.format(
                    id, int(item_response_id))] = response
            elif item_response['type'] == 'matrix':
                input = item_response.get('input', {})
                input_type = input.get('type')
                if isinstance(response, dict):
                    values = response.get('values')
                    if input_type == 'text':
                        for key1, value1 in values.items():
                            for key2, value2 in value1.items():
                                export[
                                    '{0:03d}_{1:03d}_{2:02d}_{3:02d}'.format(
                                        id, int(item_response_id), int(key1),
                                        int(key2))] = value2
                    else:
                        for key1, value1 in values.items():
                            value1 = value1.keys()
                            export['{0:03d}_{1:03d}_{2:02d}'.format(
                                id, int(item_response_id),
                                int(key1))] = ','.join(value1)

                    export['{0:03d}_{1:03d}_av'.format(id, int(
                        item_response_id))] = response.get('additionalValues')
                    export['{0:03d}_{1:03d}_ar'.format(id, int(
                        item_response_id))] = response.get('additionalRows')
            else:
                export['{0:03d}_{1:03d}'.format(
                    id, int(item_response_id))] = response

        if isinstance(item_responses, dict):
            for item_response_id, raw_item_response in item_responses.items():
                generate_response_export(item_response_id, raw_item_response)
        elif isinstance(item_responses, list):
            for raw_item_response in item_responses:
                generate_response_export(item_response_id, raw_item_response)
                item_response_id += 1

        if format == 'fill-in-the-blank':
            raw_inputs = solution.get('inputs', {})
            for raw_input_id, raw_input in raw_inputs.items():
                export['{0:03d}_{1:03d}'.format(id,
                                                int(raw_input_id))] = raw_input
        elif format == 'highlight':
            raw_highlights = solution.get('highlights', '')
            raw_highlights = raw_highlights.split('|')
            del raw_highlights[0]
            highlights = []
            for raw_highlight in raw_highlights:
                raw_highlight = raw_highlight.split('$')
                highlights.append({
                    'start': raw_highlight[0],
                    'end': raw_highlight[1]
                })
            export['{0:03d}_highlights'.format(id)] = highlights
        return export

    def is_permitted_for_read(self, user):
        return user.role.type == 'admin' or user == self.item.member or user in self.item.members_permitted

    def is_permitted_for_write(self, user):
        return False
