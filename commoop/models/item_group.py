from commoop import db
from commoop.models.association_item_group import association_item_group
from commoop.models.association_item_group_test import \
    association_item_group_test
from commoop.models.base import Base


class ItemGroup(Base):
    #__versioned__ = {}
    __tablename__ = 'item_group'

    id = db.Column(db.Integer, primary_key=True)
    is_randomized = db.Column(db.Boolean)
    is_jumpable = db.Column(db.Boolean)
    is_adaptive = db.Column(db.Boolean)
    needs_to_be_correct = db.Column(db.Boolean)
    items = db.relationship(
        'Item', secondary=association_item_group, back_populates="item_groups")
    tests = db.relationship(
        'Test',
        secondary=association_item_group_test,
        back_populates="item_groups")
    item_group_results = db.relationship(
        'ItemGroupResult', back_populates='item_group')

    def __repr__(self):
        return '<ItemGroup %r>' % self.id

    def as_dict(self):
        dict_obj = super(ItemGroup, self).as_dict()
        dict_obj['item_group_id'] = self.id
        dict_obj['is_randomized'] = self.is_randomized
        # datetime not serializable and not necessary
        del dict_obj['updated_on']
        del dict_obj['created_on']
        dict_obj['items'] = [item.id for item in self.items]
        return dict_obj
