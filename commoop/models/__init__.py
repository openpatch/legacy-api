from commoop import db
from sqlalchemy import orm
from sqlalchemy_continuum import make_versioned

from . import (category, format, item, item_group, item_group_result,
               item_result, item_result_comment, item_statistic,
               item_survey_statistic, item_test_statistic, member, role,
               survey, survey_statistic, test, test_result, test_statistic)

make_versioned(user_cls='Member')

orm.configure_mappers()
