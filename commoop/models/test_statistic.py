from commoop import db
from commoop.models.test_abstract_statistic import TestAbstractStatistic
from commoop.models.test_result import TestResult


class TestStatistic(TestAbstractStatistic):
    __tablename__ = "test_statistic"

    test_id = db.Column(db.Integer, db.ForeignKey("test.id"))
    test = db.relationship("Test", back_populates="statistic")

    def as_dict(self, filters={}):
        dict_obj = super(TestStatistic, self).as_dict()
        updated_on = self.updated_on
        results = (
            TestResult.query.filter_by(test_id=self.test_id)
            .order_by(TestResult.updated_on.desc())
            .all()
        )

        last_result = None
        try:
            last_result = results[0]
        except IndexError:
            last_result = None

        if filters:
            dict_obj = self.generate(filters=filters, results=results)
        elif last_result and (not updated_on or updated_on < last_result.updated_on):
            dict_obj = self.update(results=results)
        return dict_obj
