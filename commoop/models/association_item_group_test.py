from commoop import db


association_item_group_test = db.Table('association_item_group_test', db.metadata,
                                 db.Column('item_group_id', db.Integer,
                                           db.ForeignKey('item_group.id')),
                                 db.Column('test_id', db.Integer,
                                           db.ForeignKey('test.id'))
                                 )
