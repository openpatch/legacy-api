from datetime import datetime

from commoop import db
from commoop.models.association_item_group_test import \
    association_item_group_test
from commoop.models.association_test_favorized import \
    association_test_favorized
from commoop.models.association_test_permitted import \
    association_test_permitted
from commoop.models.base import Base
from commoop.models.test_statistic import TestStatistic
from commoop.statistics import descriptive


class Test(Base):
    __tablename__ = 'test'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), unique=True)
    results = db.relationship('TestResult', back_populates='test')
    statistic = db.relationship(
        "TestStatistic", uselist=False, back_populates="test")
    member_id = db.Column(db.Integer, db.ForeignKey('member.id'))
    member = db.relationship('Member', back_populates='tests')
    item_groups = db.relationship(
        'ItemGroup',
        secondary=association_item_group_test,
        back_populates='tests')
    item_groups_json = db.Column(db.JSON)
    item_amount = db.Column(db.Integer)
    updated_item_on = db.Column(db.DateTime)
    surveys = db.relationship('Survey', back_populates='test')
    members_permitted = db.relationship(
        'Member',
        secondary=association_test_permitted,
        back_populates='tests_permitted')
    members_favorized = db.relationship(
        'Member',
        secondary=association_test_favorized,
        back_populates='tests_favorized')

    def __repr__(self):
        return '<Test %r>' % self.id

    def __init__(self, *args, **kwargs):
        super(Test, self).__init__(*args, **kwargs)
        self.item_amount = 0
        for item_group in self.item_groups:
            self.item_amount += len(item_group.items)

    def as_dict(self, filters={}):
        dict_obj = super(Test, self).as_dict()
        dict_obj['member'] = self.member.as_dict(public=True)
        dict_obj['surveys'] = [survey.id for survey in self.surveys]
        dict_obj['members_permitted'] = [
            member.id for member in self.members_permitted
        ]

        if not self.updated_item_on or self.updated_item_on < self.updated_on:
            item = self.update_item()
            dict_obj['item_amount'] = item.get('item_amount')
            dict_obj['item_groups_json'] = item.get('item_groups_json')

        dict_obj['item_groups'] = dict_obj['item_groups_json']
        del dict_obj['item_groups_json']
        stats = self.get_statistic(filters=filters)
        dict_obj['stats'] = stats
        dict_obj['is_deletable'] = stats.get('num_results') == 0 and stats.get(
            'num_surveys') == 0
        dict_obj['is_editable'] = stats.get('num_results') == 0
        return dict_obj

    def generate_item(self):
        item_amount = 0
        item_groups_json = []
        for item_group in self.item_groups:
            item_groups_json.append(item_group.as_dict())
            item_amount += len(item_group.items)

        return {
            'item_amount': item_amount,
            'item_groups_json': item_groups_json
        }

    def update_item(self):
        item = self.generate_item()

        self.item_amount = item.get('item_amount')
        self.item_groups_json = item.get('item_groups_json')
        self.updated_item_on = datetime.now()

        db.session.commit()

        return item

    def get_items(self):
        items = []
        for ig in self.item_groups:
            items += ig.items
        return items

    def get_statistic(self, filters={}):
        if not self.statistic:
            test_statistic = TestStatistic()
            test_statistic.test_id = self.id
            db.session.add(test_statistic)
            self.statistic = test_statistic
        return self.statistic.as_dict(filters=filters)

    def is_permitted_for_read(self, user):
        return user.role.type == 'admin' \
            or user == self.member \
            or user in self.members_permitted

    def is_permitted_for_write(self, user):
        return user.role.type == 'admin' \
            or user == self.member
