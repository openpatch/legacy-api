from commoop import db


association_item_category = db.Table('association_item_category', db.metadata,
                                     db.Column('item_id', db.Integer,
                                               db.ForeignKey('item.id')),
                                     db.Column('category_id', db.Integer,
                                               db.ForeignKey('category.id'))
                                     )
