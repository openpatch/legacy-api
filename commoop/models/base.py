from commoop import db


class Base(db.Model):
    __abstract__ = True

    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(
        db.DateTime, default=db.func.now(), onupdate=db.func.now())

    def as_dict(self, filter_keys=[]):
        dict_obj = {c.name: getattr(self, c.name)
                    for c in self.__table__.columns}
        if filter_keys:
            dict_obj = {filter_key: dict_obj[filter_key]
                        for filter_key in filter_keys}

        return dict_obj
