from commoop import db


association_item_test = db.Table('association_item_test', db.metadata,
                                 db.Column('item_id', db.Integer,
                                           db.ForeignKey('item.id')),
                                 db.Column('test_id', db.Integer,
                                           db.ForeignKey('test.id'))
                                 )
