from commoop import db


association_survey_permitted = db.Table('association_survey_permitted', db.metadata,
                                     db.Column('survey_id', db.Integer,
                                               db.ForeignKey('survey.id')),
                                     db.Column('member_id', db.Integer,
                                               db.ForeignKey('member.id'))
                                     )
