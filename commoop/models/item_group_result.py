from commoop import db
from commoop.models.base import Base


class ItemGroupResult(Base):
    #__versioned__ = {}
    __tablename__ = 'item_group_result'

    id = db.Column(db.Integer, primary_key=True)
    item_group_id = db.Column(db.Integer, db.ForeignKey('item_group.id'))
    item_group = db.relationship(
        'ItemGroup', back_populates="item_group_results")
    test_result_id = db.Column(db.Integer, db.ForeignKey('test_result.id'))
    test_result = db.relationship(
        'TestResult', back_populates='item_group_results')
    item_results = db.relationship(
        'ItemResult', back_populates='item_group_result')

    def __repr__(self):
        return '<ItemGroupResult %r>' % self.id

    def as_dict(self, nested=False):
        dict_obj = super(ItemGroupResult, self).as_dict()
        if nested:
            dict_obj['item_results'] = [item_result.as_dict(nested=nested)
                                        for item_result in self.item_results]
        return dict_obj

    def get_num_correct(self):
        num_correct = 0
        for item_result in self.item_results:
            if item_result.correct:
                num_correct += 1
        return num_correct

    def get_item_results_by_id(self):
        item_results = []
        for item_result in self.item_results:
            item_results.append(item_result.id)
        return item_results
