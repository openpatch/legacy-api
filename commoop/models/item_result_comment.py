from commoop import db
from commoop.models.base import Base


class ItemResultComment(Base):
    __tablename__ = 'item_result_comment'

    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Integer, db.ForeignKey('member.id'))
    member = db.relationship('Member', back_populates='item_result_comments')
    text = db.Column(db.Text)
    category = db.Column(db.String(128))
    start_time = db.Column(db.Integer)
    end_time = db.Column(db.Integer)
    parent_id = db.Column(db.Integer, db.ForeignKey('item_result_comment.id'))
    replies = db.relationship('ItemResultComment')
    item_result_id = db.Column(db.Integer, db.ForeignKey('item_result.id'))
    item_result = db.relationship(
        'ItemResult', back_populates='item_result_comments')

    def as_dict(self, filter_keys=[]):
        dict_obj = super(ItemResultComment, self).as_dict()
        dict_obj['replies'] = [reply.as_dict() for reply in self.replies]
        dict_obj['member'] = {
            'username': self.member.username
        }
        return dict_obj

    def is_permitted_for_read(self, user):
        return True

    def is_permitted_for_write(self, user):
        return user == self.member or user.role.type == 'admin'
