from commoop import db


association_item_group = db.Table('association_item_group', db.metadata,
                                 db.Column('item_id', db.Integer,
                                           db.ForeignKey('item.id')),
                                 db.Column('item_group_id', db.Integer,
                                           db.ForeignKey('item_group.id'))
                                 )
