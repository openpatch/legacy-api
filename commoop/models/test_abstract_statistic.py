from datetime import datetime

from commoop import db
from commoop.models.base import Base
from commoop.statistics import descriptive


class TestAbstractStatistic(Base):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)

    num_results = db.Column(db.Integer)
    num_surveys = db.Column(db.Integer)
    num_aborted = db.Column(db.Integer)
    num_unfinished = db.Column(db.Integer)

    time_min = db.Column(db.Integer)
    time_max = db.Column(db.Integer)
    time_mean = db.Column(db.Float)
    time_median = db.Column(db.Integer)
    time_standard_deviation = db.Column(db.Float)
    time_variance = db.Column(db.Float)
    time_n = db.Column(db.Integer)

    score_min = db.Column(db.Integer)
    score_max = db.Column(db.Integer)
    score_mean = db.Column(db.Float)
    score_median = db.Column(db.Integer)
    score_standard_deviation = db.Column(db.Float)
    score_variance = db.Column(db.Float)
    score_n = db.Column(db.Integer)

    def generate(self, filters={}, results=[]):
        stats = {}

        scores = []
        times = []
        num_results = 0
        num_unfinished = 0
        num_aborted = 0
        try:
            num_surveys = len(self.test.surveys)
            stats['num_surveys'] = num_surveys
        except AttributeError:
            pass

        filter_surveys = filters.get('survey_ids', None)
        filter_results = filters.get('result_ids', None)

        for test_result in results:
            if filter_surveys and test_result.survey_id not in filter_surveys:
                continue
            if filter_results and test_result.id not in filter_results:
                continue

            if test_result.end and test_result.start and not test_result.abort:
                time = (test_result.end - test_result.start).total_seconds()
                times.append(time)

                num_results += 1
                scores.append(
                    test_result.get_correct().get('num_items_correct'))
            elif test_result.abort:
                num_aborted += 1
            else:
                num_unfinished += 1

        score_stats = descriptive.summary(scores)
        time_stats = descriptive.summary(times)

        stats['score_min'] = score_stats.get('min')
        stats['score_max'] = score_stats.get('max')
        stats['score_mean'] = score_stats.get('mean')
        stats['score_median'] = score_stats.get('median')
        stats['score_standard_deviation'] = score_stats.get(
            'standard_deviation')
        stats['score_variance'] = score_stats.get('variance')
        stats['score_n'] = score_stats.get('n')

        stats['time_min'] = time_stats.get('min')
        stats['time_max'] = time_stats.get('max')
        stats['time_mean'] = time_stats.get('mean')
        stats['time_median'] = time_stats.get('median')
        stats['time_standard_deviation'] = time_stats.get('standard_deviation')
        stats['time_variance'] = time_stats.get('variance')
        stats['time_n'] = time_stats.get('n')

        stats['num_results'] = num_results
        stats['num_aborted'] = num_aborted
        stats['num_unfinished'] = num_unfinished

        return stats

    def update(self, results=[]):
        stats = self.generate(results=results)

        self.time_min = stats.get('time_min')
        self.time_max = stats.get('time_max')
        self.time_mean = stats.get('time_mean')
        self.time_median = stats.get('time_median')
        self.time_standard_deviation = stats.get('time_standard_deviation')
        self.time_variance = stats.get('time_variance')
        self.time_n = stats.get('time_n')
        self.score_min = stats.get('score_min')
        self.score_max = stats.get('score_max')
        self.score_mean = stats.get('score_mean')
        self.score_median = stats.get('score_median')
        self.score_standard_deviation = stats.get('score_standard_deviation')
        self.score_variance = stats.get('score_variance')
        self.score_n = stats.get('score_n')
        self.num_results = stats.get('num_results', 0)
        self.num_surveys = stats.get('num_surveys', 0)
        self.num_aborted = stats.get('num_aborted', 0)
        self.num_unfinished = stats.get('num_unfinished', 0)

        db.session.commit()

        return stats
