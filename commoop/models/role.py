from commoop import db
from commoop.models.base import Base


class Role(Base):
    __tablename__ = 'role'

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(64), unique=True)
    members = db.relationship('Member', back_populates='role')

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return '<Role %r>' % self.type
