from commoop import db
from commoop.models.test_abstract_statistic import TestAbstractStatistic
from commoop.models.test_result import TestResult


class SurveyStatistic(TestAbstractStatistic):
    __tablename__ = 'survey_statistic'

    test_id = db.Column(db.Integer, db.ForeignKey('test.id'))
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))
    survey = db.relationship('Survey', back_populates='statistic')

    def as_dict(self, filters={}):
        updated_on = self.updated_on
        results = TestResult.query.filter_by(
            test_id=self.test_id, survey_id=self.survey_id).order_by(
                TestResult.updated_on.desc()).all()

        last_result = None
        try:
            last_result = results[0]
        except IndexError:
            last_result = None

        if last_result and (not updated_on
                            or updated_on < last_result.updated_on):
            dict_obj = self.update(results=results)
        else:
            dict_obj = super(SurveyStatistic, self).as_dict()

        dict_obj['real_results'] = len(results)
        return dict_obj
