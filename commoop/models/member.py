import os
import time

from commoop.database import db
from commoop.models.association_item_favorized import \
    association_item_favorized
from commoop.models.association_item_permitted import \
    association_item_permitted
from commoop.models.association_member_item_category import \
    association_member_item_category
from commoop.models.association_survey_favorized import \
    association_survey_favorized
from commoop.models.association_survey_permitted import \
    association_survey_permitted
from commoop.models.association_test_favorized import \
    association_test_favorized
from commoop.models.association_test_permitted import \
    association_test_permitted
from commoop.models.base import Base
from itsdangerous import BadSignature, SignatureExpired
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from passlib.apps import custom_app_context as pwd_context


class Member(Base):
    __tablename__ = 'member'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    email = db.Column(db.String(128), unique=True)
    password_hash = db.Column(db.String(128))
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))
    role = db.relationship('Role', back_populates='members')
    items_permitted = db.relationship(
        'Item',
        secondary=association_item_permitted,
        back_populates='members_permitted')
    items_favorized = db.relationship(
        'Item',
        secondary=association_item_favorized,
        back_populates='members_favorized')
    items = db.relationship('Item', back_populates='member')
    tests = db.relationship('Test', back_populates='member')
    tests_permitted = db.relationship(
        'Test',
        secondary=association_test_permitted,
        back_populates='members_permitted')
    tests_favorized = db.relationship(
        'Test',
        secondary=association_test_favorized,
        back_populates='members_favorized')
    surveys = db.relationship('Survey', back_populates='member')
    surveys_permitted = db.relationship(
        'Survey',
        secondary=association_survey_permitted,
        back_populates='members_permitted')
    surveys_favorized = db.relationship(
        'Survey',
        secondary=association_survey_favorized,
        back_populates='members_favorized')
    item_categories = db.relationship(
        'Category',
        secondary=association_member_item_category,
        back_populates='members')
    item_results = db.relationship('ItemResult', back_populates='member')
    item_result_comments = db.relationship(
        'ItemResultComment', back_populates='member')
    refresh_token = db.Column(db.String(512))

    def hash_password(self, password):
        self.password_hash = pwd_context.hash(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_access_token(self, expiration=3600):
        s = Serializer(
            os.getenv('COMMOOP_SECRECT', 'not-save'), expires_in=expiration)
        return s.dumps({'id': self.id})

    def generate_refresh_token(self):
        s = Serializer(
            os.getenv('COMMOOP_SECRECT', 'not-save'), expires_in=None)
        refresh_token = s.dumps({'id': self.id, 'date': time.time()})
        self.refresh_token = refresh_token
        return refresh_token

    @staticmethod
    def verify_access_token(token):
        s = Serializer(os.getenv('COMMOOP_SECRECT', 'not-save'))
        try:
            data = s.loads(token)
            member = Member.query.get(data['id'])
            return member
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        return None

    def verify_refresh_token(self, token):
        return self.refresh_token == token

    def as_dict(self, public=False):
        dict_obj = super(Member, self).as_dict()
        del dict_obj['role_id']
        del dict_obj['password_hash']
        del dict_obj['refresh_token']
        if not public:
            dict_obj['items_permitted'] = [
                item.id for item in self.items_permitted
            ]
            dict_obj['items_favorized'] = [
                item.id for item in self.items_favorized
            ]
            dict_obj['tests_permitted'] = [
                test.id for test in self.tests_permitted
            ]
            dict_obj['tests_favorized'] = [
                test.id for test in self.tests_favorized
            ]
            dict_obj['surveys_permitted'] = [
                survey.id for survey in self.surveys_permitted
            ]
            dict_obj['surveys_favorized'] = [
                survey.id for survey in self.surveys_favorized
            ]
        if self.role:
            dict_obj['role'] = self.role.type
        else:
            dict_obj['role'] = ''
        return dict_obj

    def is_permitted_for_read(self, user):
        return user.role.type == 'admin' \
            or user == self

    def is_permitted_for_write(self, user):
        return user.role.type == 'admin' \
            or user == self

    def __repr__(self):
        return '<Member %r>' % self.username
