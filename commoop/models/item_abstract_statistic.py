from datetime import datetime

from commoop import db
from commoop.models.base import Base
from commoop.responses.v1.choice import Choice
from commoop.responses.v1.matrix import Matrix
from commoop.responses.v1.select import Select
from commoop.responses.v1.text import Text
from commoop.statistics import descriptive


class ItemAbstractStatistic(Base):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)

    num_results = db.Column(db.Integer)
    num_unfinished = db.Column(db.Integer)
    num_correct = db.Column(db.Integer)
    num_tests = db.Column(db.Integer)
    num_surveys = db.Column(db.Integer)

    time_min = db.Column(db.Float)
    time_max = db.Column(db.Float)
    time_mean = db.Column(db.Float)
    time_median = db.Column(db.Float)
    time_standard_deviation = db.Column(db.Float)
    time_variance = db.Column(db.Float)
    time_n = db.Column(db.Integer)

    def generate(self, filters={}, results=[]):
        stats = {}

        num_results = 0
        num_correct = 0
        num_unfinished = 0

        filter_surveys = filters.get('survey_ids', None)
        filter_tests = filters.get('test_ids', None)
        filter_results = filters.get('result_ids', None)

        times = []
        for result in results:
            if filter_surveys and not result.get_survey_id() in filter_surveys:
                continue
            if filter_tests and not result.get_test_id() in filter_tests:
                continue
            if filter_results and result.id in filter_results:
                continue
            if not result.end_time or not result.start_time:
                num_unfinished += 1
                continue
            time = (result.end_time - result.start_time).total_seconds()
            times.append(time)
            num_results += 1
            if result.correct:
                num_correct += 1
        time_stats = descriptive.summary(times)
        stats['time_min'] = time_stats.get('min')
        stats['time_max'] = time_stats.get('max')
        stats['time_mean'] = time_stats.get('mean')
        stats['time_median'] = time_stats.get('median')
        stats['time_standard_deviation'] = time_stats.get('standard_deviation')
        stats['time_variance'] = time_stats.get('variance')
        stats['time_n'] = time_stats.get('n')

        stats['num_results'] = num_results
        if self.item.solution:
            stats['num_correct'] = num_correct
        stats['num_tests'] = len(self.item.get_tests())
        stats['num_surveys'] = len(self.item.get_surveys())
        stats['num_unfinished'] = num_unfinished
        return stats

    def update(self, results=[]):
        stats = self.generate(results=results)

        self.time_min = stats.get('time_min')
        self.time_max = stats.get('time_max')
        self.time_mean = stats.get('time_mean')
        self.time_median = stats.get('time_median')
        self.time_standard_deviation = stats.get('time_standard_deviation')
        self.time_variance = stats.get('time_variance')
        self.time_n = stats.get('time_n')
        self.num_results = stats.get('num_results', 0)
        self.num_correct = stats.get('num_correct')
        self.num_tests = stats.get('num_tests', 0)
        self.num_surveys = stats.get('num_surveys', 0)
        self.num_unfinished = stats.get('num_unfinished', 0)

        db.session.commit()

        return stats
