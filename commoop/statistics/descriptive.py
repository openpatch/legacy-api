import statistics


def summary(values):
    if not values or len(values) < 2:
        return {}
    return {
        'min': min(values),
        'max': max(values),
        'mean': statistics.mean(values),
        'median': statistics.median(values),
        'standard_deviation': statistics.stdev(values),
        'variance': statistics.variance(values),
        'n': len(values)
    }
