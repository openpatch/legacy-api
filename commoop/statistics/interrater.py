import math


def get_contingency_table(c1, c2):
    if len(c1) != len(c2):
        return 0

    a = 0
    b = 0
    c = 0
    d = 0

    for i in range(len(c1)):
        v1 = c1[i]
        v2 = c2[i]

        if v1 == True and v2 == True:
            a += 1
        elif v1 == True and v2 == False:
            b += 1
        elif v1 == False and v2 == True:
            c += 1
        elif v1 == False and v2 == False:
            d += 1

    return {
        'a': a,
        'b': b,
        'c': c,
        'd': d
    }


def get_rho(contingency_table):
    a = contingency_table.get('a')
    b = contingency_table.get('b')
    c = contingency_table.get('c')
    d = contingency_table.get('d')
    denominator = math.sqrt(float((a+b)*(a+c)*(d+b)*(d+c)))
    if denominator != 0:
        rho = (a*d-b*c) / denominator
    else:
        rho = 'NA'
    return rho


def get_alpha(contingency_table, n):
    a = contingency_table.get('a')
    b = contingency_table.get('b')
    c = contingency_table.get('c')
    d = contingency_table.get('d')

    denominator = float((2*a+b+c) * (b+c+2*d))

    if denominator == 0:
        return "NA"
    alpha = 1 - (2 * n - 1) * (b + c) / denominator
    return alpha


def get_kappa(contingency_table):
    a = contingency_table.get('a')
    b = contingency_table.get('b')
    c = contingency_table.get('c')
    d = contingency_table.get('d')

    denominator = float(a+b+c+d)

    po = (a+d) / denominator
    py = (a+b) / denominator * (a+c) / denominator
    pn = (c+d) / denominator * (b+d) / denominator
    pe = py + pn
    if pe == 1:
        return 'NA'
    k = (po - pe) / (1 - pe)
    return k
